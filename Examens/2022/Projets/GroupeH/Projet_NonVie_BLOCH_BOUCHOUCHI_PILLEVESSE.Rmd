---
title: "Projet Assurance Non Vie"
subtitle : "GLM sur le nombre et le coût des sinistres en assurance automobile"
author: "Groupe H : Soline BLOCH - Ines BOUCHOUCHI - Swanny PILLEVESSE"
date: "15/04/22"
output:
  html_document:
    highlight: espresso
    theme: cerulean
    toc: Yes
    top_depth: 2
  pdf_document:
    toc: Yes
---

```{r , echo=FALSE, warning=FALSE , message=FALSE}
library(data.table)
library(ggplot2)
library(readxl)
library(dplyr)
library(Hmisc)
library(data.table)
library(ggplot2)
library(readxl)
library(dplyr)
library(corrplot)
library(car)
```

# Présentation et but du projet

Nous avons décidé de travailler sur une base de données sur les coûts et le nombre des accidents de voiture sur le site “https://freakonometrics.hypotheses.org/978”.

Notre base de données regroupe deux bases : une base coût des sinistres et une base nombre des sinistres entre 1995 et 2004. Une ligne représente un assuré sur une année, s’il a eu un sinistre alors il apparaît dans la base coût.

Notre objectif serait de réaliser deux GLM :

- un sur le nombre de sinistres

- un sur le coût des sinistres 

En s'intéressant aux deux variables d'intérêt citées précédemment, nous essaierons de répondre à la problématique suivante: quels sont les facteurs qui influent sur le montant de sinistre et sur leur nombre?

# Importation et description des données 

```{r echo=FALSE}
basecout<-read_excel("basecout_groupe_h.xlsx")
basefreq<-read_excel("basefreq_groupe_h.xlsx")
basecout$coutsinistre=as.numeric(basecout$coutsin) # on met en numerique la variable cout sinistre
```

La base coût comprend 6515 observations et la  base fréquence comprend 9731 observations.

Les deux variables quantitatives que l'on cherchera à prédire sont :

  * **sinistretotal** : Nombre de sinistres de l'assuré dans l'année (variable utilisée pour le modèle Poisson).    
  
  * **coutsinistre** : Montant du sinistre (variable utilisée pour le modèle gamma).  

Notre jeu de données contient les variables explicatives suivantes : 

  * **freq_paiement** : annuel ou mensuel
  * **langue** : F (français) ou A (autre)
  * **type_prof** : actuaire, autre, avocat, hockeyeur, infirmière, informaticien, ingénieur, médecin, professeur, technicien
  * **alimentation** : carnivore, végétalien, végétarien  
  * **type_territoire** : rural, semi-urbain, urbain
  * **utilisation** : loisir, travail occasionnel, travail quotidien
  * **presence_alarme** : oui ou non
  * **marque_voiture** : alfa romeo, audi, autres, bmw, chrysler, daewoo, daihatsu, fiat, ford, gm , honda, hyundai, kia, lada, lancia, mazda, mercedes, mitsubishi, nissan, peugeot, renault, rover, saab, seat, skoda,subaru, suzuki,  toyota, volkswagen, volvo
  * **sexe** : F (féminin) ou M (masculin)
  * **âge** : entier entre 20 et 82 ans
  * **durée_permis** : entier entre 0 et 77 ans
  * **âge_véhicule** : entier entre 11 et 43 ans
  
# Préparation des données aux GLM

Dans les GLM les variables explicatives doivent être des variables qualitatives. Si nous avons des variables quantitatives que nous souhaiter utiliser un bon moyen est de les transformer en variables qualitatives en créant des classes (pour cela nous nous servirons de la fonction cut sur R).

De plus, nous avons également décidé de séparer chacunes des deux bases en deux parties. Une première, “train”, avec 80% des observations sur laquelle on entraînera nos modèles. Un autre dataset, “test”, avec les 20% restants sur lequel on réalisera les prédictions. On pourra ainsi comparer nos prédictions avec les données réelles.

## Base coût

Nous transformons les variables âge, durée du permis et âge du véhicule en variables catégorielles.

Nous avons réalisé ces classes grâce à la fonction cut et nous nous sommes servis des quantiles pour avoir des classes avec des effectifs homogènes.

```{r echo=FALSE}
summary(basecout$coutsinistre)
basecout2 =filter(basecout, basecout$coutsinistre <= 1699.6)
```

Nous remarquons un trop gros écart avec le 3e quartile et le max des observations, ce qui pourrait entrainer des erreurs par la suite. Pour cela, nous avons décidé de tronquer nos données en supprimant les lignes supérieures au 3e quartile.

Regardons la répartition des données pour la variable "âge" et créons des classes homogènes par rapport à la répartition à l'aide de la fonction cut :

```{r echo=FALSE}
summary(basecout2$age)
basecout2$age_bis =cut(basecout2$age,c(0,37,44,53,82),labels=c("<37 ans","38 à 44 ans", "45 à 53 ans", ">53 ans"))
table(basecout2$age_bis)
```

Nous effectuons le même travail pour la variable "âge véhicule" et "durée permis"

```{r echo=FALSE,include=FALSE}
summary(basecout2$age_vehicule)
basecout2$age_vehicule_bis =cut(basecout2$age_vehicule,c(0,15,17,20,43),labels=c("<15ans","15 à 17 ans", "18 à 20 ans", ">20 ans"))
table(basecout2$age_vehicule_bis)
```


```{r echo=FALSE, include=FALSE}
summary(basecout2$duree_permis)
basecout2$duree_permis_bis =cut(basecout2$duree_permis,c(0,15,22,29,77),labels=c("<15ans","15 à 22 ans", "22 à 29 ans", ">29 ans"))
table(basecout2$duree_permis_bis)
```

## Base nombre de sinistres

Nous avons effectué la même démarche pour la base nombre de sinistres en regardant la répartition des données pour la variable "âge" et en créant des classes homogènes par rapport à la répartition à l'aide de la fonction cut.

La démarche a été effectuée sur les variables suivantes : "age", "durée_permis","durée contrat en années","année du véhicule"

```{r echo=FALSE, include=FALSE}
summary(basefreq$age)
basefreq$age_bis =cut(basefreq$age,c(0,35,42,52,82),labels=c("<=35 ans","36 à 42 ans", "43 à 52 ans", ">52"))

```

```{r echo=FALSE, include=FALSE}
summary(basefreq$duree_permis)
basefreq$duree_permis_bis=cut(basefreq$duree_permis,c(0,14,20,28,73),labels=c("<=14 ans","15 à 20 ans", "21 à 28 ans", "29 à 73 ans"))
table(basefreq$duree_permis_bis)
```

```{r echo=FALSE, include=FALSE}
summary(basefreq$duree_contrat_an)
basefreq$duree_contrat_bis = cut(basefreq$duree_contrat_an,c(0,1.9,3.2,4.7,8),labels=c("<=1.9 ans","2 à 3.2 ans", "3.3 à 4.7 ans", "> 4.7 ans"))
table(basefreq$duree_contrat_bis)
```

```{r echo=FALSE, include=FALSE}
summary(basefreq$annee_vehicule)
basefreq$annee_vehicule_bis = cut(basefreq$annee_vehicule,c(1945,1990,1993,1996,2003),labels=c("avant 1990","1991-1993", "1994-1996", ">1996"))
table(basefreq$annee_vehicule_bis)
```

```{r echo=FALSE,, include=FALSE}
n1 = round(0.8*nrow(basecout2))
basecout_train = basecout2[row.names(basecout2)%in% 1:n1,]
basecout_test = basecout2[row.names(basecout2)%in% (n1+1):nrow(basecout2),]
n2 = round(0.8*nrow(basefreq))
basefreq_train = basefreq[row.names(basefreq)%in% 1:n2,]
basefreq_test = basefreq[row.names(basefreq)%in% (n2+1):nrow(basefreq),]
```

  
# Analyse descriptive

Avant d'entamer la construction des GLM, nous avons d'abord effectué une analyse descriptive concernant nos deux variables cibles "coutsinistre" et"sinistretotal" afin d'avoir une meilleure idée de la répartition et des facteurs pouvant influer sur ces variables.

## Base coût

Ci-dessous un tableau regroupant les statistiques descriptives pour les variables quantitatives de la base coût des sinistres :

```{r echo=FALSE}
m1=mean(basecout$age)
v1=var(basecout$age)
min1=min(basecout$age)
max1=max(basecout$age)
med1=median(basecout$age)

m2=mean(basecout$duree_permis)
v2=var(basecout$duree_permis)
min2=min(basecout$duree_permis)
max2=max(basecout$duree_permis)
med2=median(basecout$duree_permis)

m3=mean(basecout$age_vehicule)
v3=var(basecout$age_vehicule)
min3=min(basecout$age_vehicule)
max3=max(basecout$age_vehicule)
med3=median(basecout$age_vehicule)

m4=mean(basecout$coutsinistre)
v4=var(basecout$coutsinistre)
min4=min(basecout$coutsinistre)
max4=max(basecout$coutsinistre)
med4=median(basecout$coutsinistre)

##Tableau regroupant les statistiques descriptives
Nom<-c("age du véhicule","durée du permis", "age", "cout du sinsitre"); Moyenne<-c(m1,m2,m3,m4) ; Variance <- c(v1,v2,v3,v4);Minimum <- c(min1,min2,min3,min4);Maximum<- c(max1,max2,max3,max4);Mediane <- c(med1,med2,med3,med4)
tableau <- data.frame(Nom,Moyenne,Variance,Minimum,Maximum,Mediane) 
print(tableau)

```

Ci-dessous la matrice de corrélation des variables quantitatives :

```{r echo=FALSE}
tabquant=data.frame(basecout$age_vehicule ,basecout$duree_permis,basecout$age, basecout$coutsinistre)
corrplot(cor(tabquant), method="number")
```


On remarque une corrélation évidente entre l'âge de l'assuré et l'ancienneté de son permis.


Regardons maintenant les variables qualitatives à l'aide du package ggplot2 :

```{r echo = FALSE}

ggplot (basecout_train, aes(x=sexe, y=coutsinistre, fill=sexe ))+ geom_boxplot()+ggtitle("Boxplot du coût des sinistres en fonction du genre")
```

Nous remarquons que les hommes semblent avoir des sinistres très légèrement moins onéreux que les femmes.


```{r echo=FALSE}
ggplot (basecout_train, aes(x=duree_permis_bis,y=coutsinistre, fill=duree_permis_bis))+ geom_boxplot()+ggtitle("Boxplot du coût des sinistres en fonction de la durée du permis")
```

Il semblerait que le coût moyen d'un sinistre augmente avec la durée d'obtention du permis. En effet, les personnes ayant une durée de permis inférieure à 15 ans ont des sinistres moins chers. Cela est peut être lié au type de véhicule qu'ils conduisent (les personnes plus âgées ont une durée de permis élevée et des voitures plus haut de gamme qu'un permis récent donc une réparation plus lourde en cas de sinistre)

```{r echo=FALSE}
ggplot (basecout_train, aes(x=utilisation,y=coutsinistre, fill=utilisation))+ geom_boxplot()+ggtitle("Boxplot du coût des sinistres en fonction du type d'utilisation")

```

Les personnes se servant de leur véhicule quotidiennement pour aller au travail ont en moyenne des sinistres plus onéreux.


```{r echo=FALSE}
ggplot (basecout_train, aes(x=presence_alarme,y=coutsinistre, fill=presence_alarme))+ geom_boxplot()

```

Les personnes ayant des alarmes dans leur voiture auraient des sinistres en moyenne plus coûteux. Cela est probablement lié au type de voiture (les voitures haute gamme ont des alarmes et il est plus cher de les réparer)


## Base nombre sinistres

Nous effectuons la même démarche pour la base nombre de sinistres.

Ci-dessous un tableau regroupant les statistiques descriptives pour les variables quantitatives :

```{r echo=FALSE}
m1=mean(basefreq$age)
v1=var(basefreq$age)
min1=min(basefreq$age)
max1=max(basefreq$age)
med1=median(basefreq$age)

m2=mean(basefreq$duree_permis)
v2=var(basefreq$duree_permis)
min2=min(basefreq$duree_permis)
max2=max(basefreq$duree_permis)
med2=median(basefreq$duree_permis)

m3=mean(basefreq$annee_vehicule)
v3=var(basefreq$annee_vehicule)
min3=min(basefreq$annee_vehicule)
max3=max(basefreq$annee_vehicule)
med3=median(basefreq$annee_vehicule)

basefreq$`cout total` =as.numeric(basefreq$`cout total`)
m4=mean(basefreq$`cout total`)
v4=var(basefreq$`cout total`)
min4=min(basefreq$`cout total`)
max4=max(basefreq$`cout total`)
med4=median(basefreq$`cout total`)

m5=mean(basefreq$sinistretotal)
v5=var(basefreq$sinistretotal)
min5=min(basefreq$sinistretotal)
max5=max(basefreq$sinistretotal)
med5=median(basefreq$sinistretotal)

##Tableau regroupant les statistiques descriptives
Nom<-c("age","durée du permis", "année du véhicule", "cout total des sinsitres", "Nombre de sinistres total"); Moyenne<-c(m1,m2,m3,m4,m5) ; Variance <- c(v1,v2,v3,v4,v5);Minimum <- c(min1,min2,min3,min4,min5);Maximum<- c(max1,max2,max3,max4, max5);Mediane <- c(med1,med2,med3,med4, med5)
tableau <- data.frame(Nom,Moyenne,Variance,Minimum,Maximum,Mediane) 
print(tableau)
```


De plus, on retrouve une corrélation évidente entre l'âge de l'assuré et l'ancienneté de son permis.

Regardons maintenant les variables qualitatives à l'aide du package ggplot2 :

```{r echo=FALSE}
plot1=ggplot(basefreq_train, aes(x=sinistretotal, fill=utilisation))+geom_bar()+xlab("nombre de sinistre")+
  ylab("Effectifs")+ggtitle("nombre de sinistre en fonction de l'utilisation")
plot1
```

Nous remarquons que les loisirs ont deux sinistres au plus. Ce sont les travailleurs occasionnel qui sont le plus sinistrés. Cela est peut-être lié au fait qu'ils représentent une grande partie de nos données.

```{r echo=FALSE}
plot2=ggplot(basefreq_train, aes(x=sinistretotal, fill=sexe))+geom_bar()+xlab("nombre de sinistre")+
  ylab("Effectifs")+  ggtitle("nombre de sinsitre en fonctio du sexe")
plot2
```

Il semblerait que les hommes ont tendance à avoir plus de sinistres.

```{r echo=FALSE}

plot4=ggplot(basefreq_train, aes(x=sinistretotal, fill=age_bis))+geom_bar()+xlab("nombre de sinistre")+
  ylab("Effectifs")+  ggtitle("nombre de sinsitre en fonction de l'âge de l'assuré")
plot4
```

L'âge ne semble pas influer sur le nombre de sinistre

```{r echo=FALSE}
plot6=ggplot(basefreq_train, aes(x=sinistretotal, fill=alimentation))+geom_bar()+xlab("nombre de sinistre")+
  ylab("Effectifs")+  ggtitle("nombre de sinsitre en fonction du type d'utilisation")
plot6
```

Il semblerait que les carnivore auraient plus de sinistre que les végétaliens. Cependant on ne remarque pas de différence significative entre les végétariens et les carnivores.

```{r echo=FALSE}
plot7 = ggplot(basefreq_train, aes(x=sinistretotal, fill=type_territoire))+geom_bar()+xlab("nombre de sinistre")+
  ylab("Effectifs")+  ggtitle("nombre de sinsitre en fonction du type de territoire")
plot7
```

Le territoire semi urbain semble propice aux sinistres.

# GLM nombre de sinistres 

Notre premier modèle linéaire généralisé va chercher à prédire le nombre des sinistres survenus pour un assuré, via l'intermédiaire de la variable "sinistretotal". Nous allons pour cela utiliser la loi de Poisson car la variable à prédire est positive, discrète et non binaire. On a alors :

 * $Y_i|X_i$ qui suit une loi de Poisson
 * $log(E[Y_i|X_i]) = X_i^T\beta$

## GLM avec toutes les variables 

Dans cette première partie nous allons utiliser toutes les variables explicatives à disposition pour construire le modèle suivant.

```{r echo=FALSE}
glmfreq = glm(sinistretotal~ freq_paiement+langue+type_prof+alimentation+type_territoire+utilisation+presence_alarme+marque_voiture+sexe+age_bis+duree_permis_bis+annee_vehicule_bis , data=basefreq_train, family=poisson() )
```

```{r echo=FALSE}
summary(glmfreq)
```

Nous remarquons que seulement peu de variables sont significatives,la plupart des p-valeur sont supérieures à 0,05 et ne participent pas grandement à l'explication de variable cible. 

Nous voyons que la déviance résiduelle (qui représente le modèle avec les variables explicatives) est inférieure à la déviance null (qui représente le modèle avec seulement l'intercept) ce qui signifie que nos variables explicatives jouent un rôle quant au nombre de sinistre par assuré.

Comparons le quantile du khi-2 à 95% au dégré de liberté 7726 :

```{r echo=FALSE}
qchisq(0.95,7726)
```

Notre déviance résiduelle s'approche du quantile du khi-2, bien que nous soyons conscient que le modèle n'est pas idéal.

Ce modèle n’est donc pas optimal. Nous devons établir un processus de sélection de variables.

## Sélection des variables 

Nous allons essayer de sélectionner les variables afin d'obtenir un modèle optimal.

### Sélection par analyse de la déviance

Premièrement, nous allons sélectionner les variables grâce à la table de l’analyse de la déviance.

```{r echo=FALSE}
anova(glmfreq, test = "Chisq")
```

Dans cette anova à chaque ligne les variables sont ajoutées aux variables précédentes. On peut voir que lorsque l'on ajoute les variables "alimentation" ou "type de profession" le GLM semble perdre en pouvoir explicatif.

Cependant ce tableau ne nous donne pas une vision globale des modèles et ne permet pas de vérifier directement la significativité de chaque variable. Pour ce faire, on peut utiliser la fonction Anova du package “car” :

```{r echo=FALSE}
library(car)
Anova(glmfreq, test.statistic = "LR", type = 3)
```

Ce tableau nous donne l'analyse variable par variable. Nous décidons de ne garder que les variables ayant 3 étoiles et étant donc très significatives.

Afin de minimiser notre nombre de variables explicatives, nous allons procéder à une autre sélection de variable.

### Sélection à partir de l’AIC

Deuxièmement, nous allons sélectionner les variables en utilisant un autre indicateur : l’AIC. Nous allons regarder quel modèle le minimise. C’est pourquoi nous utilisons l'option backward. La fonction va alors prendre le modèle avec toutes les variables et retirer étape par étape les variables afin d’obtenir un modèle avec un AIC minimisé. On obtient les résultats suivants :

```{r echo=FALSE}
step(glmfreq, direction = "backward")

```

On remarque que l'AIC nous conseille de retirer le type de territoire, l'année du véhicule et la marque du véhicule afin d'obtenir un meilleur modèle.

On construit alors le nouveau modèle ajusté avec les variables sélectionnées précédemment.

```{r echo=FALSE}
modeleAIC <- glm(formula = sinistretotal ~ freq_paiement + 
    langue + type_prof + alimentation + 
    utilisation + presence_alarme + 
   sexe + age_bis + duree_permis_bis + 
   annee_vehicule_bis, family = poisson(), data = basefreq_train)
#summary(modeleAIC)
```


## Comparaison des deux modèles avec une ANOVA

Nous allons comparer les deux modèles (initial avec toutes les variables et ajusté avec l'AIC) grâce à une ANOVA

```{r echo=FALSE}
anova(glmfreq,modeleAIC,test="LRT")
```

Ce test montre bien que notre second modèle ajusté est plus significatif que celui incluant toutes les variables explicatives.

## Analyse des résidus 

Regardons l'allure des résidus mis au carré de nos individus.

De plus nous savons que : 

Δ = 2(l-l) = 2∑δi² où δi est la racine carré de la contribution de l’observation i sur la déviance.

Si δi est trop grand, cela signifie que l’individu i contribue au fait que la déviance soit grande.

E[Δ] ≈ n−p donc δi ≈ n−p/n ≈1

Une valeur de δi² éloignée de 1 indique que l’observations i contribue au mauvais ajustement du modèle.

```{r echo=FALSE}
residus=residuals(modeleAIC)
plot(residus^2)
abline(h=1, col='blue')
#print(sum(residus^2))
```

On observe qu'il y a énormement d'individus qui ont des résidus au carré supérieurs à 1 ce qui fausse le modèle.

```{r echo=FALSE}
#length(which(residus^2>1))
```

## Modèle final 

Nous gardons le modèle final suivant (celui ajusté avec l'AIC) : 

```{r echo=FALSE}
summary(modeleAIC)
```


# GLM coût des sinistres

Notre second modèle linéaire généralisé va chercher à prédire le montant des sinistres survenus pour un assuré, via l'intermédiaire de la variable "coutsinistre". Nous allons pour cela utiliser la loi  Gamma car la variable à prédire est positive et continue. On a alors :

 * $Y_i|X_i$ qui suit une loi Gamma
 * $1/(E[Y_i|X_i]) = X_i^T\beta$

## GLM avec toutes les variables 

Dans cette première partie nous allons utiliser toutes les variables explicatives à disposition pour construire le modèle suivant.

```{r echo=FALSE}
glm_cout=glm(formula = coutsinistre ~  freq_paiement+langue+type_prof+ alimentation +type_territoire+utilisation +presence_alarme +marque_voiture+sexe +duree_permis_bis+age_vehicule_bis+age_bis, data = basecout_train,family =Gamma())
```

```{r echo=FALSE}
summary(glm_cout)
```

Nous remarquons que notre modèle  contient beaucoup de variables et nous constatons à nouveau que seulement peu d'entre elles sont significatives et ont une p-valeur inférieure à 0.05. 

Comme pour le GLM précédent, la déviance résiduelle est inférieure à la deviance nulle ce qui signifie que les variables jouent bien un rôle quant à l'explication du coût du sinistre. 
Comparons le quantile du khi-2 à 95% au dégré de liberté 3852 :

```{r echo=FALSE}
qchisq(0.95,3852)
```

Notre déviance éesiduelle s’approche du quantile du khi-2, bien que nous soyons conscient que le modèle n’est pas idéal.

Ce modèle n’est donc pas optimal. Nous devons établir un processus de sélection de variables.

## Sélection des variables 

Nous allons essayer de sélectionner les variables afin d’obtenir un modèle optimal.

### Sélection par analyse de la déviance 

Premièrement, nous allons sélectionner les variables grâce à la table de l’analyse de la déviance.

```{r echo=FALSE}
anova(glm_cout, test = "Chisq")
```

Dans cette anova à chaque ligne les variables sont ajoutées aux variables précédentes.

Un modèle qui semblerait très bien expliquer le coût du sinistre serait le modèle ayant les variables suivantes:
fréquence paiement, langue, type de profession, alimentation, type territoire et utilisation.

Lorsque l'on ajoute la variable présence d'alarme au modèle, il perd de son pouvoir explicatif.Cela est peut-être lié à des corrélations entre les variables.

Cependant ce tableau ne nous donne pas une vision globale des modèles et ne permet pas de vérifier directement la significativité de chaque variable. Pour ce faire, on peut utiliser la fonction Anova du package “car” :

NB : nous décidons de retirer la variable "durée permis" du modèle car elle est très correlée "âge" et" à l'âge du véhicule "âge du véhicule" et cela nous empêche de faire tourner le test Anova de type 3 (celui prenant en compte les relations entre variables).

```{r echo=FALSE}
glm_cout2=glm(formula = coutsinistre ~  freq_paiement+langue+type_prof+ alimentation +type_territoire+utilisation +presence_alarme +marque_voiture+sexe +age_vehicule_bis+age_bis, data = basecout_train,family =Gamma())
```

```{r echo=FALSE}

Anova(glm_cout2, test.statistic = "LR", type = 3)
```

Ce tableau nous donne l’analyse variable par variable. On décidera de ne garder que les variables avec trois étoiles donc très significatives.
Néanmoins, nous n'en avons pas... C'est surement légèrement biaisé par le fait qu'on a retiré la variable "durée du permis "

### Sélection à partir de l'AIC

Deuxièmement, nous allons sélectionner les variables en utilisant un autre indicateur : l’AIC. Nous allons regarder quel modèle le minimise. C’est pourquoi nous utilisons l’option backward. La fonction va alors prendre le modèle avec toutes les variables et retirer étape par étape les variables afin d’obtenir un modèle avec un AIC minimisé. On obtient les résultats suivants :

```{r echo=FALSE}
modeleAICcout=step(glm_cout2, direction = "backward")
```

On remarque que l’AIC nous conseille de retirer la fréquence de paiement, le type de profession, l'alimentation, le type de territoire, la marque de la voiture et l'âge de l'individu.

On construit alors le nouveau modèle ajusté avec les variables sélectionnées précédemment

## Comparaison des deux modèles avec une ANOVA

Nous allons comparer les deux modèles (initial avec toutes les variables et ajusté avec l'AIC) grâce à une ANOVA

```{r echo=FALSE}
anova(glm_cout2,modeleAICcout,test="LRT")
```

Ce test ne montre pas une énorme significativité des deux modèles construits. Néanmoins, celui construit à l'aide de l'AIC semble légèrement meilleur de part sa p-valeur plus faible et son nombre plus restreint de variables explicatives.

## Analyse des résidus

Regardons l’allure des résidus mis au carré de nos individus.

De plus nous savons que :

Δ = 2(l-l) = 2∑δi² où δi est la racine carré de la contribution de l’observation i sur la déviance.

Si δi est trop grand, cela signifie que l’individu i contribue au fait que la déviance soit grande.

E[Δ] ≈ n−p donc δi ≈ n−p/n ≈1

Une valeur de δi² éloignée de 1 indique que l’observations i contribue au mauvais ajustement du modèle

```{r echo=FALSE}
residuscout=residuals(modeleAICcout)
plot(residuscout^2)
abline(h=1, col='blue')
#print(sum(residuscout^2))
```

On observe qu’il y a beaucoup d’individus (environ 27%) qui ont des résidus au carré supérieurs à 1 ce qui fausse le modèle

```{r echo=FALSE}
#length(which(residuscout^2>1))
```

## Modèle final 

Nous gardons le modèle final suivant :

```{r echo=FALSE}
summary(modeleAICcout)
```


# Prédiction

## Prédiction sur le nombre de sinistres 

Pour un GLM Poisson on sait que : 

$log(E[Y_i|X_i])=X_i^T\beta$


Ainsi la prédiction est égale à : 

$\hat{Y_i} = E[Y_i|X_i] = exp(X_i^T\beta)$


La prédicition nous donne les statistiques suivantes :

```{r echo=FALSE}
prediction <- predict(modeleAIC, basefreq_test, type="response")
summary(prediction)
```

```{r echo=FALSE}
summary(basefreq_test$sinistretotal)
summary(basefreq_train$sinistretotal)
```

Ici, la première ligne nous donne les résultats de la base test et la seconde celle de la base train.

On en conclut que notre modèle n'est pas un bon modèle dû aux différences des résultats 

Il est possible que le nombre de variables explicatives du GLM était trop nombreux et a donc entrainé un phénomère de sur apprentissage. En effet, si l'on compare les résultats de la prédiction à celui de la base train, nous constatons que la moyenne et le 3ème quartile sont proches

## Prédiction sur le montant de sinistres 

Pour un GLM Gamma on sait que : 

$1/(E[Y_i|X_i])=X_i^T\beta$


Ainsi la prédiction est égale à : 

$\hat{Y_i} = E[Y_i|X_i] = 1/X_i^T\beta$

La prédicition nous donne les statistiques suivantes :

```{r echo=FALSE}
prediction2 <- predict(modeleAICcout, basecout_test, type="response")
dif<-abs(prediction2-basecout_test$coutsinistre)
summary(dif)

```

Ici on calcule la différence entre les coûts des sinistres estimés par la prédiction et les coûts réels des sinistres de la base test.
Nous remarquons que la prédiction surestime d'en moyenne 353 euros le montant des sinistres.

```{r echo=FALSE}
summary(basecout_test$coutsinistre)
```

Ici, nous affichons le coût des sinistres de la base test. Nous constatons que la prédiction surestime de plus de la moitié le coût réel du sinistre. C'est peut-être une approche un peu trop prudente pour l'assureur.
On en conclut, que le modèle n'est pas optimal.

# Conclusion

En conclusion, nous nous sommes intéressées aux facteurs qui influent sur le montant de sinistre et sur leur nombre.

Pour cela, nous avons réalisé les deux GLM avec les variables explicatives suivantes : 

* **un GLM Poisson pour le nombre de sinistre ** : fréquence paiement, langue ,type profession ,alimentation, utilisation, présence alarme, sexe, age ,durée de permis, ancienneté du véhicule.

* **un GLM Gamma pour le coût des sinistres ** : langue, utilisation, présence alarme, sexe , ancienneté du véhicule

Nous avons procédé à une sélection de variables pour obtenir un modèle optimal et avons réalisé une prédiction pour vérifier nos résultats. Ceux-ci n'ont pas été aussi satisfaisants que nous l'espérions. Cela est peut-être dû à la base de données ou à la corrélation trop forte entre certaines variables.

Néanmoins, nous avons pu répondre à notre problématique et avoir une meilleure idée des facteurs pris en compte dans la tarification automobile.


