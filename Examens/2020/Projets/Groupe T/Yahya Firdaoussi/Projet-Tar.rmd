---
title: "Tarification d’un contrat d’assurance dommage"
author: "Firdaoussi Yahya / Benmessaoud Leila/ EL Ouahdani Hamza"
date: "Avril 2020"
output:
  prettydoc::html_pretty:
    theme: cayman
    highlight: github

---
```{r, echo=FALSE}
knitr::opts_chunk$set(error = TRUE)
library(MASS)
library(survival)
library(lsei)
library(npsurv)
library(fitdistrplus)
library(caTools)
```
# <span style="color:blue">1. __Introduction__:</span>  

L'objectif de ce projet est de proposer un modèle pour tarifier les contrats d’assurance non vie. Cette tarification passe par une première étape primordiale à savoir le calcul de la **prime pure**. Cette prime doit être calculée d'une façon à protéger, de la meilleure façon possible, les assurés et l'assureur. Sur la base des informations historiques,la compagnie d’assurance doit estimer deux quantités :

- **La fréquence** : le nombre de sinistres divisé par l’exposition (qui est d’un an dans notre cas), soit la variable aléatoire notée N.

- **Le montant des sinistres par contrat** : la somme que reçoit l’assuré de l’assureur pour un sinistre, soit la variable aléatoire $Y_i$. Les contrats sont supposés indépendants et identiquement distribués (iid).

On suppose que les variables N et $Y_i$ sont indépendantes (bien que cela soit contestable en réalité), afin de calculer la prime pure qui est ;  

<div align="center">$E(S)=E(N).E(Y)$


<div align="left">Cette approche n’est pas suffisante, car bien souvent il y a de l’hétérogénéité parmi les assurés.
Afin de la capturer, la tarification est réalisée à partir de la formule suivante :

<div align="center">$E(S/X)=E(N/X).E(Y_i/X)$      

<div align="left">où $X$ désigne un ensemble de variable explicatives.


On cherche donc à modéliser $E(N/X)$ et $E(Y_i/X)$, où $N$ est la variable qui représente le nombre de sinistres,$Y$ le coût des sinistres par contrat et $X$ des variables explicatives associées à l’assuré, à son contrat et au véhicule assuré. Une fois ces quantités calculées, on pourra calculer la prime pure à l’aide de la première formule .


## <span style="color:blue">2. __Données et traitement préliminaire__ :</span>  

### 2.1 Données

Nous disposons de deux bases de données d'une assurance relatives au produit "Contrat d'assurance automobile Responsabilité Civile"

La première contient des informations concernant les différents sinistres déclarés et qui ont fait l'objet d'une indemnisation. On y trouve: numéro d’assuré, nombre de sinistre, coût du sinistre. L'autre base de données contient des informations sur les assurés : leurs caractéristiques (sexe, âge, ancienneté du permis), le véhicule (PF, combustion).

Avant d'utiliser ces informations, nous avons fait une jointure sur Excel pour lier les deux bases en se basant sur le numéro de Police.

Finalement, nous démarrons notre travail avec la base suivante:

```{r echo=FALSE}
#Merci de placer le code et la base de données dans votre répertoire
data=read.csv("DataSet.csv",sep=";",dec=".")
attach(data)
head(data)
```
### 2.2 Traitement préliminaire

Afin d’améliorer la qualité de nos données et donc de nos résultats, nous procédons à un traitement de notre base de données. Nous allons premièrement supprimer les lignes où il y a des valeurs manquantes, et celles qui présentent des informations inhomogènes (Charge de sinistre négative, expositions négatives ...).

```{r echo=FALSE}
sapply(data,function(x) sum(is.na(x)))
data=data[!apply(data, 1, function(x) any(is.na(x))) ,]
data$Zone=as.factor(data$Zone)
data$Comubsution=as.factor(data$Comubsution)
data$Puissance.Fiscale=as.factor(data$Puissance.Fiscale)
data$Frequence=as.numeric(data$Frequence)
data$Anciennete.permis=as.numeric(data$Anciennete.permis)
data$Frequence=as.numeric(data$Frequence)

data=subset(data,exposition>=0)

summary(data)

round(table(data$Nombre.de.sinistre)/sum(table(data$Nombre.de.sinistre)),5)


```
### 2.3 Base de test et d’entraînement:

Pour être capable de tester les modèles mathématiques, nous allons séparer notre base finale en deux partie:


- une base d’entraînement (80%): où nous allons faire la modélisation

- une base de test (20%): où nous allons tester les modèles 


```{r echo=FALSE}

smp_size <- floor(0.8 * nrow(data))
set.seed(123)
train_ind <- sample(seq_len(nrow(data)), size = smp_size)

train <- data[train_ind, ]
test <- data[-train_ind, ]

```

## 2.4 Modèle de Poisson : détection de l’hétérogénéité


La loi de poisson permet de non seulement modéliser  des événements à occurences faibles, ce qui est adapté à notre varible nombre de sinistre (qui prends des valeurs entre 0 et 6), mais aussi d'évaluer l’hétérogénéité. Dans cette section, nous analysons l’ajustement de nos données pour la base de données d'entraînement (ici notre variable N : compte le nombre de sinistres) au modèle de Poisson.

```{r echo=FALSE, fig.align='center'}

#Pour cela, on commence par estimer la moyenne des comptages :
lambda=mean(train$Nombre.de.sinistre)

 #On simule des comptages selon une distribution de Poisson de paramètres Lambda.

set.seed(1234) # permet de simuler toujours les mêmes comptages.
theoretic_count =rpois(nrow(train),lambda)

# on incorpore ces comptages théoriques dans un data frame
tc_df =data.frame(theoretic_count)

# on plot simultanémaent les comptages observés et les comptages théoriques

probpoiss=dpois(0:6, lambda=mean(train$Nombre.de.sinistre))

FreqObs=table(train$Nombre.de.sinistre)/nrow(train)
a=rbind(FreqObs,probpoiss)
X11()
barplot(a, 
        beside = TRUE,
        legend.text = c('Fréquence observée', 'Fréquence loi poisson'),
        ylim = c(0, 0.7), col=c("dark green","blue"))
text(seq(1.5, by = 3, length.out = length(FreqObs)),
     FreqObs+0.05,
     labels = paste(as.character(round(FreqObs*100,1)), '%'),
     cex=0.7)
text(seq(2.5, by = 3, length.out=length(probpoiss)),
     probpoiss+0.05,
     labels = paste(as.character(round(probpoiss*100,1)), '%'),
     cex=0.7)
# ou on peut utiliser cette fonction qui calcul la densité et la FDR empirique et theorique
plot(fitdist(train$Nombre.de.sinistre,"pois"))

```

### Conclusion
Nous remarquons que la distribution ajuste bien la variable Nombre de sinistre.
La variable Nombre de sinistre suit donc une distribution de Poisson de paramètre $\lambda = 0.711$.

## <span style="color:blue">3. Modélisation de la fréquence des sinistres : $E(N|X)$:</span> 

L’objectif de cette partie est de modéliser la fréquence annuelle des sinistres. Pour cela nous allons utiliser les modèles linéaires généralisés. 

Nous avons décidé d'implémenter un GLM avec une distubition de poisson, et un GLM avec une distribution binomiale négative. Ces deux distributions sont les plus adaptées pour ce type de variable et permettent de prendre en considération une éventuelle sur-dispersion. Le meilleur (en terme de performance) des deux modèles sera retenu.

Dans cette optique, nous allons commencer par choisir puis traiter les variables tarifaires associées à l’assuré, sa police d’assurance ainsi que son véhicule.

### 4.1 Choix des variables tarifaires

__Test d’indépendance du khi-deux __ : 


Dans un premier lieu, nous allons voir le niveau de corrélation des variables explicatives avec la variable Binaire ( Déclaration ou non d'un sinistre).
Nous allons évaluer la statistique du khi-deux pour tester 

$H_0$ :  la variable observée est indépendante de la variable « déclaration de sinistre Oui/Non »)


vs


$H_1$: Les deux variables sont dépendantes.


et on donne la p-value du test associé.
```{r echo=FALSE}
Sin_Oui_Non=rep(0,nrow(train))

train=cbind(train,Sin_Oui_Non)

for (i in 1:nrow(train)){
  if (train[i,5]==0){
    train[i,11]=0
  }
  else{
    train[i,11]=1
  }
}
train[,11]=as.factor(train[,11])

table(train[,11],train[,1])

chisq.test(train[,1],train[,11],correct=FALSE)
#Le test de khi deux montre que la seule variable dependante du nombre de sinistres est le sexe mais on va garder l'ensemble des variables et on va procèder après à une sélection des variables les plus pertinantes

```
### 4.2 Choix de la famille de loi dans le modèle GLM


La régression de poisson se base sur l’hypothèse d’équi-dispersion (une variance constante pour toutes les classes). Cette dernière est forte et peut s’avérer très contraignante.


Nous allons implémenter deux GLM, et choisir celui qui présente les meilleurs résultats

#### 4.2.1 __Procédure suivie__ :

Généralement, deux méthodes opposées sont citées pour sélectionner des variables :

- L’algorithme forward : méthode ascendante qui consiste à partir du modèle constant en ajoutant une à une des variables au modèle.


- L’algorithme backward : méthode descendante qui consiste à partir du modèle comprenant toutes les variables explicatives puis de retirer les moins pertinantes une à une. 



Pour se faire on utilise la fonction glmulti() qui effectue un algorithme itératif pour retrouver le meilleur modèle.

#### 4.2.2  __Résultats GLM Poisson__ :

Nous allons commencer par un GLM Poisson. Premièrement, nous allons sélectionner les variables explicatives puis implémenter le modèle.

```{r echo=FALSE}
library(rJava)
library(glmulti)
#Régression de Poisson
model=glmulti(Nombre.de.sinistre~Sexe+Anciente.du.vehicule+Anciennete.permis+Comubsution+Puissance.Fiscale+offset(log(exposition)),data=train,level=1,method="h",fitfunction=glm,family=poisson(),crit="aic",plotty=F)
summary(model)$bestmodel


```



Cet algorithme sélectionne le meilleur modèle au sens d'un critère donné. Dans notre cas, c'est le modèle suivant:


```{r echo=FALSE}

summary(model)$bestmodel


```

Notons que ce résultat est en parfaite cohérrence avec le test de corrélation de khi-deux que nous avons effectué.

Ainsi, nous gardons le modèle suivant, pour un GLM poisson:
```{r echo=FALSE}

model_pois=glm(Nombre.de.sinistre~Sexe+offset(log(exposition)),data=train,family=poisson())           

#Evaluation de la surdispersion
summary(model_pois)
#Le ratio residual deviance / ddl est égal à 9481.3 /7653, soit 1.24.Ce ratio est près de 1 et permet de mettre en évidence l'absence d’une surdispersion. 

```

#### 4.2.3 __Résultats GLM Binomial négatif__ :
 
 Dans une démarche similaire, nous appliquons le même algorithme pour sélectionner les variables tarifaires, puis implémenter le modèle.
 
En utilisant la même fonction sur R. Nous trouvons que:
 
 
```{r echo=FALSE, warning=FALSE}
#Régression Binomiale Négative

model=glmulti(Nombre.de.sinistre~Sexe+Anciente.du.vehicule+Anciennete.permis+Comubsution+Puissance.Fiscale+offset(log(exposition)),data=train,level=1,method="h",fitfunction=glm.nb,
              crit="aic",plotty=F)

```

Ainsi, le meilleur modèle est:

```{r echo=FALSE, warning=FALSE}

summary(model)$bestmodel

```

Donc, pour la loi binomial négative, le modèle optimal est:

```{r echo=FALSE, warning=FALSE}

model_nb=glm.nb(Nombre.de.sinistre~Sexe+offset(log(exposition)),data=train)

```




#### 4.4.4 __Comparaison et choix final__ :



```{r echo=FALSE, warning=FALSE}


# comparaison entre les deux modeles Pois et BN

Model=c("deviance","AIC")
GLM_Poisson=c( model_pois$deviance,model_pois$aic)
GLM_BN=c( model_nb$deviance,model_nb$aic)
Recap=cbind(Model,GLM_Poisson,GLM_BN)
Recap=as.table(Recap)
 knitr::kable(Recap, caption = "AiC et déviance du chaque modèle")
#On remarque qu'ils sont presque le meme AIC mais le modèle Binomiale négative à une deviance plus base que le modèle de poisson.Le	choix	final	est	porté	sur	la binomiale	négative

```



 Les déviances étant très proches, nous décidons de garder le modéle GLM Binomial négatif comme il possède l'AIC la plus petite.

## <span style="color:blue"> 5. __Modélisation des coûts des sinistres : $E(Y_i|X)$__ :</span> 

### 5.1 Traitement préliminaire:

Pour calculer la prime pure, il faut aussi modéliser le coût des sinistres, dit sévérité. Les assurés n’ayant pas eu de sinistre, ont un coût associé de 0. Il faut supprimer ces observations pour modéliser la sévérité. En effet, la non sinistralité de ces individus est capturée dans la modélisation de la frèquence. Pour ne pas biaiser les concepts, et pour modéliser la charge des sinistres, il est donc nécessaire de supprimer les observations où la charge est nulle.

Par la suite, nous gardons que les observations dont les coûts sont strictement positifs. On remarque la présence de coûts extrêmes (supérieurs à 50 000 Euros). Afin de minimiser leur impact, on utilisera un modèle log-normal pour la prédiction.

Une première étude statistique sur la variable coût nous permet de mieux connaître la distribution des coûts de sinistres.

```{r echo=FALSE}
hist(train$Charge, 
     col = c("orange"),
     main = paste("Répartition des couts des sinistres"),
     ylab = "Frequency",
     xlab = "Cout")
```

### 5.2 __Modélisation de la charge__ :

En procédant comme dans le cas de la fréquence avec une comparaison entre la régression log normal et Gamma.

Nous allons donc commemcer par choisir le meilleur GLM en faisant une sélection des variables pour chacune des deux distrubtions, puis choisir le meilleur GLM des deux.

#### 5.2.1 __GLM loi log normal__ :

Nous allons premièrement sélecionner les meilleures variables explicatives en utilisant la même fonction "glmulti". Nous trouvons:
```{r warning=FALSE}
#Régression lognormal

train2=subset(train,Charge!=0)
model=glmulti(log(Charge)~Sexe+Anciente.du.vehicule+Anciennete.permis+Comubsution+Puissance.Fiscale,data=train2,level=1,method="h",fitfunction=glm,
            crit="aic",plotty=F,family=gaussian())


```


Ainsi, le meilleur modèle est:

```{r echo=FALSE, warning=FALSE}

summary(model)$bestmodel

```

 Nous remarquons d'après l'algorithme itératif utilisé par la fonction "glmulti" que le meilleur modèle est celui avec l'intercept seulement ( juste avec la constante).De ce fait, nous éliminons ce GLM de notre choix final.


#### 5.2.2 __GLM loi Gamma__ :


Nous allons premièrement sélectionner les variables explicatives en utilisant la même fonction "glmulti". Nous trouvons:

```{r echo=FALSE}
#Loi gamma
train2=subset(train,Charge!=0)
model=glmulti(Charge~Sexe+Anciente.du.vehicule+Anciennete.permis+Comubsution+Puissance.Fiscale,data=train2,level=1,method="h",fitfunction=glm,
              crit="aic",plotty=F,family=Gamma)


```

Ainsi, le modèle optimial dans ce cas, est:

```{r echo=FALSE}
#Loi gamma
summary(model)$bestmodel
```

Implémentant ce modèle, nous trouvons:


```{r echo=FALSE}

model_gamma=glm(Charge~Anciente.du.vehicule+Anciennete.permis,data=train2,family=Gamma)           

```

#### 5.2.3 __Comparaison et choix final__ :



```{r echo=FALSE, warning=FALSE}


# comparaison entre les deux modeles Pois et BN

Model=c("deviance","AIC")
GLM_gamma=c( model_gamma$deviance,model_gamma$aic)
Recap=cbind(Model,GLM_gamma)
Recap=as.table(Recap)
 knitr::kable(Recap, caption = "AiC et déviance du chaque modèle")
 

```

Nous décidons alors de garder le modèle Gamma qui possède de bonnes proprietés.

## <span style="color:blue"> 6 Prédiction sur la base de test :</span> 
Dans cette partie, nous utilisons la base de test pour mesurer les écarts entre les valeurs prédites et les valeurs observées.Pour se faire, on utilisera le RMSE et MSE.

```{r echo=FALSE, warning=FALSE}
Predict_fre=predict.glm(model_nb,test,type="response")
Predict_Char=predict.glm(model_gamma,test,type="response")
library(Metrics)


Model=c("Fréquence","Charge")
RMSE=c( rmse(test[,10],Predict_fre),rmse(test[,6],Predict_Char))
MSE=c( mse(test[,10],Predict_fre),mse(test[,6],Predict_Char))
Recap=cbind(Model,RMSE,MSE)
Recap=as.table(Recap)
 knitr::kable(Recap, caption = "RMSE ET MSE entre les données de test et les données prédites pour les modèles retenus")
```

On voit que le MSE dans notre cas est élevé cela peut etre du au fait que  si nous avons des données bruyantes (c'est-à-dire des données qui peuvent prendre des valeurs extrêmes ou parfoit nulles) même un modèle «parfait» peut avoir un MSE élevé dans cette situation.Donc si nous faisons une seule très mauvaise prédiction, la quadrature aggravera l’erreur et risque de biaiser la métrique et donc surestimer l’erreur de prédiction de notre modèle.

## <span style="color:blue"> 7 Conclusion et estimation de la prime pure :</span> 

Afin d’estimer la prime pure pour les assurés dont les données sont regroupées dans la base de souscription, on regroupe les estimations obtenues pour la fréquence des sinistres $E(N/X)$ et le coût des sinistres $E(Y_i|X)$. On multiplie ensuite, contrat par contrat, ces deux quantités pour obtenir la prime pure par contrat.
