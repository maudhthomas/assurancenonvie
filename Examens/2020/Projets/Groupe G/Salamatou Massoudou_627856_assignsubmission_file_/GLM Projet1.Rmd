---
title: "Assurance non vie - Projet GLM"
author: 'Grégory-Raphael-Salamatou '
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
 prettydoc::html_pretty:
    theme: cayman
    highlight: github
---
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE)
```

##Introduction

**L'assurance catastrophe naturelle** est une assurance qui permet d'être indemnisé pour les dégâts dus aux catastrophes naturelles (*tremblement de terre, inondations, sécheresse, glissement de terrain, action mécanique des vagues...*). Celle-ci est incluse dans l'assurance multirisques habitation.  
Dans le but d'aider l'assureur à quantifier ces risques pour ainsi établir une tarification adaptée ou un provisionnement, les données historiques de catastrophes naturelles sont d'une importance capitale.  
C'est dans ce cadre que nous allons réaliser une étude, sur des données historiques de catastrophe naturelles en Australie, visant à établir différents modèles linéaire généralisés (GLM) pour modéliser le montant des sinistres.

##Description du jeu de données

Nous allons considérer les données historiques de catastrophes naturelles en Australie de 1967 à 2014. Elles sont réparties dans 9 colonnes dont les descriptions sont données ci-dessous:

- *Year*: l'année
 - *Quarter*: le trimestre pendant lequel le sinistre a eu lieu
 - *FirstDay*: le premier jour de la catastrophe
 - *LastDay*: le dernier jour de la catastrophe
 - *Event*: description de la catastrophe
- *Type*: type de la catastrophe
- *Location*: le lieu
- *OriginalCoast*: Coût d'origine du sinistre en millions de dollars australiens
- *NormCost2011*: Coût normalisé du sinistre en millions de dollars australiens de
2011 en tenant compte de l'inflation, de l'évolution de la richesse et de
la population
- *NormCost2014*: Coût normalisé du sinistre en millions de dollars australiens de
2014 (AUD) calculé comme le coût NormCost2011 utilisant CPI.
Nous avons cette première appercue des données:

```{r, echo=FALSE}
load("~/auscathist.rda")
donnees <- data.frame(auscathist)
head(auscathist)
```

Etant donnée qu'au cours de l'étude, nous n'auront  besoin ni des dates de début, ni des dates de fin de la catastrophe, ni de la description "event" du sinistre (celle-ci étant déja dans "type"), ni de la localisation, il nous a paru judicieux de les retirer de la base de données. Il s'agit en effet de modéliser le cout de ces catastrophes.  
Nous tenterons ainsi de répondre à la question suivante :  

**La nature exacte de la catastrophe et le moment de survenance ont-elles une influence significative sur le cout ?**   



##Appréciation des variables

Nous allons d'abord observer la répartition générale de nos données à travers les occurences par type de catastrophe et les tendances centrale de celles-ci.
Ces tendances centrales peuvent être utiles à l'assureur pour se faire une première idée du coût moyen des sinistre.

```{r, echo=FALSE}
data <- data.frame(auscathist)
type = data$Type
table(type)
```
On voit que les tempêtes, sont bien plus fréquentes (avec une survenance de 54) que les tremblements de terre ( occurence = 4) ou les tornades (occurence = 1). Ce qui nous permet de considérer l'occurence comme étant un facteur important dans les prévisions de provisionement pour les catastrophes naturelles.


```{r, echo=FALSE}
type = ifelse(type=='Bushfire', 'A',
              ifelse(type=='Cyclone', 'B',
                     ifelse(type=='Earthquake', 'C',
                            ifelse(type=='Flood', 'D',
                                   ifelse(type=='Flood, Storm', 'E',
                                          ifelse(type=='Hailstorm', 'F',
                                                 ifelse(type=='Other', 'G',
                                                        ifelse(type=='Storm', 'H', 'I') ) ) ) ) ) ) )

Data = data.frame(data$OriginalCost,data$Year, data$Quarter,type)
summary(Data)

```
Concernant le coût moyen des sinistre, il serait plus adapté de considérer les coûts moyens par type de catastrophe. Ce que nous retrouvons ci-dessous:


```{r, echo=FALSE}
library(dplyr)
```

```{r, echo=FALSE}
gp<-group_by(auscathist,Type)
dt<-summarise(gp,mean=mean(OriginalCost,na.rm = TRUE))
dt
```
Ainsi, on observe que le coût moyen pour un tremblement de terre, qui est de 226.05 millions de dollars, est bien plus conséquent que le coût moyen pour les tempêtes qui est de 131.06 millions. Ce qui nous montre déja l'influence du type de la catastrophe sur le prix des sinistres.

Regardons à présent les occurences pour chaque trimestre:
```{r, echo=FALSE}
table(data$Quarter)
```

D'après ces résultats, les catastrophes ont donc tendance à survenir durant le premier ou le dernier trimestre de l'année : 95 durant le premier trimestre, contre 15 durant le troisième.

Les occurrences par année sont :
```{r, echo=FALSE}
table(data$Year)
```

Pour l'année aussi, on constate qu'en 1998, il y a eu plus de catastrophe naturelles que pendant les autres années, en l'occurence 13. Pour avoir une idée de quelle type de catastrophe il y a eu en 1998, nous présentons le tableau suivant:
```{r, echo=FALSE}
table(data$Type, data$Year == "1998")
```
Le tableau ci-dessus montre, par type, le nombre de fois qu'une catastrophe donnée a eu lieu au cours de l'année 1998. Ainsi, nous pouvons voir que durant l'année 1998, l'Australie a surtout connue des tempêtes et des innondations liées à celles-ci.


##Modélisation 

L'objectif de cette partie est de modéliser le coût des sinistres (OriginalCost) en fonction des autres variables (type, trimestre, année), dans le but d'évaluer la dépendance du coût par rapport à ces facteurs. Nous n'utiliserons ni NormCost2011, ni NormCost2014 car nous ne les connaitront qu'après.  

Dans un premier temps, nous allons réaliser cette modélisation à partir d'un GLM (modèle linéaire généralisé) Gamma. Nous choisissons de construire un GLM Gamma car la variable à modéliser est une variable positive et continue.

```{r, glm 1, echo=FALSE}

fit1<-glm(formula = data$OriginalCost ~ data$Year +type + data$Quarter, data = Data, family=Gamma(link='log'))

summary(fit1)

```

D'après la sortie, la déviance résiduelle est de 423,75. La déviance, elle, vaut 638,29. Cela est une mesure qui permet d'indiquer un écart au modèle qui comprend toutes les variables. 
On peut aussi prendre en note le critère d'information d'Akaike (AIC). Il permettra de comparer nos modèles et déterminer le meilleur modèle.

On peut aussi faire une Anova :
```{r, echo=FALSE}
library("car")
Anova(fit1  , test = "LR")
```
D'après les p-values obtenues on pourrait enlever data$Quarter de notre modèle. À priori, ce n'est pas une variable significative.  

On calcule aussi les Critères d'Information Bayésien (BIC) pour voir on si on doit bien l'enlever et entrer plus en détail :
 
```{r, echo=FALSE}
library(leaps)
choix <- regsubsets(data$OriginalCost~data$Year+data$Quarter+ type, data=Data,nbest=6)
plot(choix, scale = "bic")
```


D'après notre BIC ci dessus, data$Quarter pourrait bien être retiré. On pourrait aussi retirer certains types de catastrophes qui se produisent trop peu souvent, sans que cela influe significativement sur le résultat. On pourrait par exemple citer le type H qui correspond aux tempêtes.

À présent, nous allons procéder à un deuxième GLM, le comparer avec le premier et ainsi voir lequel des deux est plus adapté. On va faire ce GLM avec une fonction de lien différente de celle du premier:

```{r, echo=FALSE}

fit2<-glm(formula = data$OriginalCost ~ data$Year  + type + data$Quarter, data = Data, family=Gamma(link='identity'))

summary(fit2)
```

Comme pour le premier, on regarde la déviance résiduelle qui vaut 490,81. La deviance, elle, vaut 638,29.

La déviance est la même que dans le premier cas. On compare donc la déviance résiduelle. Elle est plus faible dans le premier cas. En effet elle vaut 423,75 contre 490,81 pour notre deuxième GLM. Notre fonction de lien est donc mieux adaptée dans le premier cas. 

On relève aussi la mesure de l'AIC que l'on garde pour la suite.

On fait aussi une Anova pour ce modèle :

```{r, echo=FALSE}
Anova(fit2  , test="LR")
```
De la même manière que pour le premier GLM, on garderait data$year, ainsi que type même si sa p-value est assez élevée. Réaliser un GLM sans cette variable nous semble en effet être une mauvaise idée.
 
## Sélection de modèle

```{r, echo=FALSE}
fit1$aic
fit2$aic
```
 
 On récupère les deux AIC que l'on a calculé. Le plus faible est celui correspondant au premier GLM. Cela confirme bien ce que nous avions dit. Le premier modèle est donc celui que l'on retiendra.
 
On va d'abord se baser sur le meilleur modèle GLM que l'on puisse avoir avec nos variables : 
 
```{r, echo=FALSE}
step(fit1)
```
 On choisit donc le modèle qui permet d'expliquer OriginalCost en fonction de type et de data$Year car c'est celui qui a l'AIC le plus faible. C'est bien celui que nous avions au préalablement choisit.

## Conclusion

Avec le critère de comparaison AIC, nous avons déduit que le coût (OrginalCost) dépend bien du Type et de l'année (data$Year). 
Toujours dans le but d'aider à la tarification par les assureurs, nous aurions, par exemple, pu faire un zonier afin de se faire une idée des zones les plus touchées par ces catastrophes, et ainsi voir que certaines zones sont touchées par un type de catastrophe en particulier. Ce qui pourrait aider au niveau des estimations des provisions en assurance de catastrophe naturelle. Cependant, pour cela, il aurait fallut qu'on ait les coordonnées géographiques pour chaque catastrophe. Cela pourrait aussi permettre d'adapter l'offre selon la région, ce qui semble aussi être un critère pertinent.
