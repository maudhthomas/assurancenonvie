library(MASS)
library(car)
library(pscl)
library(readxl)
library(Hmisc)
library(vcd)

          ##Chemin du Projet de travail (Merci d'inserer le votre) ##
getwd() 
setwd("C:/Users/ANAS/Desktop/Cours_ISUP/Ann�e_N/S2/Assurance_Non_Vie/Projet/Projet Auto/Tarification_a_Priori")

data=data.frame(read_excel("Data_R.xlsx",sheet="Base Def"))
class(data)
head(data,n=5)

                   ## Statistiques ##
Hmisc::describe(data$Nbr_Sin) ##53% des contrats d'assurance sont non sinistrés##
Hmisc::describe(data$Sexe) ## 60% des conducteurs sont des Hommes##

                 ##Mod�lisation FM et CM des sinistres Auto##
                 ##Analyse distribution + Mod�lisation##
                 ##FM##
Nbre_Sin=data$Nbr_Sin
head(Nbre_Sin)
table_sin=table(Nbre_Sin)
table_sin
y=table_sin/length(Nbre_Sin)
y
mean(Nbre_Sin)
var(Nbre_Sin) ## valeurs proches de la moyenne et la variance, on soup�onne une binomiale n�gative � la limite une poisson##
jpeg("Fr�quence des sinistres.jpeg")
barplot(table_sin,col="blue",main = "Fr�quence des sinistres")
dev.off()

                      ##negbin##

names(goodfit(Nbre_Sin,"nbinom"))
goodfit(Nbre_Sin,"nbinom")$fitted
par_nbin=goodfit(Nbre_Sin,"nbinom",par=list(size=3))$par
par_nbin

jpeg("Ajustement par la loi binomiale n�gative.jpeg")
plot(goodfit(Nbre_Sin,"nbinom",par=list(size=3)),main="Ajustement par la loi binomiale n�gative") 
dev.off()  
                    ##test de Chi-deux##
length(Nbre_Sin)*c(dnbinom(0:6,par_nbin$size,par_nbin$prob))              

nbin_theo=c(dnbinom(0:5,par_nbin$size,par_nbin$prob),1-pnbinom(5,par_nbin$size,par_nbin$prob))
nbin_theo
empiri_nbin=vector()
empiri_nbin[1:6]=y[1:6]
empiri_nbin[7]=1-sum(y[1:6])
empiri_nbin

names(chisq.test(length(Nbre_Sin)*empiri_nbin,p=nbin_theo))
chisq.test(length(Nbre_Sin)*empiri_nbin,p=nbin_theo) 

##==> Il s'agit bien d'une distribution binomiale n�gative ##

                    ##GLM##


##Pr�ciser que pour �viter qu'une �ventuelle colin�arit� att�nue l'effet d'une autre variable explication, on a proc�d� par m�thode de Forward##
fit_nbin=glm.nb(Nbre_Sin~Sexe+Tranche_Conduc+Tranche_Veh+Zone,data=data)
summary(fit_nbin)
anova(fit_nbin,test="Chisq")
fit_nbin=glm.nb(Nbre_Sin~Sexe,data=data)
summary(fit_nbin)
capture.output(summary(fit_nbin),file="mod�lisation FM.txt")

             ##CM##

Montant_Sin=data$Claim_Amount
head(Montant_Sin)
Montant_Sin_Pond=Montant_Sin/(Nbre_Sin)
Montant_Sin_Pond[is.nan(Montant_Sin_Pond)]<-0 
head(Montant_Sin_Pond)
summary(Montant_Sin_Pond)
##plus de 50% de la population ont un cout Moyen par sinistre Nul et donc On a affaire � une mod�lisation � deux types de sinistres ##
                   ##ad�quation du vecteur des couts non nul##
ind=which(Montant_Sin_Pond>0)
Montant_Sin_Pond1=rep(0,length(ind))
Montant_Sin_Pond1=Montant_Sin_Pond[ind] 
head(Montant_Sin_Pond1,n=15)

names(fitdistr(Montant_Sin_Pond1,"exponential"))
fitexpo=fitdistr(Montant_Sin_Pond1,"exponential")$estimate
fitexpo
fitlognor=fitdistr(Montant_Sin_Pond1,"lognormal")$estimate
lognor
x=Montant_Sin_Pond1

jpeg("Fonction de r�partition du cout Moyen de sinistres.jpg")
plot(ecdf(x),ylab="",xlab="Montant de sinistres",main="Fonction de r�partition du cout Moyen de sinistres",lty=1)
curve(pexp(x,fitexpo),col="red",add=TRUE,lwd=2,lty=2)
curve(plnorm(x,fitlognor[1],fitlognor[2]),col="blue",add=TRUE,lwd=2,lty=3)
legend(6000,0.35,legend=c("Fonction de r�partition empirique","Fonction de r�partion exponentielle","Fonction de r�partition lognormale"),col = c("black","red","blue"),lty=1:3,cex = 0.7)
dev.off()

ks.test(x,plnorm,fitlognor[1],fitlognor[2])
s.test(x,pexp,fitexpo)

##Aucune des lois n'ajuste notre distribution, sauf que la loi lognormale s'av�re la plus proche) 

                    ##GLM##
Montant_Sin
Cout= rep(0,length(ind))
Cout=Montant_Sin[ind]
Sexe1=data$Sexe[ind]
Tranche_Conduc1=data$Tranche_Conduc[ind]
Tranche_Veh1=data$Tranche_Veh[ind]
Zone1=data$Zone[ind]
Nbre_Sin1=Nbre_Sin[ind]
data1=data.frame(Cout,Sexe1,Tranche_Veh1,Tranche_Conduc1,Zone1,Nbre_Sin1)
head(data1)
class(data1)
CM=Cout/Nbre_Sin1
lCM=log(CM)



fit_gamma=glm(formula = CM~Sexe1+Tranche_Conduc1+Tranche_Veh1+Zone1,data=data1,family = Gamma())
summary(fit_gamma)
anova(fit_gamma,test="Chisq")
## Mod�le Gamma (exponentielle) ne colle pas##


fit_lnorm=glm(formula = lCM~Tranche_Conduc1,data=data1,family = gaussian())
summary(fit_lnorm)
anova(fit_lnorm,test="Chisq")
apture.output(summary(fit_lnorm),file="mod�lisation Cout Moyen.txt")
##Le mod�le Lognormal ne colle pas, pourtant il a le moins d'AIC, on supposera par la suite que la tranche du conducteur est explicative pour le cout (pour la continuit� de notre analyse)##

 ## Mod�lisation de la probabilit� d'avoir un sinistre de notre base de donn�e##

head(data)
bernoui=rep(0,length(Montant_Sin))
bernoui[ind]<-1
head(bernoui,n=20)
mean(bernoui)
data2=data.frame(data,bernoui)
class(data2)
head(data2,n=20)


fitbern=glm(formula=bernoui~Sexe+Tranche_Conduc,data=data2,family = binomial(link = "logit"))
summary(fitbern)
anova(fitbern,test="Chisq")
capture.output(summary(fitbern),file="mod�lisation probabilit� de sinistre.txt")
##La var explicative Sexe est significative par exc�llence, avec un degr� moins tranche d'age conducteur, pourtant on gardera les deux ##  



