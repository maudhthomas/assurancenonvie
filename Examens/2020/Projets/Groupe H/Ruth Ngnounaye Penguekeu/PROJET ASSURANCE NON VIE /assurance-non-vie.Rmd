---
title: "Econométrie de l'assurance non-vie"
author: "HAMMOND ISRAEL - NGNOUNAYE PALOCHE- ADOKVEPE PIERRE MARIE"
date: "24/03/2020"
output:
  pdf_document: default
  html_document:
    df_print: paged
subtitle: Modélisation de la prime pure en assurance santé GLM
includes:
  in_header:
  - \usepackage[utf8]{inputenc}
  - \usepackage[T1]{fontenc}
  - \usepackage[american]{babel}
  - \usepackage{graphicx}
  - \usepackage{xcolor}
  - \usepackage{lmodern}
  - \usepackage{amsmath}
  - \usepackage{amssymb}
  - \usepackage{mathrsfs}
  - \usepackage{tabularx}
  - \usepackage{cite}
  - \usepackage{placeins}
  - \usepackage{float}
---
\setlength{\parindent}{0.5cm}

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library("quantmod")
library(knitr)
library(kableExtra)
library(fBasics)
library(pander)
library(GGally)
library(ggplot2)
library(gridExtra)
library(gdata)
library(tseries)
library(fGarch)


library(magrittr) #pour utiliser l'op??rateur pipe %>%
library(data.table)
library(MASS)
library(rgdal)
library(maptools)
library(colorspace)
library(readr)
library(magrittr)
library(mfx)
```

\vspace*{\stretch{1}}
\tableofcontents
\vspace*{\stretch{1}}

# Introduction:

En assurance non-vie, le cycle de production inversée impose à l'assureur de déterminer à l'avance ce qu'un assureur pourrait lui couter en termes de sinistres. C'est pourquoi, selon le profil de risque de l'assureur, c'est-à-dire selon ses caractéristiques personnelles et les caractéristiques de son bien à assurer (age, nombre d'enfants,sex,etc...) et selon ses sinsitres passées éventuellement, l'assureur va tenter de déterminer le meilleur tarif pour ce contrat qui couvrira l'assureur pendant une annéee. Ce tarif s'appelle la prime commerciale. Elle est composée de ce qu'on appelle la prime pure, à laquelle s'ajoutent des chargements pour frais d'acquisition, de gestion et la marge. \newline

La prime pure est ce que va tenter de déterminer de manière robuste l'assureur car elle estime à elle seule le coût que va probablement représenter l'assuré à l'assureur. Biensûr, tous les assurés ne vont pas avoir de sinistre au cours de l'année, et le principe de la prime pure repose donc sur le principe de mutualisation du risque et la loi des grands nombres car on cherchera a estimer à la fois l'espérance du nombre de sinistre qu'un assuré aura au cours de l'année selon ses caractéristiques, et à la fois le coût moyen d'un sinistre pour cet assuré si toutefois un sinistre lui arrive. La prime pure pour chaque assuré sera donc le produit de ces deux variables.\newline

Pour estimer au mieux a posteriori ces deux variables, il faut procéder en trois temps :

\begin{itemize}
\item \textbf{Modélisation de fréquence }: pour estimer le nombre de sinistres. On utilisera un GLM avec soit une loi de Poisson, soit une loi de Quasi-Poisson, soit une loi binomiale négative.

\item \textbf{Modélisation de la sévérité} : pour estimer le montant moyen d'un sinistre. On utilisera un GLM avec une loi Gamma.

\item \textbf{Obtention de la prime pure} : en multipliant les variables modélisées dans les deux étapes précédentes.

\end{itemize}



L'intéret du GLM ("Generalized Linear Model") est sa simplicité d'utilisation et offre une plus grande flexibilité par rapport à une régression linéaire classique, et donc a de plus fortes chances de faire de meilleures prédictions.\newline

Dans le cadre de notre étude, nous disposons d'une base de données représentant un portefeuille d'assurés en assurance santé. Chaque observation concerne une police. Il y a donc pour chaque observation des informations concernant l'assuré. \newline

\textbf{Notre objectif est donc de déterminer pour chacun de ces assurés leur prime pure grâce à un GLM.}

# Base de données et analyse descriptive des données

La base de données est assurentielle, elle représente un portefeuille en risque santé.\newline

## Variables utilisées

\begin{itemize}
\item \textbf{age} : age of policyholder

\item \textbf{sex}:gender of policy holder (female=0, male=1)

\item \textbf{bmi}:Body mass index, providing an understanding
of body, weights that are relatively high or low relative to height, objective index of body weight (kg / m2) using the ratio of height to weight, ideally 18.5 to 25 steps: average walking steps per day of policyholder

\item \textbf{children}: number of children / dependents of policyholder

\item \textbf{smoker}: smoking state of policyholder (non-smoke=0;smoker=1)

\item \textbf{region}: the residential area of policyholder in the US (northeast=0, northwest=1, southeast=2, southwest=3)

\item \textbf{charges}: individual medical costs billed by health insurance

\item \textbf{insuranceclaim}: yes=1, no=0


\end{itemize}



Notons que nous avons pris en compte le sex de l'assuré car nous n'avions pas assez de variables. La réglementation en vigueur ne l'autorise pas. \newline

Voici un apperçu de la base de données à  disposition :

```{r, echo=FALSE}
f=file.choose()
data=read.csv(f, header = TRUE)
kable(head(data),format = "latex",booktabs = TRUE,caption = "Aperçu de la base de données") %>%
  kable_styling(position = "center",font_size = 4,latex_options = c("striped","HOLD_position"))
```


## Nettoyage des données 
Dans les modèles GLM, les variables explicatives sont qualitatives. Nous avons donc choisi de transformer nos variables quantitatives en variables qualitatives en construisant des classes. Nous les avons constituées en essayant de garder un équilibre dans les effectifs mais aussi, et surtout, une certaine pertinence.

```{r, echo=FALSE, message=FALSE, warning=FALSE}
#categorisation des variables 
data$age <- cut(data$age,
                breaks=c(-Inf, 27, 37,46,55, Inf),
                labels=c("18-27", "28-37","37-46","47-55","55+"))
data$bmi=cut(data$bmi, 
             breaks=c(-Inf,23.4,30,37,Inf),
             labels=c("15.9-23.4","23.5-30","30.1-37","37.1+"))
data$steps=cut(data$steps,
               breaks=c(-Inf,3500,5340,Inf),
               labels=c("2990-3500","3501-5340","5341+"))
data$newsteps <- factor(as.character(data$steps), levels = c("5341+", "2990-3500", "3501-5340"))


data$newbmi <- factor(as.character(data$bmi), levels = c("30.1-37", "15.9-23.4", "23.5-30","37.1+"))

data$newsex <- factor(as.character(data$sex), levels = c("1", "0"))



#base de donnees finale 
BDD <- subset(data,select=c(age,newsteps,newbmi,newsex,children,smoker,region,charges,insuranceclaim))

```

Pour ces variables catégorielles il est également nécessaire de bien choisir la classe de référence car cette dernière a un impact du le GLM. En effet, si la classe de référence (qui est prise en compte dans l’intercept du modèle) n’est pas la modalité la plus représentée, alors les autres modalités risquent de ne pas être suffisamment significatives et les coefficients associés risquent de ne pas être adaptés.


## Statistiques descriptives de notre base de données

\begin{center}
```{r, echo=FALSE, message=FALSE, warning=FALSE}
kable(summary(BDD), format = "latex",booktabs = TRUE,caption = "Statistiques descriptives des variables") %>%
  kable_styling(position = "center",font_size = 4,latex_options = c("striped","HOLD_position"))

```
\end{center}

# Modélisation GLM

## Sélection aléatoire de données d'apprentissage et de test

Nous souhaitons créer une base d'apprentissage pour les modèles GLM sur 75% de la base de données initiale. Nous réaliserons par la suite un test de notre modèle de prime pure sur les 25% restants de la base de données initiale. \newline
Nous faisons en sorte qu'il y ait proportionnellement le même nombre de sinistre  dans la base d'entrainement que dans la base de test.\newline

Ainsi, nous allons créer 4 bases : 
\begin{itemize}
\item 2 bases pour le modèle de fréquence : une pour le training, l'autre pour le test.
\item 2 bases pour le modèle de sévérité : une pour le training, l'autre pour le test.
\end{itemize}

L'apprentissage du GLM sera faite sur les bases de training, et les bases de test nous permettront de vérifier si nos prédictions sont efficaces ou non.\newline

A noter que nous allons retirer la variable prime (charges) pour le modèle de fréquence, et la variable de nombre de sinistres (insuranceclaim) pour le modèle de sévérité.

```{r, echo=FALSE, message=FALSE, warning=FALSE}
library(caret)
set.seed(123)
trainIndex <- createDataPartition(BDD$insuranceclaim,p=0.75,list=F)

# Base de training :
train <- BDD[trainIndex,]
print(c("dimension de la base de training : ", dim(train)))

# Base de test :
test <- BDD[-trainIndex,]
print(c("dimension de la base de test : ",dim(test)))

print(c("dimension de la base de BDD : ",dim(BDD)))

print(prop.table(table(train$insuranceclaim)))


```
## Partie I : modélisation de la fréquence

Une Quasi-poisson ne se rèleve pas efficace dans notre cas et d'ailleurs ne propose pas de critère d'AIC :

```{r, echo=FALSE, message=FALSE, warning=FALSE}
train_freq <- subset(train, select=-c(charges))


# Quasi Poisson : 

model_freq <- glm(formula = insuranceclaim ~ . , data = train_freq, family = quasipoisson)

# Poisson :
model_freq_poisson <- glm(formula = insuranceclaim ~ . , data = train_freq, family = poisson())

#Binomiale négative :

library(MASS)
model_freq_bn <- glm.nb(data = train_freq ,formula = insuranceclaim ~.)



# Tableau récapitulatif :
recap_perf <- data.frame(array(NA,dim=c(3,3)))
colnames(recap_perf)<-c("AIC","Déviance résiduelle","Score de Fischer")
row.names(recap_perf)<-c("Quasi Poisson","Poisson","Binomiale négative")

recap_perf$AIC <- c(model_freq$aic,model_freq_poisson$aic,model_freq_bn$aic)
recap_perf$`Déviance résiduelle` <- c(model_freq$deviance,model_freq_poisson$deviance,model_freq_bn$deviance)
recap_perf$`Score de Fischer` <- c(model_freq$iter,model_freq_poisson$iter,model_freq_bn$iter)
kable(recap_perf, format = "latex",booktabs = TRUE,caption = "Perfomance des modèles de fréquence GLM suivant différentes lois") %>%
  kable_styling(position = "center",font_size = 7,latex_options = c("striped","HOLD_position"))
```
On constate donc que les valeurs d'AIC et de déviances résiduelles sont très proches pour les lois de Poisson et Binomiale Négative. Nous allons comparer la moyenne de leurs résidus pour connaitre le modèle à choisir.

### Comparaison des deux modèles (Poisson et binomiale négative)


```{r, echo=FALSE, message=FALSE, warning=FALSE}
pred1<-predict.glm(model_freq_bn,test,type='response')

test$pred1<-predict.glm(model_freq_bn,test,type = 'response')

test$residu1<-test$pred1-test$insuranceclaim
res1=abs(mean(test$residu1))
# moyenne des résidus de la loi binomiale négative 
res1


pred2<-predict.glm(model_freq_poisson ,test,type='response')
test$pred2<-predict.glm(model_freq_poisson,test,type =  'response')
test$residu2<-test$insuranceclaim-test$pred2

# moyenne des résidus de la loi de poisson 
res2=abs(mean(test$residu2))
res2

#Différence entre les résidus de la loi binomiale et les résidus de la loi de poisson 
res=res1-res2
res
```

A partir de cette comparaison nous constatons que les deux modèles ont sensiblement une même moyenne des résidus en valeur absolue mais la moyenne des résidus la plus pétite est la moyenne des résidus provenant du glm de la loi de poisson . 
Nous allons donc continuer notre modélisation avec le glm de la loi de poisson 

### Modélisation par la loi de poisson  

Soit $Y_i$ le nombre de sinistres associé à l'assuré $i$, $X_{i}$ le vecteur colonne contenant les caractéristiques des variables explicatives de notre modèle associé è l'assuré $i$, et $\beta$ le vecteur colonne correspondant aux $p$ paramètres du modèle.\newline

Nous souhaitons déterminer, pour un assuré $i$ donné dont on connait les caractéristiques, l'espérance du nombre de sinistres qui le concernerait l'année à venir : $\mathbb{E}[Y_i| X_i]$. Nous avons vu que, dans notre cas, il vaut mieux utiliser une loi de poisson telle que :
\[Y|X \sim Poi^-(lamda)\]
avec $lamda   >0 . \newline

La fonction de lien utilisée dans notre cas est la fonction log : 
\[g(\mathbb{E}[Y_i| X_i])=\log(\mathbb{E}[Y_i| X_i])=X\beta\]

Intéressons nous maintenant aux résultats de la modélisation en fréquence.

### Analyse du summary

```{r, echo=FALSE, message=FALSE, warning=FALSE}
summary(model_freq_poisson)

```
 Après avoir fait tourner le GLM pour la modlélisation de la fréquence, nous constatons que la \textbf{"Null deviance"} (correspondant au modèle ne possédant qu'une constante) pour la loi binomiale de poisson   est egale à \textbf{622.59} sur 1003 degrés de liberté, tandis que la \textbf{"Residual deviance"} (correspondant au modèle avec les variables explicatives que nous avons imposé) est égale à \textbf{411.27} sur 990. Naturellement, plus on rajoute de variables explicatives, plus la deviance diminue car nous avons davantage d'informations pour prédire la fréquence.
                                                                En outre, nous constatons un \textbf{AIC de 1629.3}. Toutefois, ce score n'est utile que si nous le comparons à d'autres modèles de fréquence. Seul, il ne nous permet pas de déterminer si ce modèle est relativement bon ou pas.\newline
                                                              
La sortie du summary du GLM nous indique donc les coefficients à appliquer aux différentes variables catégorielles. Il faudra ensuite appliquer la fonction de lien log associée à la loi binomiale de poisson  pour retrouver l'espérance de la fréquence de sinistre associée à un profil d'assuré donné. On remarque aussi que les classes de références que nous avions pris soin de choisir précédement n'apparaissent pas dans le summary. Cela est dû au fait qu'elles sont directement intégrées dans l'intercept du modèle.\newline

Par exemple, si l'on se concentre sur la variable de l'age de l'assuré, la classe de référence pour cette variable est la classe allant de 18 ans à 27 ans. On constate que les autres catégories pour cette variable contribuent positivement à l'augmentation de l'espérance de la fréquence de sinistre pour un assuré mise à part la catégorie qui comprend les assurés compris entre 28 et 37 ans. Concrètement, si l'assuré possède un age compris entre  37 et 46, le coefficient associé est 0.19085. Sachant que la fonction de lien choisie est le logarithme, alors l'espérance pour cet assuré, toutes choses égales par ailleurs, sera multipliée par $\exp(0.19085) = 1.21029$.VCeci  montre bien que cette catégorie contribue à l'augmentation de la fréquence de sinistres. De la même manière on peut constater que la catégorie qui comprend  les assurés ayant un bmi compris entre 23.5 et 30   contribue à la diminution de la fréquence puisque le coefficient associé est de -0.28621 , et donc va multiplier l'espérance de fréquence par une valeur inférieure à 1. A noter que pour les assurés ayant un bmi compris entre 15.9 et 23.4 , le coefficient associé est encore inférieur, ce qui diminue davantage l'espérance de fréquence. On peut en déduire à juste titre que plus que qu'importe la valeur du bmi , moins il a de chance d'avoir un  sinistre.\newline

 
Rappelons ce que signifie la \textbf{Null deviance} : cette valeur indique la déviance d'un modèle GLM constitue uniquement d'une constante (c'est à dire l'intercept) par rapport à un modéle saturé. Rappelons la définition de la déviance dans ce cas : 
\[\Delta = 2.\left(LL(modèle ~ saturé)-LL(modèle ~ nul) \right) \]
avec $LL$ signifiant la log-vraisemblance. \newline

Il est utile de vérifier que le modèle qui explique le mieux le nombre de sinistres ne dérive pas d'un modèle GLM uniquement constitue d'un constante. Pour ce faire, nous savons que dans notre cas :
\[\Delta_{null} \underset{n \to +\infty}{\overset{\mathcal{L}}{\longrightarrow}} \chi^2_{n-p}\]
avec $n$ le nombre d'observations et $p$ le nombre de paramètres à estimer.\newline
                                                                  Nous pouvons donc tester l'hypothèse nulle suivante :
\[
 \mathcal{H}_0:\left\lbrace
       \begin{array}{l}
          \beta_1 = 0\\
          \beta_2 = 0\\
          ...\\
          \beta_{p} = 0\\
       \end{array}
 \right.
\qquad ; \qquad
\mathcal{H}_1:not \space \mathcal{H}_0
\]

```{r, echo=FALSE, message=FALSE, warning=FALSE}
p_value_null_poisson <- 1-pchisq(model_freq_poisson$null.deviance,model_freq_bn$df.null)
print(c("P-value associée à l'hypothèse nulle =",p_value_null_poisson))
```
 Ce test nous indique donc que nous ne pouvons pas rejeter l'hypothèse nulle, c'est à dire que le modèle constitue uniquement d'une constante peut tout à fait expliquer le nombre de sinistres.\newline

Par ailleurs, la \textbf{Residual deviance} indique la déviance du modèle GLM propose avec un certain nombre de variables explicatives par rapport au modèle saturé. Rappelons la définition de la déviance dans ce cas :
\[ \Delta = 2.\left(LL(modèle ~ saturé)-LL(modèle ~ proposé) \right) \]

Nous pouvons réaliser le test suivant :
\[
 \mathcal{H}_0: le ~ nombre ~ de ~ sinistres ~ peut ~ etre ~ explique ~ par ~ le ~ modele ~ propose
\qquad ; \qquad
\mathcal{H}_1:non \space \mathcal{H}_0
\]

```{r, echo=FALSE, message=FALSE, warning=FALSE}
p_value_prop_poisson <- 1-pchisq(model_freq_poisson$deviance,model_freq_bn$df.residual)
print(c("P-value associée à l'hypothèse nulle =",p_value_prop_poisson))
```
De même on ne peut pas rejeter l'hypothèse nulle : on peut considérer que le modèle proposé peut expliquer le nombre de sinistres. \newline

Alors, est-ce que ces deux modèles sont significativement différents ? 
On peut refaire un test en faisant la différence des déviances et des degrés de liberté :

\[
 \mathcal{H}_0: le ~ modele ~ propose ~ n'est ~ pas ~ significativement ~ different ~ du ~ modele ~ nul 
\qquad ; \qquad
\mathcal{H}_1:non \space \mathcal{H}_0
\]

```{r, echo=FALSE, message=FALSE, warning=FALSE}
p_value_diff_poisson <- 1-pchisq(model_freq_poisson$null.deviance-model_freq_poisson$deviance,model_freq_poisson$df.null-model_freq_poisson$df.residual)
print(c("P-value associée à l'hypothèse nulle =",p_value_diff_poisson))
```

Nous en déduisons que, pour un GLM avec une loi de poisson , le modèle nul et le modèle proposé tout de même sont satistiquement différents car la p-value est nettement inférieure à 5\%.
De plus, étant donnée que la residual deviance pour notre modèle avec les variables explicatives est inférieure à celle du modèle constitué uniquement d'une constante, nous allons préférer garder le modèle avec les variables explicatives.\newline
                                                                      
Il est également interessant de s'intéresser aux déviances résiduelles $\delta_i$ associées à chaque observations de notre training dataset. En effet, si un des $\delta_i^2$ est trop grand par rapport ?? 1, l'assuré $i$ associé contribue à l'augmentation de la déviance du modèle. Ainsi, si une valeur de $\delta_i^2$ est assez supérieure à 1 cela signifie que l'observation $i$ contribue au mauvais ajustement du modèle.

```{r, echo=FALSE, message=FALSE, warning=FALSE}
adr <- residuals(model_freq_poisson)
plot(adr^2, main = "Visualisation des déviances résiduelles pour le modèle de fréquence", xlab="Observations", ylab="Déviances résiduelles au carré")
abline(h=1,col='2', lwd=3)

length(which(adr^2>1))/length(adr)
```
 
Nous constatons qu'il y a un certains nombre de $\delta_i^2$ qui dépassent largement 1. Cela représente 8\% de notre base d'entrainement (c'est-à-dire 8\% de nos observations sur cette base d'entrainement contribuent au mauvais ajustement du modèle), ce qui reste raisonnable.

### Analyse ANOVA 

Nous réalisons un test step et l'Anova  pour chaque variable explicative.
```{r, echo=FALSE, message=FALSE, warning=FALSE,fig.height=2.5, fig.width=3.5}
library(car)
Anova(model_freq_poisson)


```
```{r, echo=FALSE, message=FALSE, warning=FALSE}

#vérification avec la fonction step
step(model_freq_poisson)

```
A partir du tableau step  nous constatons que les variables qui expliquent notre modèle sont : l'âge , le bmi, le nombre d'enfants (children) et le fait que l'assuré soit fumeur ou non (smoker).
L'Anova nous ressort le même résultat en identifiant les varibales dont la p-value sont supérieur à 5%.Nous constatons que deux varibales (steps et sex )ont une p-valeurs supérieurs à 5%. Par le Step on constate que ces deux variables(sex et steps )ont une significativité donc elles ne peuvent etre retirées de notre modèle.

Notre modèle définitif est donc le modèle constitué des varibales citées plus haut car elles ont une p-value inférieur à 10%. 


### Test sur les fréquences de sinistres avec la base de test

Une manière simple de vérifier concrètement la qualité de la prédiction de notre modèle de fréquence est de représenter, pour chaque catégorie de chaque variable explicative, la moyenne des fréquences observées sur notre base de test et la moyenne des fréquences prédites avec notre GLM sur cette même base de test. Ainsi, plus ces moyennes seront proches pour un maximum de varaibles, plus grande sera la qualité de notre modèle.\newline

Nous représentons donc un graphique pour chaque variable explicative, sur lequel figure :

\begin{itemize}
\item la répartition des observations par catégorie sous forme de diagramme à barres ;
\item en rouge les moyennes des fréquences de sinistres observées sur notre base de test ;
\item en bleu les moyennes des fréquences de sinistres prédites à partir de cette même base de test.
\end{itemize}

```{r, message=FALSE, warning=FALSE, include=FALSE}
graph_pred<-function(variable, data_test)
{
  base <- data_test 
  base$predict_freq <-predict.glm(model_freq_poisson, newdata = data_test, type = "response")
  
  tabvar <- setNames(aggregate(base$predict_freq,by=list(variable),FUN=length), c("Var", "Count"))
  tabpred<-setNames(aggregate(base$predict_freq,by=list(variable),FUN=mean), c("Var", "Pred"))
  tabobs<- setNames(aggregate(base$insuranceclaim,by=list(variable),FUN=mean), c("Var", "Obs"))
  
  table <- merge(tabvar,tabpred, by.polices="Var",all=TRUE)
  table <- merge(table, tabobs, by.polices="Var",all=TRUE)
  
  scal= max(table$Count)/max(table$Pred)
  
  q <- ggplot(table, aes(Var,group=1)) +  geom_bar(aes(y = Count), stat = "identity") +
    geom_line(aes(y = Pred*scal, colour = "Prédit")) + 
    geom_line(aes(y = Obs*scal, colour = "Observé")) + scale_y_continuous(sec.axis = sec_axis(trans = ~ . / scal)) + theme(axis.text.x = element_text(size=5,angle=45))
  
  q
}
```
```{r, echo=FALSE, message=FALSE, warning=FALSE,fig.height=2.5, fig.width=3.5}
test <- na.omit(test)
par(mfrow = c(2,2))
graph_pred(test$age,test)
graph_pred(test$newbmi,test)
graph_pred(test$children,test)
graph_pred(test$smoker,test)
```

Nous allons   choisir un seuil à partir duquel on décide de prédire Y=1 ou Y=0.

En pratique, on choisit un seuil de 0.5 :

```{r, echo=FALSE, message=FALSE, warning=FALSE,fig.height=5, fig.width=5}
confusionMatrix(factor(ifelse(test$pred2> 0.5 ,TRUE,FALSE)), factor(as.logical(test$insuranceclaim)))
levels(factor(ifelse(test$pred2> 0.5 ,TRUE,FALSE)))
levels(factor(as.logical(test$insuranceclaim)))
P <- factor(ifelse(test$pred2> 0.5 ,TRUE,FALSE))
Y <- factor(as.logical(test$insuranceclaim))



```



Afin de trouver le seuil optimal, on trace la courbe ROC qui indique le taux de vrais positifs en fonction du taux de faux positifs, pour différent seuil.\newline

```{r, echo=FALSE, message=FALSE, warning=FALSE,fig.height=5, fig.width=5}

library(ROCR)
library(PRROC)
ROCRpred <-  prediction(test$pred2, test$insuranceclaim)
ROCRperf <- performance(ROCRpred, 'tpr','fpr')
plot(ROCRperf, colorize = TRUE)

auc_ROCR <- performance(ROCRpred, measure = "auc")
auc_ROCR <- auc_ROCR@y.values[[1]]
auc_ROCR



```

Comme l'aire sous la courbe est proche de 1 alors notre modèle est adéquat.
par ailleurs nous choisissons un seuil qui maximise les vrais positifs et qui minimise les faux positifs. Choisissons un seuil de 0.2, i.e. si la prédiction est supérieur à 0.2 on prédit la survenance du sinistre.\newline 
```{r, echo=FALSE, message=FALSE, warning=FALSE,fig.height=5, fig.width=5}

result <- factor(ifelse(test$pred2 > 0.2 ,TRUE,FALSE))
confusionMatrix(result,factor( as.logical(test$insuranceclaim)))



```



## Partie II : modélisation du coût des sinistres

Le coût des sinistres est une variable continue positive, donc on va utiliser un GLM avec une loi Gamma.
De plus, chaque observation correspond à un sinistre, donc pas besoin de diviser les coûts par le nombre de sinistre dans notre cas.\newline


```{r, message=FALSE, warning=FALSE, include=FALSE}
# On retire toutes les observations où il n'y a pas de sinistre

train_amount <- train[-which(train$insuranceclaim==0),]
train_amount <- subset(train, select=-c(insuranceclaim))
                                                                
# On retire les coûts inférieurs ou égaux à zéros (cas où la responsabilité de l'assuré n'est pas engagé) :
                                                                                                                              train_amount <- na.omit(train_amount)                           
```

### Modélisation

Soit $Y_i$ le coût d'un sinistre associé à  l'assuré $i$, $X_{i}$ le vecteur colonne contenant les caractéristiques des variables explicatives de notre modèle associé à l'assuré $i$, et $\beta$ le vecteur colonne correspondant aux $p$ paramètres du modèle.\newline

Nous souhaitons déterminer, pour un assuré $i$ donné dont on connait les caractéristiques, l'espérance du coût d'un sinistre (s'il survient) qui le concernerait l'année à venir : $\mathbb{E}[Y_i| X_i]$. dans ce cas, nous utilisons une loi Gamma :
\[Y|X \sim \Gamma(k,\theta)\]
avec $k > 0$ et $\theta >0$. \newline

La fonction de lien utilisée dans notre cas est la fonction log (cela permet de s'assurer notamment que les prédictions de coûts seront bien positives): 
        \[g(\mathbb{E}[Y_i| X_i])=\log(\mathbb{E}[Y_i| X_i])=X\beta\]

Intéressons nous aux résultats de la modélisation de la sévérité, c'est-à-dire du coût d'un sinistre.
```{r, message=FALSE, warning=FALSE, include=FALSE}
model_amount <- glm(formula = charges ~ . , data = train_amount, family = Gamma(link=log))
```
### Analyse du summary

```{r, message=FALSE, warning=FALSE, include=FALSE}
summary(model_amount)
```
On constate que la "Null deviance" = 796,66 .\newline
La "Residual deviance" est égale à 263,22. Quant à l'AIC, il est égal à 19842, mais sans d'autres modèles pour comparer les performances, il n'est pas possible de conclure directement si notre modéle est relativement bon.
Selon le score de Fisher, qui est égal à 7, le modèle converge moins rapidement que le modèle de fréquence (le score de Fisher etait égal à 1).\newline

Concernant l'analyse des coefficients, on constate par exemple que, par rapport à la catégorie de référence, le fait que l'assuré soit fumeur ou non (smoker) contribue à l'augmentation importante du coût d'un sinistre : son coefficient est égal à 1.59131, donc toutes choses égales par ailleurs et sachant que la fonction de lien est le logarithme, si un assuré dépend de cette catégorie, son espérance de coût est multipliée par $\exp(1.59131) = 4.910177049$, ce qui parait logique puisque cette zone de risque concerne justement les départements où les sinistres coûtent le plus cher.\newline

Réalisons un test pour vérifier que le modèle que nous proposons peut expliquer la sévérité des sinistres :
\[
 \mathcal{H}_0: le ~ montant ~ de ~ sinistres ~ peut ~ etre ~ expliqué ~ par ~ le ~ modèle ~ proposé
\qquad ; \qquad
\mathcal{H}_1:non \space \mathcal{H}_0
\]

```{r, echo=FALSE, message=FALSE, warning=FALSE}
p_value_prop <- 1-pchisq(model_amount$deviance,model_amount$df.residual)
print(c("P-value associée à l'hypothèse nulle =",p_value_prop))
```

Visiblement, le modèle que nous proposons expliquerait assez-bien les montants.\newline

Qu'en est-il du modèle constitué d'une seule constante ?

\[
 \mathcal{H}_0:\left\lbrace
       \begin{array}{l}
          \beta_1 = 0\\
          \beta_2 = 0\\
          ...\\
          \beta_{p} = 0\\
       \end{array}
 \right.
\qquad ; \qquad
\mathcal{H}_1:not \space \mathcal{H}_0
\]

```{r, echo=FALSE, message=FALSE, warning=FALSE}
p_value_null<- 1-pchisq(model_amount$null.deviance,model_amount$df.null)
print(c("P-value associée à l'hypothèse nulle =",p_value_null))
```

La sévérité des sinistres peut être bien décrit par le modèle à une seule constante.\newline

Intéressons-nous dès à présent aux déviances résiduelles $\delta_i$ associées à chaque observations de notre training
dataset :

```{r, echo=FALSE, message=FALSE, warning=FALSE}
adr <- residuals(model_amount)
plot(adr^2, main = "Visualisation des déviances résiduelles pour le modèle de coût", xlab="Observations", ylab="Déviances résiduelles au carré")
abline(h=1,col='2', lwd=3)

length(which(adr^2>1))/length(adr)
```

Il est clair que certaines observations (une dizaine) sont beaucoup plus grandes que 1. Mais nous calculons que 5\% des observations de notre training dataset contribuent au mauvais ajustement du modèle. C'est une meilleur   performance comparé au modèle de fréquence.

### Analyse ANOVA 

Nous réalisons un test de step et l'Anova pour chaque variable explicative.
```{r, echo=FALSE, message=FALSE, warning=FALSE,fig.height=2.5, fig.width=3.5}
library(car)
Anova(model_amount)


```
```{r, echo=FALSE, message=FALSE, warning=FALSE,fig.height=2.5, fig.width=3.5}
#vérification avec la fonction step
step(model_amount)

```                 
On remarque avec l'analyse de l'anova que toutes les variables  ont une significativité dans l'explication du coût du sinistre , on décide donc de toutes les garder.

### Test sur les coûts de sinistres avec la base de test

De la même manière que pour le modèle de fréquence, nous choisissons de représenter, pour chaque catégorie de chaque variable explicative, la moyenne des coûts par sinistre observÃ©e sur notre base de test et la moyenne des coûts prédites avec notre GLM sur cette même base de test.

Nous représentons donc un graphique pour chaque variable explicative, sur lequel figure :
\begin{itemize}
\item la répartition des observations par catégorie sous forme de diagramme à barres ;
\item en rouge les moyennes des coûts de par sinistre observées sur notre base de test ;
\item en bleu les moyennes des coûts de par sinistre prédites à partir de cette même base de test.

\end{itemize}

```{r, echo=FALSE, message=FALSE, warning=FALSE,fig.height=2.5, fig.width=3.5}
graph_pred2<-function(variable, data_test)
{
  base <- data_test 
  base$predict_amount <-predict.glm(model_amount, newdata = data_test, type = "response")
  
  tabvar <- setNames(aggregate(base$predict_amount,by=list(variable),FUN=length), c("Var", "Count"))
  tabpred<-setNames(aggregate(base$predict_amount,by=list(variable),FUN=mean), c("Var", "Pred"))
  tabobs<- setNames(aggregate(base$charges,by=list(variable),FUN=mean), c("Var", "Obs"))
  
  table <- merge(tabvar,tabpred, by.polices="Var",all=TRUE)
  table <- merge(table, tabobs, by.polices="Var",all=TRUE)
  
  scal= max(table$Count)/max(table$Pred)
  
  q <- ggplot(table, aes(Var,group=1)) +  geom_bar(aes(y = Count), stat = "identity") +
    geom_line(aes(y = Pred*scal, colour = "Prédit")) + 
    geom_line(aes(y = Obs*scal, colour = "Observé")) + scale_y_continuous(sec.axis = sec_axis(trans = ~ . / scal)) + theme(axis.text.x = element_text(size=5,angle=45))
  
 q 
}
test2 <- na.omit(test)
par(mfrow = c(3,2))
graph_pred2(test2$age,test2)
graph_pred2(test2$newsteps,test2)
graph_pred2(test2$newbmi,test2)
graph_pred2(test2$newsex,test2)
graph_pred2(test2$children,test2)
graph_pred2(test2$smoker,test2)


```

## Partie III : Calcul de la prime pure

A partir du modèle de coût et du modèle de fréquence nous pouvons ainsi faire la meilleure estimation de la prime pure : en supposant le coût de sinistres indépendant de la fréquence de sinistre, l'espérance de ce que va coûter un assuré à l'assureur pour l'année à venir sera le produit de l'espérance de son coût par sinistre avec l'espérance de sa fréquence de sinistres.

```{r, echo=FALSE, message=FALSE, warning=FALSE,fig.height=2.5, fig.width=3.5}
test <- na.omit(test)
test$newfreq <- predict.glm(model_freq_poisson, newdata = test, type = "response")
test$newamount <- predict.glm(model_amount,newdata = test, type = "response")

prime_pure <- function(data_table)
{
  prime_pure <- predict.glm(model_freq_bn, newdata = data_table, type = "response")*predict.glm(model_amount,newdata = data_table, type = "response")
  return(prime_pure )
}

```

## Test de la formule de prime pure sur les données test

On peut donc estimer la prime pure sur notre base de données de test, dont voici les premières lignes :

```{r, echo=FALSE, message=FALSE, warning=FALSE}
test3 <- na.omit(test)
test3$prime_pure <- prime_pure(test3)
```


\begin{center}
```{r, echo=FALSE, message=FALSE, warning=FALSE}
kable(head(test3[,1:6]), format = "latex",booktabs = TRUE,caption = "Estimation de la Prime Pure") %>%
  kable_styling(position = "center",font_size = 6,latex_options = c("striped","HOLD_position"))
kable(head(test3[,10:16]), format = "latex",booktabs = TRUE,caption = "Estimation de la Prime Pure (suite)") %>%
  kable_styling(position = "center",font_size = 6,latex_options = c("striped","HOLD_position"))

```
\end{center}

# Conclusion


Dans le cadre de cette étude, nous avons pu étudier l'aspect tarifaire du métier d'actuaire. Cela nous permettra d'aborder du mieux possible les sujets de tarification en entreprise. Diverses méthodes existent pour déterminer la meilleure prime pure afin d'établir un tarif : classification, régression linéaire, GLM... Notre étude se focalise sur la technique du GLM qui démontre de bonnes performances de prédictions de coût et de fréquence malgrè un nombre restreint de variables explicatives et choisies arbitrairement.\newline

Nous ne prétendons pas que les modéles réalisés sont de bons modèles. Pour approfondir cette étude, il serait nécessaire de sélectionner de manière plus efficace et rationnelle les variables explicatives (soit selon des méthodes comme le Backward, ou soit en réalisant un GLM pénalisé, de type Elastic-Net). Il serait également judicieux de tester d'autres lois pour les modèles GLM afin de pouvoir comparer leur capacité de prédiction et leur vitesse de convergence.\newline

Nous considérons que ce projet nous a beaucoup apporté, que ce soit dans la conception de modèle tarifaire ou dans la façon d'aborder le traitement d'une basse de données. Cela nous a permis d'avoir un exemple concret de ce qui  peut se faire en entreprise.