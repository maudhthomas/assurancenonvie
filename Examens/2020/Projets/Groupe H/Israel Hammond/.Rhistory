setwd("~/Documents/Enseignements/AssuranceNonVie/Examens/2020/Projets/Groupe H/Israel Hammond")
knitr::opts_chunk$set(echo = TRUE)
library("quantmod")
library(knitr)
library(kableExtra)
library(fBasics)
library(pander)
library(GGally)
library(ggplot2)
library(gridExtra)
library(gdata)
library(tseries)
library(fGarch)
library(magrittr) #pour utiliser l'op??rateur pipe %>%
library(data.table)
library(MASS)
library(rgdal)
library(maptools)
library(colorspace)
library(readr)
library(magrittr)
library(mfx)
f=file.choose()
data=read.csv(f, header = TRUE)
kable(head(data),format = "latex",booktabs = TRUE,caption = "Aperçu de la base de données") %>%
kable_styling(position = "center",font_size = 4,latex_options = c("striped","HOLD_position"))
#categorisation des variables
data$age <- cut(data$age,
breaks=c(-Inf, 27, 37,46,55, Inf),
labels=c("18-27", "28-37","37-46","47-55","55+"))
data$bmi=cut(data$bmi,
breaks=c(-Inf,23.4,30,37,Inf),
labels=c("15.9-23.4","23.5-30","30.1-37","37.1+"))
data$steps=cut(data$steps,
breaks=c(-Inf,3500,5340,Inf),
labels=c("2990-3500","3501-5340","5341+"))
data$newsteps <- factor(as.character(data$steps), levels = c("5341+", "2990-3500", "3501-5340"))
data$newbmi <- factor(as.character(data$bmi), levels = c("30.1-37", "15.9-23.4", "23.5-30","37.1+"))
data$newsex <- factor(as.character(data$sex), levels = c("1", "0"))
#base de donnees finale
BDD <- subset(data,select=c(age,newsteps,newbmi,newsex,children,smoker,region,charges,insuranceclaim))
kable(summary(BDD), format = "latex",booktabs = TRUE,caption = "Statistiques descriptives des variables") %>%
kable_styling(position = "center",font_size = 4,latex_options = c("striped","HOLD_position"))
library(caret)
set.seed(123)
trainIndex <- createDataPartition(BDD$insuranceclaim,p=0.75,list=F)
# Base de training :
train <- BDD[trainIndex,]
print(c("dimension de la base de training : ", dim(train)))
# Base de test :
test <- BDD[-trainIndex,]
print(c("dimension de la base de test : ",dim(test)))
print(c("dimension de la base de BDD : ",dim(BDD)))
print(prop.table(table(train$insuranceclaim)))
train_freq <- subset(train, select=-c(charges))
# Quasi Poisson :
model_freq <- glm(formula = insuranceclaim ~ . , data = train_freq, family = quasipoisson)
# Poisson :
model_freq_poisson <- glm(formula = insuranceclaim ~ . , data = train_freq, family = poisson())
#Binomiale négative :
library(MASS)
model_freq_bn <- glm.nb(data = train_freq ,formula = insuranceclaim ~.)
# Tableau récapitulatif :
recap_perf <- data.frame(array(NA,dim=c(3,3)))
colnames(recap_perf)<-c("AIC","Déviance résiduelle","Score de Fischer")
row.names(recap_perf)<-c("Quasi Poisson","Poisson","Binomiale négative")
recap_perf$AIC <- c(model_freq$aic,model_freq_poisson$aic,model_freq_bn$aic)
recap_perf$`Déviance résiduelle` <- c(model_freq$deviance,model_freq_poisson$deviance,model_freq_bn$deviance)
recap_perf$`Score de Fischer` <- c(model_freq$iter,model_freq_poisson$iter,model_freq_bn$iter)
kable(recap_perf, format = "latex",booktabs = TRUE,caption = "Perfomance des modèles de fréquence GLM suivant différentes lois") %>%
kable_styling(position = "center",font_size = 7,latex_options = c("striped","HOLD_position"))
pred1<-predict.glm(model_freq_bn,test,type='response')
test$pred1<-predict.glm(model_freq_bn,test,type = 'response')
test$residu1<-test$pred1-test$insuranceclaim
res1=abs(mean(test$residu1))
# moyenne des résidus de la loi binomiale négative
res1
pred2<-predict.glm(model_freq_poisson ,test,type='response')
test$pred2<-predict.glm(model_freq_poisson,test,type =  'response')
test$residu2<-test$insuranceclaim-test$pred2
# moyenne des résidus de la loi de poisson
res2=abs(mean(test$residu2))
res2
#Différence entre les résidus de la loi binomiale et les résidus de la loi de poisson
res=res1-res2
res
summary(model_freq_poisson)
p_value_null_poisson <- 1-pchisq(model_freq_poisson$null.deviance,model_freq_bn$df.null)
print(c("P-value associée à l'hypothèse nulle =",p_value_null_poisson))
p_value_prop_poisson <- 1-pchisq(model_freq_poisson$deviance,model_freq_bn$df.residual)
print(c("P-value associée à l'hypothèse nulle =",p_value_prop_poisson))
p_value_diff_poisson <- 1-pchisq(model_freq_poisson$null.deviance-model_freq_poisson$deviance,model_freq_poisson$df.null-model_freq_poisson$df.residual)
print(c("P-value associée à l'hypothèse nulle =",p_value_diff_poisson))
adr <- residuals(model_freq_poisson)
plot(adr^2, main = "Visualisation des déviances résiduelles pour le modèle de fréquence", xlab="Observations", ylab="Déviances résiduelles au carré")
abline(h=1,col='2', lwd=3)
length(which(adr^2>1))/length(adr)
library(car)
Anova(model_freq_poisson)
#vérification avec la fonction step
step(model_freq_poisson)
graph_pred<-function(variable, data_test)
{
base <- data_test
base$predict_freq <-predict.glm(model_freq_poisson, newdata = data_test, type = "response")
tabvar <- setNames(aggregate(base$predict_freq,by=list(variable),FUN=length), c("Var", "Count"))
tabpred<-setNames(aggregate(base$predict_freq,by=list(variable),FUN=mean), c("Var", "Pred"))
tabobs<- setNames(aggregate(base$insuranceclaim,by=list(variable),FUN=mean), c("Var", "Obs"))
table <- merge(tabvar,tabpred, by.polices="Var",all=TRUE)
table <- merge(table, tabobs, by.polices="Var",all=TRUE)
scal= max(table$Count)/max(table$Pred)
q <- ggplot(table, aes(Var,group=1)) +  geom_bar(aes(y = Count), stat = "identity") +
geom_line(aes(y = Pred*scal, colour = "Prédit")) +
geom_line(aes(y = Obs*scal, colour = "Observé")) + scale_y_continuous(sec.axis = sec_axis(trans = ~ . / scal)) + theme(axis.text.x = element_text(size=5,angle=45))
q
}
test <- na.omit(test)
par(mfrow = c(2,2))
graph_pred(test$age,test)
graph_pred(test$newbmi,test)
graph_pred(test$children,test)
graph_pred(test$smoker,test)
confusionMatrix(factor(ifelse(test$pred2> 0.5 ,TRUE,FALSE)), factor(as.logical(test$insuranceclaim)))
levels(factor(ifelse(test$pred2> 0.5 ,TRUE,FALSE)))
levels(factor(as.logical(test$insuranceclaim)))
P <- factor(ifelse(test$pred2> 0.5 ,TRUE,FALSE))
Y <- factor(as.logical(test$insuranceclaim))
library(ROCR)
library(PRROC)
ROCRpred <-  prediction(test$pred2, test$insuranceclaim)
ROCRperf <- performance(ROCRpred, 'tpr','fpr')
plot(ROCRperf, colorize = TRUE)
auc_ROCR <- performance(ROCRpred, measure = "auc")
auc_ROCR <- auc_ROCR@y.values[[1]]
auc_ROCR
result <- factor(ifelse(test$pred2 > 0.2 ,TRUE,FALSE))
confusionMatrix(result,factor( as.logical(test$insuranceclaim)))
# On retire toutes les observations où il n'y a pas de sinistre
train_amount <- train[-which(train$insuranceclaim==0),]
train_amount <- subset(train, select=-c(insuranceclaim))
# On retire les coûts inférieurs ou égaux à zéros (cas où la responsabilité de l'assuré n'est pas engagé) :
train_amount <- na.omit(train_amount)
model_amount <- glm(formula = charges ~ . , data = train_amount, family = Gamma(link=log))
summary(model_amount)
p_value_prop <- 1-pchisq(model_amount$deviance,model_amount$df.residual)
print(c("P-value associée à l'hypothèse nulle =",p_value_prop))
p_value_null<- 1-pchisq(model_amount$null.deviance,model_amount$df.null)
print(c("P-value associée à l'hypothèse nulle =",p_value_null))
adr <- residuals(model_amount)
plot(adr^2, main = "Visualisation des déviances résiduelles pour le modèle de coût", xlab="Observations", ylab="Déviances résiduelles au carré")
abline(h=1,col='2', lwd=3)
length(which(adr^2>1))/length(adr)
library(car)
Anova(model_amount)
#vérification avec la fonction step
step(model_amount)
graph_pred2<-function(variable, data_test)
{
base <- data_test
base$predict_amount <-predict.glm(model_amount, newdata = data_test, type = "response")
tabvar <- setNames(aggregate(base$predict_amount,by=list(variable),FUN=length), c("Var", "Count"))
tabpred<-setNames(aggregate(base$predict_amount,by=list(variable),FUN=mean), c("Var", "Pred"))
tabobs<- setNames(aggregate(base$charges,by=list(variable),FUN=mean), c("Var", "Obs"))
table <- merge(tabvar,tabpred, by.polices="Var",all=TRUE)
table <- merge(table, tabobs, by.polices="Var",all=TRUE)
scal= max(table$Count)/max(table$Pred)
q <- ggplot(table, aes(Var,group=1)) +  geom_bar(aes(y = Count), stat = "identity") +
geom_line(aes(y = Pred*scal, colour = "Prédit")) +
geom_line(aes(y = Obs*scal, colour = "Observé")) + scale_y_continuous(sec.axis = sec_axis(trans = ~ . / scal)) + theme(axis.text.x = element_text(size=5,angle=45))
q
}
test2 <- na.omit(test)
par(mfrow = c(3,2))
graph_pred2(test2$age,test2)
graph_pred2(test2$newsteps,test2)
graph_pred2(test2$newbmi,test2)
graph_pred2(test2$newsex,test2)
graph_pred2(test2$children,test2)
graph_pred2(test2$smoker,test2)
test <- na.omit(test)
test$newfreq <- predict.glm(model_freq_poisson, newdata = test, type = "response")
test$newamount <- predict.glm(model_amount,newdata = test, type = "response")
prime_pure <- function(data_table)
{
prime_pure <- predict.glm(model_freq_bn, newdata = data_table, type = "response")*predict.glm(model_amount,newdata = data_table, type = "response")
return(prime_pure )
}
test3 <- na.omit(test)
test3$prime_pure <- prime_pure(test3)
kable(head(test3[,1:6]), format = "latex",booktabs = TRUE,caption = "Estimation de la Prime Pure") %>%
kable_styling(position = "center",font_size = 6,latex_options = c("striped","HOLD_position"))
kable(head(test3[,10:16]), format = "latex",booktabs = TRUE,caption = "Estimation de la Prime Pure (suite)") %>%
kable_styling(position = "center",font_size = 6,latex_options = c("striped","HOLD_position"))
