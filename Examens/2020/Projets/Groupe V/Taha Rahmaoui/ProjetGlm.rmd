---
title: "Projet : Survenance d'accident sur des polices d'assurance de voitures de camping"
author: "Jihane Serrar / Asmae Aqil / Taha Rahmaoui"
date: "Avril 2019"
output:
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```
## <span style="color:black">Introduction:</span>  
Nous souhaitons expliquer une variable $Y$, à partir de plusieurs variables explicatives $X=(X_1, X_2, . . . , X_p)$ lorsque $Y$ est de type binaire 0 ou 1. <br/>
Nous cherchons &agrave; mod&egrave;liser $\mathbb{P}(Y = 1|X_1 = x_1, X_2 = x_2, . . . , X_p = x_p)$. Nous abordons ici diff&eacute;rents mod&egrave;les (selon le niveau d\'interaction consid&eacute;r&eacute; entre les variables explicatives quantitatives ou qualitatives) qui appartiennent &agrave; la famille des mod&egrave;les lin&eacute;aires g&eacute;n&eacute;ralis&eacute;s. <br/>
**Notre jeu de donn&eacute;es est autour de la survenance d\'accident sur des polices d\'assurance de voiture de camping**
(Une police d\'assurance est un document contractuel qui fixe les conditions d\'engagements de l\'assureur &agrave; l\'&eacute;gard de l\'assur&eacute; ou d\'un groupe d\'assur&eacute;s. Elle est la preuve mat&eacute;rielle de l\'accord entre l\'assureur et l\'assur&eacute;). <br/>

## <span style="color:black">Exploration des donn&eacute;es:</span> 
Notre base de donn&eacute;es contient des informations sur les assur&eacute;s et leurs vehicules, ainsi que la survenance d'un sinistre:
```{r include=TRUE}
library(readxl)
Data=read_excel("DataSet.xlsx") #lecture du dataset
```
<!--**Visualisation d\'un &eacute;chantillon du jeu de donn&eacute;es**-->
```{r}

Data$situationfamiliale = as.factor(Data$situationfamiliale)
Data$sexeconducteur = as.factor(Data$sexeconducteur)
Data$habitation = as.factor(Data$habitation)
Data$zone = as.factor(Data$zone)
Data$marque = as.factor(Data$marque)
Data$poids = as.factor(Data$poids)
Data$usage = as.factor(Data$usage)
Data$proprietaire = as.factor(Data$proprietaire)
Data$payment = as.factor(Data$payment)
Data$voiture = as.factor(Data$voiture)
summary(Data)
#head(Data)
str(Data)
```
Ici, la variable $Y$ modelise la survenance ou non du sinistre.
```{r}
y=Data$nombre
table(y)
```
i.e. 5.1467% des polices sont touch&eacute;es par un sinistre. <br/>
Les variables cat&eacute;gorielles ou explicatives $X_i$ dont on dispose sont: la situation familiale, le sexe, la marque du v&eacute;hicule, le type de paiement pour la prime, etc.
```{r}
names(Data)
```
## <span style="color:black">Cr&eacute;ation des ensembles de train et de test:</span> 
Pour verifier l'ad&eacute;quation des mod&egrave;les qu'on va utiliser, il est primordial de separer les donn&eacute;es en ensemble d'entrainement et ensemble de validation pour v&eacute;rifier si le mod&egrave;le fonctionne aussi bien sur les nouvelles donn&eacute;es. Il faut veiller à ce qu'on ait des distributions semblables entre les donn&eacute;es du train et les donn&eacute du test
```{r}
library(caTools)
library(ggplot2)
train.size<- 0.8
sample = sample.split(Data$nombre, SplitRatio = train.size)
train = subset(Data, sample == TRUE)
test  = subset(Data, sample == FALSE)
#pour afficher le ratio de 1 et 0 dans le train et le test
#hist(train$nombre,breaks=c(0,0.5,1),freq=F)
#hist(test$nombre,breaks=c(0,0.5,1),freq=F)

```
<br/>
**Tests de corr&eacute;lation**
```{r include=TRUE}
library(GGally)
library(ggplot2)
ggcorr(Data,
     method = c("pairwise", "spearman"),
     nbreaks = 10,
     hjust = 0.8,
     label = TRUE,
     label_size = 3,
     color = "grey50")
```
**Prise en compte de l\'exposition $E$:**
Parfois, les polices d\'assurance ne sont pas observ&eacute;es pendant un an, car les assur&eacute;s peuvent les r&eacute;silier
```{r include=TRUE}
E=Data$exposition
table(E)
```
Si on suppose que la survenance d\'un &eacute;v&egrave;nement est mod&eacute;lis&eacute;e par un processus de Poisson (absence de memoire), alors: $\mathbb{P}(y=0)=\mathbb{P}(N=0)^{E}$ si
$N \sim \mathcal{B}(p):\\$
$$\mathbb{P}(Y_i = y_i) = p^{e_i y_i}[1-p^{e_i} ]^{1-y_i}$$
<br/>
-On cherche &agrave; maximiser la vraisemblance:
```{r include=T}
logL=function(p){-sum(log(dbinom(y,size=1,prob=1-(1-p)^E)))}
opt=optimize(f=logL,interval=c(1e-5,1e-1))
opt$minimum
```
<h3> Mod&egrave;le Logistique:</h3>
-Plus g&eacute;neralement on peut utiliser une regression logistique pour mod&eacute;liser la non survenance des sinistres: <br/>
On note $\pi(x)=\mathbb{P}(Y = 1|X_1 = x_1, X_2 = x_2, . . . , X_p = x_p)$ et on suppose que $\pi(x)=F(\beta_0 + \beta_1X_1 + \beta_2X_2 + . . . + \beta_pX_p)$ o&ugrave; $F$ est une fonction de r&eacute;partition et les coefficients $\beta_i$ sont inconnus.
<br/> 
**Mod&egrave;le Logit:** $F(t)=\frac{exp(t)}{1+exp(t)}$ <br/>
*Cas Simple: une seule variable explicative est prise en compte : $\pi(x)=\frac{exp(\beta_0+\beta_1x)}{1+exp(\beta_0+\beta_1x)}$ <br/>
*Cas Multiple: Dans ce cas la variable d\'int&eacute;r&ecirc;t est mod&eacute;lis&eacute;e par plusieurs variables pexplicatives $X_1,...X_p$: $\pi(x)=\frac{exp(\beta_0+\beta_1x+...+\beta_px)}{1+exp(\beta_0+\beta_1x+...+\beta_px)}$

<h1><span style="color:green">Construction du mod&egrave;le:</span>  <h1/>
On commence par un mod&egrave;le GLM qui introduit toutes les variables explicatives: (<span style="color:red">**Remarque**</span>  :le choix de la famille binomiale est d&ucirc; au type des donn&eacute;es binaire par opposition &agrave; la famille poisson qui est &agrave; utiliser si la variable $Y$ est une variable de comptage)
```{r}
reg0 = glm(nombre~., 
           data = train, family = binomial())
summary(reg0)
```
<br/>
Ce mod&egrave;le suppoe que toutes les variables explicatives sont independantes. On peut l\'am&eacute;liorer en prenant en consid&eacute;ration les dependances entre ces variables, par exemple:
<ul>
<li> Entre situationfamiliale et usage: </li>
</ul>
```{r}
reg2 = glm(nombre~.+proprietaire:usage, 
           data = train, family = binomial())
summary(reg2)
```
**S&eacute;lection de variable &agrave; prendre en compte: **
On &eacute;limine les variables qui poss&egrave;dent une p-value relativement faible.
On obtient alors le mod&egrave;le ci-dessous : 
```{r}
reg1 = glm(nombre~ageconducteur+agepermis+agevehicule
           +exposition+situationfamiliale, 
           data = train, family = binomial())
summary(reg1)
anova(reg1,test="Chisq")
```
Nous nous basons sur le crit&egrave;re de minimisation de l\'AIC pour choisir le meilleur mod&egrave;le parmi ceux &eacute;tudi&eacute;s.

<h3>Qualit&eacute; des classifieurs: Courbe ROC<h3/>

Pour le mod&egrave;le s&eacute;lectionn&eacute;, la courbe ROC est largement au-dessus de la courbe $y=x$ ce qui se traduit par une bonne performance de notre classificateur binaire. 


```{r}
library(ROCR)
M = predict(reg2,test,type="response")
pred=prediction(M,test$nombre)
plot(performance(pred,"tpr","fpr"))
```
<h3> Matrice de confusion<h3/>

- Finalement et afin de r&eacute;sumer les r&eacute;sultats de nos pr&eacute;dictions, ci-dessous la matrice de confusion r&eacute;sumant la pr&eacute;cision de notre mod&egrave;le.

```{r}
table_mat <- table(test$nombre, M > 0.5)
table_mat
```
