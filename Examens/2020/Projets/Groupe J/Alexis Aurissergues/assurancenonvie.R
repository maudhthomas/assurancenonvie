install.packages("CASdatasets", repos = "http://dutangc.free.fr/pub/RRepos/", type="source")
library(CASdatasets)
?CASdatasets

data(fremarine)
fremarine

#Variables � expliquer : 
#ClaimCharge (montant des frais de la r�clamation)
#InsuredValue (valeur assur�e)

#Variables explicatives quantitatives :
#ShipPower
#ShipBuildYear
#ShipLength
#ShipTonnage

#Variables explicatives qualitatives :
#ShipCateg
#ShipBrand 
#ShipHull

#On ne prend en compte que les variables qui nous int�ressent
tab <- fremarine[,-c(1:2,6:7,13,15:20)]
tab

#On supprime les lignes o� les donn�es ne sont pas renseign�es
datai <- na.omit(tab)

write.csv(datai,'datai.csv')

#On supprime les lignes o� InsuredValue ou ClaimCharge valent 0 car on ne peut pas effectuer de GLM Gamma dans ce cas

data = subset(datai, InsuredValue > 0 & ClaimCharge > 0)
data

write.csv(data,'data.csv')

#Cr�ation de cat�gories pour les variables explicatives quantitatives

data$ShipPower <- cut(data$ShipPower,
                  breaks=c(-Inf, 500, 1000,1500, Inf),
                  labels=c("0-500","501-1000","1001-1499","1500+"))
 
data$ShipBuildYear <- cut(data$ShipBuildYear,
                      breaks=c(-Inf, 1970, 1985,2000, Inf),
                      labels=c("1970-","1971-1985","1986-1999","2000+"))

data$ShipLength <- cut(data$ShipLength,
                   breaks=c(-Inf, 5, 10, 15, Inf),
                   labels=c("0-5","5.1-10","10-14.9","15+"))

data$ShipTonnage <- cut(data$ShipTonnage,
                    breaks=c(-Inf, 10, 50, 100, 150, Inf),
                    labels=c("0-10","10.1-50","50.1-100","100.1-149.9", "150+"))


#Mod�lisation de ClaimCharge

#V�rifions l'existence d'un lien entre la variable r�ponse et les variables explicatives.

install.packages("ggplot2")
library(ggplot2)
install.packages("gridExtra")
library("gridExtra")

g1 <- ggplot(data, aes(y=ClaimCharge, x=ShipLength, fill=ShipLength))+ 
   geom_boxplot()+
   ylim(0,30)+
   theme_classic()

g2 <- ggplot(data, aes(y=ClaimCharge, x=ShipHull, fill=ShipHull))+ 
  geom_boxplot()+
  ylim(0,30)+
  theme_classic()

grid.arrange(g1, g2, nrow=2,ncol=1)

#Variable r�ponse positive et continue : GLM Gammma

#On s�pare le jeu de donn�es pour faire des pr�dictions

#n nombre d'observations
n = nrow(data)
m = floor(0.75*n)

data_mod <- data[c(1:m),]
data_pred <- data[c(m+1:n),]

data_mod

fit1 <- glm(data = data_mod, ClaimCharge ~ ShipPower + ShipBuildYear + ShipLength
                                          + ShipTonnage
                                          + ShipCateg
                                          + ShipBrand 
                                          + ShipHull, family = Gamma())

summary(fit1)


#essai
#model1<-glm(ClaimCharge~ShipPower*ShipBuildYear,family=binomial(link="logit"))
#> model2<-glm(incidence~area+isolation,family=binomial(link="logit"))
#> anova(model2,model1,test="Chisq")
#ShipBuildYear



