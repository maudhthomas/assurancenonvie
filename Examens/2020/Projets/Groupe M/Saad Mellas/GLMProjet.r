library(tweedie)
library(statmod)
library(Metrics)
library(MASS)
library(car)


#Lecture du jeu de Donn�es

data = read.table("car.csv", header = TRUE, sep=";")
#data = read.table(file.choose(), header = TRUE, sep=";")

set.seed(101) # d�finir la graine afin que le m�me �chantillon puisse �tre reproduit � l'avenir �galement

# la selection de 75% du jeu de donn�es comme jeu d'apprentissage 

sample <- sample.int(n = nrow(data), size = floor(.75*nrow(data)), replace = F)
train <- data[sample, ]
test  <- data[-sample, ]

#Histogramme

hist(data$claimcst0)
hist(log(data$claimcst0))

#GLM Poisson
poisson1 <- glm(numclaims ~ veh_value + veh_body + veh_age + gender + area + agecat, family="poisson", data=train)
summary(poisson1)

#Selection des variables
library(MASS)
p2 <- stepAIC(poisson1,direction = "backward", k = 2)
summary(p2)

#La d�viance
lambda1 <- p2$fitted.values
print(lambda1)

Stat_D <- 2 * sum(ifelse(train$numclaims>0,train$numclaims*log(train$numclaims/lambda1),0)-(train$numclaims-lambda1))
print(Stat_D)
print(pchisq(Stat_D,p2$df.residual,lower.tail = FALSE))

#MSE
pred <- predict(poisson1, newdata = test, type="response")
mean((test$claimcst0-pred)^2)




#Tweedie GLM
#for (i in seq(1.1,1.9,by=0.1)) {
p_Values = seq(1.1,1.6,by=0.1)
dispersions = c()
loglikelihoods = c()


# p = 1.1
fit <- glm(formula = train$claimcst0 ~ veh_value + veh_body + veh_age + gender + area + agecat, data = train, family = tweedie(link.power=0, var.power=1.1))
summary(fit)
value  <- summary(fit)$dispersion
LogLhd <- logLiktweedie( fit)
dispersions = c(dispersions,value)
loglikelihoods = c(loglikelihoods,LogLhd)


# p = 1.2
fit <- glm(formula = train$claimcst0 ~ veh_value + veh_body + veh_age + gender + area + agecat, data = train, family = tweedie(link.power=0, var.power=1.2))
summary(fit)
value  <- summary(fit)$dispersion
LogLhd <- logLiktweedie( fit)
dispersions = c(dispersions,value)
loglikelihoods = c(loglikelihoods,LogLhd)


# p = 1.3
fit <- glm(formula = train$claimcst0 ~ veh_value + veh_body + veh_age + gender + area + agecat, data = train, family = tweedie(link.power=0, var.power=1.3))
summary(fit)
value  <- summary(fit)$dispersion
LogLhd <- logLiktweedie( fit)
dispersions = c(dispersions,value)
loglikelihoods = c(loglikelihoods,LogLhd)


# p = 1.4
fit <- glm(formula = train$claimcst0 ~ veh_value + veh_body + veh_age + gender + area + agecat, data = train, family = tweedie(link.power=0, var.power=1.4))
summary(fit)
value  <- summary(fit)$dispersion
LogLhd <- logLiktweedie( fit)
dispersions = c(dispersions,value)
loglikelihoods = c(loglikelihoods,LogLhd)


# p = 1.5
fit <- glm(formula = train$claimcst0 ~ veh_value + veh_body + veh_age + gender + area + agecat, data = train, family = tweedie(link.power=0, var.power=1.5))
summary(fit)
value  <- summary(fit)$dispersion
LogLhd <- logLiktweedie( fit)
dispersions = c(dispersions,value)
loglikelihoods = c(loglikelihoods,LogLhd)


# p = 1.6
fit <- glm(formula = train$claimcst0 ~ veh_value + veh_body + veh_age + gender + area + agecat, data = train, family = tweedie(link.power=0, var.power=1.6))
summary(fit)
value  <- summary(fit)$dispersion
LogLhd <- logLiktweedie( fit)
dispersions = c(dispersions,value)
loglikelihoods = c(loglikelihoods,LogLhd)

plot(seq(1.1,1.6,by=0.1),loglikelihoods, type="o")

#We take p = 1.3
fit <- glm(formula = train$claimcst0 ~ veh_value + veh_body + veh_age + gender + area + agecat, data = train, family = tweedie(link.power=0, var.power=1.3))
summary(fit)
value  <- summary(fit)$dispersion
pred <- predict(fit, newdata = test, type="response")
LogLhd <- logLiktweedie( fit)
AIC <- AICtweedie(fit, dispersion=value, k = 2, verbose=TRUE)  
#mse(y_pred = pred$fitted.values, y_true = test$claimcst0)
mse(test$claimcst0,pred)

resultats�<-�data.frame(loglikelihoods=loglikelihoods,�dispersions=dispersions,�row.parameter_values=p_Values)
resultats

#Selection de variables
Anova(fit, test.statistic = "LR", type = 3)