#Projet Assurance non Vie 
#Groupe C : Aslihan Cetin - Guillaume Stasinski - Hakim Tebib

#Voici la liste des packages nécessaires à la bonne execution du code
require(MASS)
require(ggplot2)
require(foreach)

setwd(dirname(rstudioapi::getActiveDocumentContext()$path)) #Cette commande devrait éviter toutes interactions de l'utilisateur. 
#Il peut être nécessaire de la supprimer si cela ne fonctionne pas.

Database <- read.table(file="Assurance-non-vieBaseDeDonnees.csv", sep=";")
Database<-Database[-c(1),] #On supprime la ligne et la colonne supplémentaire

names(Database) <- c("zon", "bussald", "kundnr", "antavt", "dur", "antskad", "skadkost")

summary(Database) #Inclure ce résumé dans le rapport 

#On remarque que la variable skadkost présente des valeurs comme suit : ''. En le comparant avec la variable 
#antskad, on remarque qu'il s'agit d'observation nulle et donc aucune réclamation pécunière n'a été réclamée à 
#l'assurance
#On corrige donc ces observations en remplaçant le '' par '0'

Database$Reclamation <- Database$skadkost 
Database$Reclamation[Database$Reclamation==""] <- 0

#On finalise ensuite le nettoyage de la base en convertissant les factor en as.numeric.

Database$Reclamation <- as.character(Database$Reclamation) 
Database$Reclamation <- as.numeric(Database$Reclamation)

Database$dur <- as.character(Database$dur) 
Database$dur <- as.numeric(Database$dur)

Database$kundnr <- as.character(Database$kundnr) 
Database$kundnr <- as.numeric(Database$kundnr)

Database$antavt <- as.character(Database$antavt) 
Database$antavt <- as.numeric(Database$antavt)

Database$antskad <- as.character(Database$antskad) 
Database$antskad <- as.numeric(Database$antskad)

Database <- within(Database, {
  zon <- factor(zon)
  bussald <- factor(bussald)
})

#On vérifie la qualité du nettoyage : 
summary(Database)

#Histogrammes conditionnels : 

DataTemporaire <- subset(Database, antskad <300)
#Ancien : 
ggplot(Database, aes(x=antskad, fill=zon))+ geom_histogram(binwidth = 40) +facet_grid(~ zon) + theme(legend.text=element_text(size=20)) 
#Nouveau :
ggplot(DataTemporaire, aes(x=zon, y=antskad))+ geom_boxplot() + xlab("Zone") + ylab("Réclamation par société")  #Réclamation par zone



#Ancien :
ggplot(Database, aes(x=antskad, fill=bussald))+ geom_histogram(binwidth = 40) +facet_grid(~ bussald) + theme(legend.text=element_text(size=20))
#Nouveau : 
ggplot(DataTemporaire, aes(x=bussald, y=antskad))+ geom_boxplot() + xlab("Âge du bus") + ylab("Réclamation par société")  #Réclamation par âge de bus

ggplot(Database, aes(x=antskad)) + geom_histogram() #Réclamation totale sans condition

#Pour modéliser le nombre de demandes divisé par la durée, c'est-à-dire le nombre moyen de demandes par an,
#il est raisonnable de penser à une régression de Poisson ou à une régression binomiale négative avec un terme de 
#compensation. - offset term - qui est ici la variable dur pour duration
   
summary(model.frequency_p <-glm(antskad~zon+bussald+offset(log(dur)), data=Database, family=poisson))
   
#Tester la significativité de bussald 
model.frequency_p1 <-update(model.frequency_p,.~.-bussald) 
anova(model.frequency_p1,model.frequency_p,test="Chisq")

#Résidu pour étudier si le modèle représente correctement les données 
with(model.frequency_p, 
     cbind(res.deviance = deviance, df = df.residual, 
           p = pchisq(deviance, df.residual, lower.tail = FALSE)))
#Au vu de la p-value, la réponse est non ( p-value = 3.344299e-248)


#On va laisser "tomber" le GLM POISSON pour le moment et nous allons nous occuper du GLM binomial negatif
#Nous repartons des hypothèses similaires à celles faites dans le cadre du GLM POISSON
summary(model.frequency_nb <-glm.nb(antskad~zon+bussald+ offset(log(dur)), data=Database))

#Tester la significativité de bussald 
model.frequency_nb1 <-update(model.frequency_nb,.~.-bussald) 
anova(model.frequency_nb1,model.frequency_nb,test="Chisq")
#Bussald semble être significatif

#Résidu pour étudier si le modèle représente correctement les données 
with(model.frequency_nb, 
     cbind(res.deviance = deviance, df = df.residual, 
           p = pchisq(deviance, df.residual, lower.tail = FALSE)))
#On ne peut pas rejetter l'hypothèse nulle. Le modèle Binomiale Negative semble être plus approprié.

#Comparaison entre la binomiale negative et la Poisson. 
pchisq(2*(logLik(model.frequency_nb)-logLik(model.frequency_p)), df=1, lower.tail= FALSE)
#La P-value (=0) suggeste que la binomiale negative est plus pertinente


#On choisit donc le modèle binomiale négatif avec fonction de lien logarithmique comme modèle de 
#calcul des réclamations


#Sevérité de la réclamation

ggplot(Database, aes(x=Reclamation))+ geom_histogram() +facet_grid(vars(Database$zon),vars(Database$bussald)) #Réclamation par âge de bus et selon zone


#Fit une gamma sur notre modèle :
summary(model.severity_g <- glm(Reclamation ~ zon + bussald, data =Database[Database$Reclamation > 0, ], 
                                family = Gamma("log"), weights = antskad))


#Testons l'importance de la zone d'un côté puis l'importance de l'âge du bus de l'autre

model.severity_g1 <- update(model.severity_g, . ~ . - zon)

anova(model.severity_g1, model.severity_g, test = "Chisq") 
model.severity_g2 <- update(model.severity_g, . ~ . - bussald)
anova(model.severity_g2, model.severity_g, test = "Chisq")

#Au vu des p-values, nous rejettons les deux hypothèses. Nous conservons donc le modèle complet

#Graphique en forme de violon : 

#Database temporaire pour retirer les 0 
DataTemporaire <- subset(Database, Reclamation >0)
ggplot(DataTemporaire, aes(x=bussald, y=Reclamation)) + geom_violin(scale = "count",trim = FALSE,adjust = .5) + geom_dotplot(binaxis='y', stackdir='center', dotsize=0.05, stackratio = .7) 

ggplot(DataTemporaire, aes(x=zon, y=Reclamation)) + geom_violin(scale = "count",trim = FALSE,adjust = .5) + geom_dotplot(binaxis='y', stackdir='center', dotsize=0.05, stackratio = .7) 


glm(formula, family=familytype(link=linkfunction), data=)

glm(Reclamation ~ zon + bussald, data =Database[Database$Reclamation > 0, ], 
    family = Gamma("inverse"), weights = antskad)
modelechoisi <- glm(Reclamation ~ zon + bussald, data =Database[Database$Reclamation > 0, ], 
    family = Gamma("log"), weights = antskad)
modelechoisi
glm(Reclamation ~ zon + bussald, data =Database[Database$Reclamation > 0, ], 
    family = Gamma("identity"), weights = antskad)

glm(Reclamation ~ zon + bussald, data =Database[Database$Reclamation > 0, ], 
    family = gaussian("inverse"), weights = antskad)
glm(Reclamation ~ zon + bussald, data =Database[Database$Reclamation > 0, ], 
    family = gaussian("log"), weights = antskad)
glm(Reclamation ~ zon + bussald, data =Database[Database$Reclamation > 0, ], 
    family = gaussian("identity"), weights = antskad)


with(modelechoisi, 
     cbind(res.deviance = deviance, df = df.residual, 
           p = pchisq(deviance, df.residual, lower.tail = FALSE)))
