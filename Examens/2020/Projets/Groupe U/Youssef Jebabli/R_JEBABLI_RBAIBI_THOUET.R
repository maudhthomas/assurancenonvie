library("quantmod")
library(knitr)
library(kableExtra)
library(fBasics)
library(pander)
library(GGally)
library(ggplot2)
library(gridExtra)
library(gdata)
library(tseries)
library(fGarch)
library(ccgarch)
library(magrittr)
library(data.table)
library(MASS)
library(mapview)
library(maptools)
library(colorspace)
library(readr)
library(magrittr)
library(mfx)
library(data.table)


################## FICHIERS CSV


polices <- fread("PG_2017_YEAR0.csv", sep="auto",header="auto")
claims <- fread("PG_2017_CLAIMS_YEAR0.csv", sep="auto",header="auto")


###################################################### Preparation donnée
################## Fusion et selection

claims$id_policy <- paste(claims$id_client,claims$id_vehicle, sep="-")
data <- merge(polices,claims, by.polices="id_policy",all=TRUE)
data<-subset(data,select=c(pol_bonus,pol_coverage,pol_payd,pol_usage,pol_insee_code,drv_drv2,drv_age_lic1,drv_age_lic2,vh_age,vh_din,vh_value,claim_nb,claim_amount))

##################  Nettoyage

# age permis > 111 ans
data<-data[-which(data$drv_age_lic1==111),] 
data<-data[-which(data$drv_age_lic2==111),]

# variables cibles NA
data$claim_amount<-replace(data$claim_amount,is.na(data$claim_amount),0)
data$claim_nb<-replace(data$claim_nb,is.na(data$claim_nb),0)

# Zonier
data$dept<-substr(data$pol_insee_code,1,2)
tab<-aggregate(data$claim_amount,by=list(dept=data$dept),FUN=sum)
tab$dept<-replace(tab$dept,which(tab$dept==c("2A")),c(201))   #201 c'est ajjacio et 202 c'est BASTIA 
tab$dept<-replace(tab$dept,which(tab$dept==c("2B")),c(202))
data$dept<-replace(data$dept,which(data$dept==c("2A")),c(201))  
data$dept<-replace(data$dept,which(data$dept==c("2B")),c(202))
tab$dept<-as.numeric(tab$dept)
data$dept<-as.numeric(data$dept)
data<-merge(data,tab, by.dept ="dept",all=TRUE)
colnames(data)[colnames(data)=="x"] <- "prest_zone"


################## Categoriser les variables 


data$dal1 <- cut(data$drv_age_lic1 , c(0  , 3   ,15  ,35 ,max(data$drv_age_lic1)) , labels = c( "Jeunes conducteurs" , "Moyennement experimentes" , "Experimentes" , "Vieux conducteurs"))


data$dal2 <-cut(data$drv_age_lic2 , c(0 , 3   ,15  ,35 , max(data$drv_age_lic2)) , labels = c("Jeunes seconds conducteurs" , "Seconds conducteurs Moyennement experimentes" , "Second conducteur Experimentes" , "Vieux seconds conducteurs"))
data$dal2<-paste(data$dal2,data$drv_drv2,sep="-")

data$Vage<-cut(data$vh_age,c(0, 7, 13,30, 55, max(data$vh_age)),labels=c("nouveau vehicule","vehicule assez nouveaux","ancien vehicule","tres ancien vehicule"))

data$vh_din<-cut(data$vh_din,c(12,68,89,109,555),c("petit moteur","moyen petit moteur","moyen gros moteur","gros moteur"))

data$valeur<-cut(data$vh_value,c(0,6000,13000,20000,45000,max(data$vh_value)),c("vehicule pas cher","vehicule moyennement peu cher","vehicule moyennement cher","vehicule cher","vehicule tres cher"))

data$pol_bonus <- cut(data$pol_bonus,breaks = c(0, 0.5, 0.67, 0.9, 2.16),c("bonus faible","bonus moyen","bonus eleve","bonus tres eleve"))

data$zone<-cut(data$prest_zone,breaks =c(1132,82479.98,189043.26,279107.62,496402.15),na.rm=TRUE,c("Z1","Z2","Z3","Z4")) #bug sur la fonction quantile construction a la main 

data$dal1 <- factor(as.character(data$dal1), levels = c("Experimentes", "Vieux conducteurs", "Moyennement experimentes","Jeunes conducteurs"))

data$dal2 <- factor(as.character(data$dal2), levels = c("NA-No", "Second conducteur Experimentes-Yes", "Vieux seconds conducteurs-Yes","Seconds conducteurs Moyennement experimentes-Yes","Jeunes seconds conducteurs-Yes"))

data$pol_coverage <- as.factor(data$pol_coverage)

data$pol_payd <- as.factor(data$pol_payd)

data$pol_usage <- factor(as.character(data$pol_usage), levels = c("WorkPrivate", "Retired", "Professional","AllTrips"))

data$Vage <- factor(as.character(data$Vage), levels = c("ancien vehicule", "vehicule assez nouveau", "tres ancien vehicule","nouveau vehicule"))


data$claim_nb <- as.integer(data$claim_nb)

################### BDD Finale
BDD <- subset(data,select=c(pol_bonus,pol_coverage,pol_payd,pol_usage,dal1,dal2,Vage,vh_din,valeur,zone,claim_nb,claim_amount)) 

summary(BDD)
str(BDD)



###################################################### MODELISATION DE LA FREQUENCE



library(caret)
set.seed(123)

#############TRAIN TEST

trainIndex <- createDataPartition(BDD$claim_nb,p=0.75,list=F)

# Base de training :
train <- BDD[trainIndex,]
print(c("dimension de la base de training : ", dim(train)))

# Base de test :
test <- BDD[-trainIndex,]
print(c("dimension de la base de test : ",dim(test)))


############# LOI

# Quasi Poisson : 
train_freq <- subset(train, select=-c(claim_amount))
model_freq <- glm(formula = claim_nb ~ . , data = train_freq, family = quasipoisson)

# Poisson :
model_freq_poisson <- glm(formula = claim_nb ~ . , data = train_freq, family = poisson())

# Binomiale négative :
library(MASS)
model_freq_bn <- glm.nb(formula = claim_nb ~ . , data = train_freq)

# Tableau récapitulatif Loi :
recap_perf <- data.frame(array(NA,dim=c(3,3)))
colnames(recap_perf)<-c("AIC","Déviance résiduelle","Score de Fischer")
row.names(recap_perf)<-c("Quasi Poisson","Poisson","Binomiale négative")


recap_perf$AIC <- c(model_freq$aic,model_freq_poisson$aic,model_freq_bn$aic)
recap_perf$`Déviance résiduelle` <- c(model_freq$deviance,model_freq_poisson$deviance,model_freq_bn$deviance)
recap_perf$`Score de Fischer` <- c(model_freq$iter,model_freq_poisson$iter,model_freq_bn$iter)

recap_perf



#############GLM

train_freq <- subset(train, select=-c(claim_amount))
model_freq_bn <- glm.nb(formula = claim_nb ~ . , data = train_freq)
summary(model_freq_bn)


adr <- residuals(model_freq_bn)
length(which(adr^2>1))/length(adr)


################## ANOVA
anova(model_freq_bn, test = "Chisq")


################## Prédiction
graph_pred<-function(variable, data_test)
{
  base <- data_test 
  base$predict_freq <-predict.glm(model_freq_bn, newdata = data_test, type = "response")
  
  tabvar <- setNames(aggregate(base$predict_freq,by=list(variable),FUN=length), c("Var", "Count"))
  tabpred<-setNames(aggregate(base$predict_freq,by=list(variable),FUN=mean), c("Var", "Pred"))
  tabobs<- setNames(aggregate(base$claim_nb,by=list(variable),FUN=mean), c("Var", "Obs"))
  
  table <- merge(tabvar,tabpred, by.polices="Var",all=TRUE)
  table <- merge(table, tabobs, by.polices="Var",all=TRUE)
  
  scal= max(table$Count)/max(table$Pred)
  
  q <- ggplot(table, aes(Var,group=1)) +  geom_bar(aes(y = Count), stat = "identity") +
    geom_line(aes(y = Pred*scal, colour = "Prédit")) + 
    geom_line(aes(y = Obs*scal, colour = "Observé")) + scale_y_continuous(sec.axis = sec_axis(trans = ~ . / scal)) + theme(axis.text.x = element_text(size=5,angle=45))
  
  q
}

test <- na.omit(test)
par(mfrow = c(2,2))
graph_pred(test$zone,test)
graph_pred(test$pol_bonus,test)
graph_pred(test$pol_payd,test)
graph_pred(test$pol_usage,test)
test <- na.omit(test)
par(mfrow = c(3,2))
graph_pred(test$dal1,test)
graph_pred(test$dal2,test)
graph_pred(test$Vage,test)
graph_pred(test$vh_din,test)
graph_pred(test$valeur,test)




###################################################### MODELISATION DE LA SINISTRALITE

# On retire toutes les observations oû il n'y a pas de sinistre !
train_amount <- train[-which(train$claim_nb==0),]
train_amount <- subset(train, select=-c(claim_nb))
# On retire les coûts inférieurs ou égaux à zéros (cas oû la responsabilité de l'assuré n'est pas engagée) :
train_amount <- train_amount[-which(train$claim_amount<=0),]
train_amount <- na.omit(train_amount)
model_amount <- glm(formula = claim_amount ~ . , data = train_amount, family = Gamma(link=log))
summary(model_amount)

adr <- residuals(model_amount)
length(which(adr^2>1))/length(adr)



################## Anova

anova(model_amount, test = "Chisq")





################## Prédiction

graph_pred2<-function(variable, data_test)
{
  base <- data_test 
  base$predict_amount <-predict.glm(model_amount, newdata = data_test, type = "response")
  
  tabvar <- setNames(aggregate(base$predict_amount,by=list(variable),FUN=length), c("Var", "Count"))
  tabpred<-setNames(aggregate(base$predict_amount,by=list(variable),FUN=mean), c("Var", "Pred"))
  tabobs<- setNames(aggregate(base$claim_amount,by=list(variable),FUN=mean), c("Var", "Obs"))
  
  table <- merge(tabvar,tabpred, by.polices="Var",all=TRUE)
  table <- merge(table, tabobs, by.polices="Var",all=TRUE)
  
  scal= max(table$Count)/max(table$Pred)
  
  q <- ggplot(table, aes(Var,group=1)) +  geom_bar(aes(y = Count), stat = "identity") +
    geom_line(aes(y = Pred*scal, colour = "Prédit")) + 
    geom_line(aes(y = Obs*scal, colour = "Observé")) + scale_y_continuous(sec.axis = sec_axis(trans = ~ . / scal)) + theme(axis.text.x = element_text(size=5,angle=45))
  
  q
}

test2 <- na.omit(test)
test2 <- test2[-which(test2$claim_amount<=0),]
par(mfrow = c(2,2))
graph_pred2(test2$zone,test2)
graph_pred2(test2$pol_bonus,test2)
graph_pred2(test2$pol_payd,test2)
graph_pred2(test2$pol_usage,test2)
par(mfrow = c(3,2))
graph_pred2(test2$dal1,test2)
graph_pred2(test2$dal2,test2)
graph_pred2(test2$Vage,test2)
graph_pred2(test2$vh_din,test2)
graph_pred2(test2$valeur,test2)
