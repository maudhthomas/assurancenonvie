---
lang: fr-FR
title: <center> <h1>Projet GLM</h1> </center>
subtitle: <center> <h1>Analyse et modélisation des pertes des ménages causées par les sinistres</h1> </center>
author: 
- Yousra Elkhassouani
- Ihsane El Gharef
- Yassine Belgaid
date: "`r format(Sys.time(), '%d %B %Y')`"
output:   
  bookdown::html_document2:
    fig_caption: yes
    number_sections: yes
    toc: yes
    theme: united
linkcolor: red
header-includes:
- \usepackage[francais]{babel}
- \usepackage{float}
- \usepackage{booktabs}
- \usepackage{ggplot2}
---

```{r global_options, include=FALSE}
knitr::opts_chunk$set(fig.align = 'center',fig.pos = 'H', fig.path='Figs/',echo=FALSE, warning=FALSE, message=FALSE)
#position par défaut des figures dans le pdf : centrées et à l'endroit où on les construit
library(magrittr) #pour utiliser l'opérateur pipe %>%
library(kableExtra) #pour améliorer les tableaux
options(knitr.table.format = "pandoc") 
```

# Introduction {#intro}

Le présent travail rend compte du projet GLM réalisé dans le cadre du cours « **Assurance non-vie** ».

Notre étude consiste principalement à répondre aux questions suivantes : Quel est le nombre des sinistres survenus par foyer ? Combien de dégâts ont-ils causés ?
La réponse à ces questions aidera donc l’assureur à déterminer, avec précision, la prime des assurances par foyer, ainsi qu’une estimation des prévisions nécessaires pour le remboursement dans le cas d’un sinistre.

Notre jeu de données est issu du cours « Assurance non-vie » de l’université de Paris-Dauphine, il présente le nombre de sinistres Habitation et le coût moyen des sinistres en neuf régions de France sur une période donnée.

Dans ce qui suit, nous présentons brièvement le traitement opéré au niveau des bases de sonnées. Ensuite, nous partageons les différentes approches adoptées pour modéliser notre variable. Finalement, nous exposons et analysons les résultats obtenus.

## Analyse des données {#analdonnee}

### A propos des données

La base de données utilisée contient 5352 observations, autrement dit, notre étude va porter sur 5352 différents foyers. Pour chaque foyer, on retrouves les informations suivantes :
```{r variables}
data = read.csv("dataframe.csv", header = TRUE)
qualitative = c("categories socio-professionnelles","categories sociales du revenu","catégories/quantiles des revnus par personne","code région","habitat","statut d'ocupation","catégorie d'âge du chef du ménage","composition du ménage","présence d'enfants","Nationalité","Possession d'un véhicule automobile")
quantitative = c("Revenu du foyer","Revenu par unité","nombre de personnes par ménage","nombre d'adultes par ménages","dégats des sinistres type 1","dégats des sinistres type 2","dégats des sinistres type 3","cotisation police 1","cotisation police 2","cotisation police 3","")
variables = data.frame(qualitative, quantitative)
knitr::kable(variables,align='c',caption="Table des variables utilisées dans l'études",booktabs=T)
```

### Les variables qualitatives

Commençons par étudier la répartition de l’échantillon sur chaque classe de ces variables qualitatives.
<br/><br/>
$a)$ La première variable considérée est **la catégorie socio-professionnelle** du chef de chaque ménage, présentée par le graph ci-dessous :

```{r pcschart, fig.cap="Nombre de foyers par catégorie socio-professionnelle"}
library(ggplot2)
temp = as.data.frame(table(data$pcs))
ggplot(temp, aes(x=Var1, y=Freq)) + 
  geom_bar(stat = "identity") + 
  geom_text(aes(label=Freq), vjust=0, size=3) + 
  coord_flip() +
  labs(y= "Nombre de foyers", x = "Catégorie socio-professionnelle")
```

On constate que la catégorie socio-professionnelle dominante est : retraitants, ouvriers et employés ou personnes à profession intermédiaire.D’autre part, les Artisans agriculteurs exploitants ou personnes sans activité professionnelle représente une minorité dans l’échantillon étudié.
Le nombre minimal dans une classe socio-professionnelle est 116 ; Ceci donne suffisamment d’observations pour inclure cette variable dans notre étude.

```{r sinistreparpcs, fig.cap="Nombre et dégâts des sinistres par catégorie socio-professionnelle"}
library(scales)

data$sisistretotal=apply(cbind(data$Sinistre1,data$Sinistre2,data$Sinistre3),1,sum,na.rm = TRUE)
temp1 = aggregate(data$NSin, by=list(Category=data$pcs), FUN=sum)
temp1$x2 = aggregate(data$sisistretotal, by=list(Category=data$pcs), FUN=sum)$x

scaleFactor <- max(temp1$x) / max(temp1$x2)

ggplot(temp1, aes(x=Category,  width=.4)) +
  geom_col(aes(y=x), fill="#999999", position = position_nudge(x = -.4)) +
  geom_col(aes(y=x2 * scaleFactor), fill="#333333") +
  scale_y_continuous(name="Nbr sinistres", sec.axis=sec_axis(~./scaleFactor, name="Dégâts des sinistres", labels = label_dollar(prefix = "", suffix = "€")))  +
  theme(
    axis.title.x.bottom=element_text(color="#999999"),
    axis.text.x.bottom=element_text(color="#999999"),
    axis.title.x.top=element_text(color="#333333"),
    axis.text.x.top=element_text(color="#333333"),
    axis.title.y.left=element_text(color="black"),
    axis.text.y.left=element_text(color="black"),
  ) +
  coord_flip() +
  labs(x = "Catégorie socio-professionnelle")
```

On remarque une corrélation naturelle entre le nombre des sinistres et leurs dégats, sauf pour les retraités et les personnes à profession intermédiaire, où les dégâts sont plus élevés par sinistre.

```{r moyensinparcat}

moyennesparcat = temp
moyennesparcat$nbsin = temp1$x
moyennesparcat$degats = temp1$x2
moyennesparcat$meansin = moyennesparcat$nbsin/moyennesparcat$Freq
moyennesparcat$mandegats = moyennesparcat$degats/moyennesparcat$Freq

knitr::kable(moyennesparcat,align='c',caption="Moyennes du nombre et des dégats des sinistre par foyer par catégorie socio-professionnelle", col.names = c("Catégorie socio-professionnelle", "Nombre de foyers", "Nombre de sinistres", "Dégats", "Moyenne sinistres par foyer", "Moyenne dégats par foyer"),booktabs=T)
```


$b)$ Maintenant, on considère la répartition des foyers par zones géographiques. L’intégration de cette variable donne une idée sur les zones considérées comme étant plus ou moins dangereuses, chose qui est considérée comme étant très importantes pour l’assureur.

```{r regionchart, fig.cap="Nombre de foyers par région"}

temp3 = as.data.frame(table(data$region))
ggplot(temp3, aes(x=Var1, y=Freq)) + 
  geom_bar(stat = "identity") + 
  geom_text(aes(label=Freq), vjust=0, size=3) + 
  labs(y= "Nombre de foyers", x = "Région")
```


```{r sinistreparcoderegion, fig.cap="Nombre et dégâts des sinistres par région"}
temp2 = aggregate(data$NSin, by=list(Category=data$region), FUN=sum)
temp2$x2 = aggregate(data$sisistretotal, by=list(Category=data$region), FUN=sum)$x

temp2$Category = as.factor(temp2$Category)

scaleFactor <- max(temp2$x) / max(temp2$x2)

ggplot(temp2, aes(x=Category,  width=.4)) +
  geom_col(aes(y=x), fill="#999999", position = position_nudge(x = -.4)) +
  geom_col(aes(y=x2 * scaleFactor), fill="#333333") +
  scale_y_continuous(name="Nbr sinistres", sec.axis=sec_axis(~./scaleFactor, name="Dégâts des sinistres", labels = label_dollar(prefix = "", suffix = "€")))  +
  theme(
    axis.title.x.bottom=element_text(color="#999999"),
    axis.text.x.bottom=element_text(color="#999999"),
    axis.title.x.top=element_text(color="#333333"),
    axis.text.x.top=element_text(color="#333333"),
    axis.title.y.left=element_text(color="black"),
    axis.text.y.left=element_text(color="black"),
  ) +
  coord_flip() +
  labs(x = "Code région")
```

Comme attendu, la région 2 qui contient le plus grand nombre de foyer contient aussi le plus grands nombres de sinistres. Contrairement à la région 3 où le nombre de foyer ainsi que le nombres de sinistres sont respectivement petits.

```{r moyensinparregion}

moyennesparreg = temp3
moyennesparreg$nbsin = temp2$x
moyennesparreg$degats = temp2$x2
moyennesparreg$meansin = moyennesparreg$nbsin/moyennesparreg$Freq
moyennesparreg$mandegats = moyennesparreg$degats/moyennesparreg$Freq

knitr::kable(moyennesparreg,align='c',caption="Moyennes du nombre et des dégats des sinistre par foyer par région", col.names = c("Code région", "Nombre de foyers", "Nombre de sinistres", "Dégats", "Moyenne sinistres par foyer", "Moyenne dégats par foyer"),booktabs=T)

```

$c)$ Le graphique suivant présente la répartition des foyers par catégorie d'âge du chef du foyer :

```{r agechart, fig.cap="Nombre de foyers par catégorie d'âge"}

temp4 = as.data.frame(table(data$agecat))
ggplot(temp4, aes(x=Var1, y=Freq)) + 
  geom_bar(stat = "identity") + 
  geom_text(aes(label=Freq), vjust=0, size=3) + 
  labs(y= "Nombre de foyers", x = "Catégorie d'âge")

```

### Les variables quantitatives

$a)$ Parmis les variables importantes dans cette base de données est le revenu des foyers, l'histogramme suivant donne une répartition des foyers par revenus :

```{r histrevenu, fig.cap= "Histogramme de répartition des revenus des foyers", results = 'hide'}
ggplot(data, aes(x = reves)) +
        geom_histogram()+  
        scale_x_log10(labels = label_dollar(prefix = "", suffix = "€"), limits= c(1000,100000))+ 
  labs(y= "Nombre de foyers", x = "Revenus du foyer")
mean(data$reves)
sd(data$reves)
```

La répartition des revunus par foyer ressemble à une loi normale de moyenne 14 880€ et d'un grand écart type de 74 609 € ce qui montre une grande différence entre les classes sociales. On peut observer aussi des valeurs extrêmes :<br/>
- min = 1000 €<br/>
- max = 3 416 250 €

Les revenus d'un foyer sont un indicateur très important pour l'assureur. D'une part, il permet d'estimer le type ou la valeur du produit d'assurance que peut acheter le foyer, et d'autre part, il donne une estimation des dégâts possibles dans le cas d'un sinistre.

$b)$ Répartition des revenus par unité de consommation par foyer

Une autre façon de voir l'indicateur des revenus est le revenu par personne dans le foyer. le diagramme suivant donne la répartition des foyers par cet indicateur :

```{r histrevenuparunite, fig.cap= "Histogramme de répartition des revenus des foyers par unité de consommation", results = 'hide'}
ggplot(data, aes(x = RUC)) +
        geom_histogram()+  
        scale_x_log10(labels = label_dollar(prefix = "", suffix = "€"))+ 
  labs(y= "Nombre de foyers", x = "Revenus du foyer par personne")
mean(data$RUC)
sd(data$RUC)
```

Comme les revenus par foyer, les revenus par personne par foyer suivent une distribution normale de moyenne 6 277 € et d'écart type 3 709 €.

$c)$ Nombre de personnes par ménage

Il est aussi intéressant de voir l'impact du nombre de personnes par foyer sur le nombre et le dégâts des sinistres, ce qui nous mène à intégrer cette variable dans notre modèle. Ci dessous la répartition des foyers par nombre de personnes dans le foyer

```{r histperson, fig.cap= "Histogramme du nombre de personnes par foyer", results = 'hide'}
ggplot(data, aes(x = nbpers)) +
        geom_histogram(binwidth = 1) + 
  labs(y= "Nombre de foyers", x = "Nombre des membres du foyer")+
    scale_x_continuous(breaks=c(2,4,6,8,10))
length(data$nbpers[data$nbpers<2.5])/length(data$nbpers)
length(data$nbpers[2.5<data$nbpers & data$nbpers<4.5])/length(data$nbpers)
length(data$nbpers[4.5<data$nbpers & data$nbpers<6.5])/length(data$nbpers)
length(data$nbpers[6.5<data$nbpers])/length(data$nbpers)
```

La majorité (+80%)des foyers ont un effectif inférieur ou égale à 4 :
- 42% sont seuls ou vivent à 2
- 41% des foyers entre 3 et 4 personnes
- 15% entre 5 et 6, et le rest pour des foyers avec 7 personnes ou plus 

Cette visualisation des variables quantitatives nous permet de comprendre leurs distributions, afin de choisir la meilleure classification qui leur rend qualitative.

# Problématique

L’objectif du présent travail est de proposer une démarche et une modélisation pour déterminer le montant des fonds propres que doit impérativement détenir l’assureur pour garantir ses engagements vis-à-vis des assurés et pour leur permettre de faire face aux aléas inhérents à son activité. Cette problématique se traduit par la nécessité de prévoir le nombre et les dégâts des sinistres dans le futur. Cela permettra à l'assureur aussi de calculer la prime de risque.

Ainsi, on introduit un modèle linéaire généralisé afin d’estimer le nombre des sinistres, leurs montants, en fonction des caractéristiques de chaque foyer.

Comment peut-on donc choisir ce modèle ? Quelle famille exponentielle sera adéquate à ce modèle ? Et quelle sera la fonction de liaison ? 


# Modélisation GLM {#glm}

Dans cette partie nous on va proposer deux modélisations différentes. Une pour le nombre des sinistres, et une autre pour les dégâts de ces sinistres. De cette façon, nous aurons la possibilité d’utiliser plusieurs lois appartenant à des familles exponentielles.

## Nombre de sinistre comme variable de réponse

Commençons d’abord par une modélisation du nombre de sinistres par foyer. Cette variable étant discrète, devra être modélisée par une loi de Poisson ou une loi binomiale négative, le choix final de la loi dépendera des résultats obtenus.

### Loi de poisson 

On va adopter une démarche progressive où l’on commence par une variable explicative et ajouter à chaque fois d’autres variables.

#### Premier modèle

Pour un premier modèle, nous allons expliquer le nombre des sinistres par foyer en fonction des revenus du foyer par personne : 

```{r Poisson1}
glm_Poisson_1 = glm(formula = NSin ~ crevpp, family = poisson(link = "log"), data = data)
summary(glm_Poisson_1)

```

La p-value de la variable crevpp (quantiles des revenus par personne) est proche de 0, c'est bien donc une variable explicative, or on remarque que la residual deviance est très grande par rapport au degré de liberté, ce qui se traduit par un mauvais modèle. le crevpp seul ne peut pas donc expliquer le nombre de sinistres par foyer.

#### Modèle à quatre variables

L'étude préliminaire des données, a montré certaines corrélations entre le nombre de sinistres et : la région et la catégorie socio-professionnelle (pcs). En plus de ces paramètres, nous allons ajouter au modèle la catégorie d'âge du chef du ménage.

```{r Poisson2}
glm_Poisson_2 = glm(formula = NSin ~ crevpp + region + pcs + agecat, family = poisson(link = "log"), data = data)
summary(glm_Poisson_2)

```

En ajoutant ces variable explicatives, et comme prévu, le modèle s'est amélioré, Puisque le critère d'information d'Akaike AIC a passé de 33219 à 31249 et le paramètre $RD/DL$ de 3,51 à 3,26. 

__Remarques :__ 
1- RD représente la déviance résiduelle et DL le degré de liberté du modèle. A savoir que le modèle est plus pertinent d'autant que le paramètre $RD/DL$ est proche de 1. 
2- Dans ce modèle, nous avons remplacé RUC par crevpp qui représente les quartiles des revenus par persone par foyer, afin d'améliorer la performance du modèle.

#### Modèle complet {#FinalPoisson}

Nous allons cette fois utiliser toutes les variables, sauf celles redondantes (revenus du foyer,, revenus par personne par foyer ...) afin d'évaluer la pertinance de notre base de données dans la modélisation du nombre de sinistres par foyer.

```{r Poisson3}
glm_Poisson_2 = glm(formula = NSin ~ crevpp + region + pcs + agecat + Ahabi + Atyph + Acompm + nbpers, family = poisson(link = "log"), data = data)
summary(glm_Poisson_2)

```

Ce résultat n'inclu pas les variables suivantes : <br/>
- Présence des enfants dans le foyer<br/>
- Catégorie sociale des revenus<br/>
- Nationalité du foyer<br/>
- Présence d'un véhicule

En effet, après un premier essai avec toutes les variables, le test de Student a révélé la non pertinence de ces variables, d'où leur exclusion du modèle. D'autre part, le quotient $RD/DL$ est réduit à 2,82, ce qui rend le modèle plus robuste. Or, ce résultat est toujours loin de 1, ce qui nous mène à se poser la question : est ce que notre modèle est mal choisi (il faut changer de famille de lois) ? ou bien il nous faut rechercher de nouvelles variables explicatives ? la partie suivante nous aidera à trouver la réponse en utilisant un modèle avec une loi binomiale négative.

### Loi binomiale négative

Dans cette partie, nous allons conserver les variables utilisées en \@ref(FinalPoisson), or, au lieu d'utiliser la famille des lois de poisson, nous allons utiliser les lois binomiales négatives.

```{r negativeBinomial}
library(MASS)
glm_NB = glm.nb(formula = NSin ~ crevpp + region + pcs + agecat + Ahabi + Atyph + Acompm + nbpers, link = log, data = data)
summary(glm_NB)

```

On remarque que l'utilisation de la loi binomiale négative a réduit considérablement la déviance résiduelle, ainsi que le critère AIC. En effet on a $RD/DL_{Binomnégative} = 1.29 \approx 1$ et $28904= AIC_{Poisson} > AIC_{Binomnégative} = 25883$
<br/>Ce résultat nous permet de conclure que ces variables expliquent bien le nombre de sinistres par foyer.<br/>

## Dégâts du sinistre comme variable de réponse
Cette fois, on va essayer d’expliquer les dégâts des sinistres en fonction des variables dont on dispose. Contrairement à la partie précédente, on va procéder par un modèle global qui utilise toutes les variables. Et par suite, le raffiner en fonction des résultats du test de student, en relevant les variables non pertinentes.

La loi choisie est la loi Gamma, puisque les dégâts représentent une variable continue positive (donc impossibilité d’utiliser la loi normale).

### Modèle avec tout les paramètres

```{r Gamma}
data1 = data[data$sisistretotal > 1e-05, ]

glm_Gamma = glm(formula = sisistretotal ~ RUC + cs + reves + crevpp + region + pcs + agecat + Ahabi + Atyph + Acompm + nbpers + enfants + Anat + Bauto, family = Gamma(link = log), data = data1)
summary(glm_Gamma)
```

On remarque que plusieurs variables ne sont pas explicatives, d'après le test de student. Or $RD/DL = 1.42$ et $AIC_{Gamma} = 19247$, on peut dire que le modèle est plus au moins cohérent.

### Modèle raffiné

```{r Gamma2}
glm_Gamma2 = glm(formula = sisistretotal ~ crevpp + region + nbpers, family = Gamma(link = log), data = data1)
summary(glm_Gamma2)
```

En supprimant les variables incohérentes, on retrouve que les dégâts des sinistres dépendent de :<br/>
- Catégorie de revenu par personne<br/>
- La région<br/>
- Le nombre de personnes par foyer

Nous n'avons pas pu utiliser la fonction canonique $g(x)=\frac{1}{x}$ comme fonction de lien, car elle retourne une erreur.

__Conclusion du modèle :__
Nous n'avons pas eu un modèle qui donne $RD/DL \approx 1$, on peut conclure que les dégâts des sinistres dépendent d'autres variables non inclues dans le modèle.

