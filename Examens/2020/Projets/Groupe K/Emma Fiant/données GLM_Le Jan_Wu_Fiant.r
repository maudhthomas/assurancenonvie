#install.packages("CASdatasets", repos = "http://dutangc.free.fr/pub/RRepos/", type="source")
#install.packages("tweedie")
#install.packages("statmod")
library(CASdatasets)
library(tweedie)
library(statmod)
??CASdatasets
data("pg15training")

# Dataset assurance autombile en France en 2015
#Le dataset comprend 100021 observations faites sur un an.
#On �tudiera le montant et le nombre des prestations r�clam�es pour dommage mat�riel sur un tiers(variable Numtppd Indtppd) en 
#fonction des variables explicatives: Cat�gorie du v�hicule (Category), l'occupation du conducteur (Occupation),
#son �ge(Age), la r�gion de r�sidence du conducteur (Group2), la densit� de population de la ville dans laquelle 
#le conducteur vit (density).

