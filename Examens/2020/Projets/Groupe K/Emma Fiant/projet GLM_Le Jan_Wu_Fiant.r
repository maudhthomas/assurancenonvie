install.packages("CASdatasets", repos = "http://dutangc.free.fr/pub/RRepos/", type="source")
#install.packages("tweedie")
#install.packages("statmod")
#install.packages("car")
library(CASdatasets)
library(tweedie)
library(statmod)
library(car)
data("pg15training")

# Dataset assurance autombile en France en 2015
#Le dataset comprend 100021 observations faites sur un an.
#On �tudiera le montant et le nombre des prestations r�clam�es pour dommage mat�riel sur un tiers(variable Numtppd Indtppd) en 
#fonction des variables explicatives: Cat�gorie du v�hicule (Category), l'occupation du conducteur (Occupation),
#son �ge(Age), la r�gion de r�sidence du conducteur (Group2), la densit� de population de la ville dans laquelle 
#le conducteur vit (density).

#On garde les variables voulues et on a choisi d'�liminer les valeurs avec Numtppd>0 & Indtppd=0 
#(cela signifie que il y a eu accident mais un remboursement nul, ce qui semble peu coh�rent).
#Ayant beaucoup de donn�es,on ne prendra,pour notre 2eme GLM
#que les valeurs de montants>0 et <2000 apr�s observation de sa distribution;

df<-subset(pg15training,( Numtppd>0&Indtppd>0) | (Numtppd==0 & Indtppd==0), select = c(Category,Occupation,Age,Group2,Density,Numtppd,Indtppd))
dff<-subset(df,( 2000>Indtppd&Indtppd>0) , select = c(Category,Occupation,Age,Group2,Density,Numtppd,Indtppd))


#Age et Density sont des variables continues qu'on doit traiter.
df$Age<-cut(df$Age,breaks = 10*(0:8))
df$Density<-cut(df$Density,breaks = 30*(0:10))
dff$Age<-cut(df$Age,breaks = 10*(0:8))
dff$Density<-cut(df$Density,breaks = 30*(0:10))

"*********************************1ere GLM*********************************"

fit <- glm(formula =  Numtppd~ Category+Occupation+Group2+Age+Density, data = df, family = poisson())
summary(fit)

#SELECTION DE VARIABLES

#Dans ce cas on peut faire une autre GLM avec moins de variables, on choisit d'enlever la variable Occupation
fit2 <- glm(formula =  Numtppd~ Category+Age+Group2+Density, data = df, family = poisson())
summary(fit2)

#Crit�re AIC
c(AIC(fit),AIC(fit2))

#fit est mieux que fit2 au niveau AIC

#Comparaison des d�viances pour faire le choix
anova(fit2, fit,test = "LRT")

#On teste donc l'influence du facteur Occupation. La p-value est <0,05.
#On rejette H0 donc le facteur a une influence.

#Le mod�le fit reste le mod�le le plus pertinent d'apr�s le AIC et la d�viance.

#Plus simplement, on peut aussi utiliser la fonction Anova du package pour
#�valuer la significativit� de toutes les variables.
Anova(fit)

#ANALYSE DE RESIDUS

res<-residuals(fit,type="deviance")
plot(res^2,ylab="Residuals Deviance")
abline(h=1, col='red')
length(which(res^2>1))

#11370 observations contribuent � la mauvaise qualit� de l'ajustement.


#PREDICTIONS (pour les 100eres valeurs)
test<-subset(df[1:100,],select = c(Category,Occupation,Age,Group2,Density))
predfit<-predict.glm(fit,newdata=test,type='response')

x<-seq(1,100)
y1<-predfit
y2<-df[1:100,]$Numtppd 

plot(x, y1, type = "n", ylim =c(0,3), xlab = "Indice", ylab = "Numtppd")
lines(x, y1, col = "blue")
lines(x, y2, col = "red")
legend(1, 3, legend=c("Valeurs pr�dites", "Valeurs observ�es"),
       col=c("blue", "red"), lty=1:2, cex=0.8, text.font=4, bg='lightblue')

"*********************************2e GLM*********************************" 

require(statmod)
fitbis <- glm(formula =  Indtppd~ Category+Occupation+Age+Group2+Density, data = dff, family = tweedie())


#SELECTION DE VARIABLES

Anova(fitbis)
#D'apr�s le r�sultat, la variable Category n'est pas significative.
#Dans ce cas on peut faire une autre GLM avec moins de variables, on choisit d'enlever la variable Occupation
fitbis2 <- glm(formula =  Indtppd~ Occupation+Age+Group2+Density, data = dff, family = tweedie())
summary(fitbis2)


#ANALYSE DE RESIDUS

resbis<-residuals(fitbis2,type="deviance")
plot(resbis^2,ylab="Residuals Deviance")
abline(h=1, col='red')


#PREDICTIONS (pour les 100eres valeurs)

testbis<-subset(dff[1:100,],select = c(Occupation,Age,Group2,Density))
predfitbis<-predict.glm(fitbis2,newdata=test,type='response')
x<-seq(1,100)
z1<-predfitbis
z2<-df[1:100,]$Indtppd

plot(x, z1, type = "n", ylim =c(0,5000), xlab = "Indice", ylab = "Indtppd")
lines(x, z1, col = "blue")
lines(x, z2, col = "red")
legend(1, 5000, legend=c("Valeurs pr�dites", "Valeurs observ�es"),
       col=c("blue", "red"), lty=1:2, cex=0.8, text.font=4, bg='lightblue')

