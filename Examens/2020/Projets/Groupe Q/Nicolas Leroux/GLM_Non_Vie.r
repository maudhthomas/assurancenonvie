
library(car)

# Importation des datas
data4 <- read.csv("/Users/nicolasleroux/Documents/ISUP 2/Assurance non vie/Projet/test2.csv", sep=";")

#GLM Gamma pour estimer le cout d'un sinistre
fit4 <- glm(formula = data4$charges~data4$age+data4$sex+data4$children+data4$smoker+data4$region+data4$bmi, data = data4, family = Gamma())
summary(fit4)
anova(fit4, test="Chisq")
Anova(fit4, test.statistic = "LR", type = 3)



#Fonction lien du GLM Gamma
lien <- function(x){return(1/x)}



#Fonction permettant l'estimation en fonction des coefficients du GLM, du profil qu'on souhaite estimer et la fonction lien correspondante.
estimation <- function(coef,profil,f){
  estimation = f(sum(coef*profil))
  return(estimation)
}

profil_age <- function(age){
  if (age == "18-22") return(c(1,0,0,0,0,0,0,0))
  if (age == "23-27") return(c(1,1,0,0,0,0,0,0))
  if (age == "28-33") return(c(1,0,1,0,0,0,0,0))
  if (age == "34-39") return(c(1,0,0,1,0,0,0,0))
  if (age == "40-45") return(c(1,0,0,0,1,0,0,0))
  if (age == "46-51") return(c(1,0,0,0,0,1,0,0))
  if (age == "52-55") return(c(1,0,0,0,0,0,1,0))
  if (age == "56-64") return(c(1,0,0,0,0,0,0,1))
}

profil_sexe <- function(sexe){
  if (sexe =="female") return(c(0))
  if (sexe =="male") return(c(1))
}

profil_bmi <- function(bmi){
  if (bmi == "15.93-23") return(c(0,0,0,0,0,0,0))
  if (bmi == "23.01-26,30") return(c(1,0,0,0,0,0,0))
  if (bmi == "26.31-28.31") return(c(0,1,0,0,0,0,0))
  if (bmi == "28.32-30.40") return(c(0,0,1,0,0,0,0))
  if (bmi == "30.41-32.40") return(c(0,0,0,1,0,0,0))
  if (bmi == "32.41-34.70") return(c(0,0,0,0,1,0,0))
  if (bmi == "34.71-38.70") return(c(0,0,0,0,0,1,0))
  if (bmi == "38.71-53.13") return(c(0,0,0,0,0,0,1))
  
}

profil_children <- function(child){
  if (child == "zero") return(c(0,0,0,0,1))
  if (child == "one") return(c(0,1,0,0,0))
  if (child == "two") return(c(0,0,0,1,0))
  if (child == "three") return(c(0,0,1,0,0))
  if (child == "four") return(c(1,0,0,0,0))
  if (child == "five") return(c(0,0,0,0,0))
}

profil_smoker <- function(smoker){
  if (smoker=="yes") return(c(1))
  if (smoker=="no") return(c(0))
}

profil_region <- function(region){
  if (region == "northeast") return(c(0,0,0))
  if (region == "northwest") return(c(1,0,0))
  if (region == "southeast") return(c(0,1,0))
  if (region == "southwest") return(c(0,0,1))
}

profil <- function(age,sexe,children,smoker,region,bmi){
  profil = c(age,sexe,children,smoker,region,bmi)
  return(profil)
}


#Calcul de l'ecart absolu moyen (EAM) et la racine de l'erreur quadratique moyenne (REQM)
sigma = 0
EAM = 0
sigma2 = 0

for (i in 1:1338) {
  age = profil_age(data4$age[i])
  sexe = profil_sexe(data4$sex[i])
  bmi = profil_bmi(data4$bmi[i])
  children = profil_children(data4$children[i])
  smoker = profil_smoker(data4$smoker[i])
  region = profil_region(data4$region[i])
  profil_final = profil(age,sexe,children,smoker,region,bmi)
  estimate = estimation(fit4$coefficients, profil_final, lien)
  sigma = sigma + abs(estimate-data4$charges[i])
  sigma2 = sigma2 + (estimate-data4$charges[i])^2
        
  }


EAM = sigma/1338
REQM = sigma2/1338
sigma
EAM
(REQM)^(1/2)


#GLM Gamma pour estimer le cout d'un sinistre qu'avec les classes significatives
fit5 <- glm(formula = data4$charges~data4$age+data4$smoker+data4$bmi, data = data4, family = Gamma())
summary(fit5)
anova(fit5, test="Chisq")
Anova(fit5, test.statistic = "LR", type = 3)



#Calcul de l'ecart absolu moyen (EAM) et la racine de l'erreur quadratique moyenne (REQM)
sigma = 0
EAM = 0
sigma2 = 0

profil <- function(age,smoker,bmi){
  profil = c(age,smoker,bmi)
  return(profil)
}

for (i in 1:1338) {
  age = profil_age(data4$age[i])
  bmi = profil_bmi(data4$bmi[i])
  smoker = profil_smoker(data4$smoker[i])
  profil_final = profil(age,smoker,bmi)
  estimate = estimation(fit5$coefficients, profil_final, lien)
  sigma = sigma + abs(estimate-data4$charges[i])
  sigma2 = sigma2 + (estimate-data4$charges[i])^2

}


EAM = sigma/1338
REQM = sigma2/1338
sigma
EAM
REQM^(1/2)

#Determination des cas dont l'ecart avec la prédiction est supérieur a 10 000

L=c()
for (i in 1:1338) {
  age = profil_age(data4$age[i])
  bmi = profil_bmi(data4$bmi[i])
  smoker = profil_smoker(data4$smoker[i])
  profil_final = profil(age,smoker,bmi)
  estimate = estimation(fit5$coefficients, profil_final, lien)
  if (estimate-data4$charges>=10000){
    L=c(L,i)
  }
}
L


#Elimination de ces données de la table initiale et GLM Gamma 

data8<-data4
for (k in 0:138){
  data8<-data8[-L[139-k],]
}


fit8 <- glm(formula = data8$charges~data8$age+data8$smoker+data8$bmi, data = data8, family = Gamma())
summary(fit8)
anova(fit8, test="Chisq")
Anova(fit8, test.statistic = "LR", type = 3)


#Calcul de l'ecart absolu moyen (EAM) et la racine de l'erreur quadratique moyenne (REQM)
sigma = 0
EAM = 0
sigma2 = 0

profil <- function(age,smoker,bmi){
  profil = c(age,smoker,bmi)
  return(profil)
}

for (i in 1:1338) {
  age = profil_age(data8$age[i])
  bmi = profil_bmi(data8$bmi[i])
  smoker = profil_smoker(data8$smoker[i])
  profil_final = profil(age,smoker,bmi)
  estimate = estimation(fit5$coefficients, profil_final, lien)
  sigma = sigma + abs(estimate-data8$charges[i])
  sigma2 = sigma2 + (estimate-data8$charges[i])^2
  
}


EAM = sigma/1338
REQM = sigma2/1338
sigma
EAM
REQM^(1/2)

#Determination du nombre de cas grave parmi les 139 lignes eliminees

compteur=0

for (k in 1:139){
  if (data4$Graves[L[k]]=="1"){
    compteur=compteur+1
  }
}
compteur


#Exemple d'une estimation d'un profil
profilageX=profil_age("34-39")
profilfumX=profil_smoker("yes")
profilbmiX=profil_bmi("26.31-28.31")
profilX=profil(profilageX,profilfumX,profilbmiX)
X = estimation(fit8$coefficients,profilX,lien)
X


#GLM Binomial pour estimer la probabilite d'avoir un sinistre grave.
fit6 <- glm(formula = data4$Graves~data4$age+data4$sex+data4$children+data4$smoker+data4$region+data4$bmi, data = data4, family = binomial())
summary(fit6)
anova(fit6, test="Chisq")
Anova(fit6,test.statistic="LR",type=3)

#Fonction lien du GLM Binomial
lien2<- function(x){return(exp(x)/(1+exp(x)))}

#Calcul du nombre de cas qu'on aurait declare grave (car proba > 0.85) parmis les cas graves de notre base de donnees
l=0
m=0

profil <- function(age,sexe,children,smoker,region,bmi){
  profil = c(age,sexe,children,smoker,region,bmi)
  return(profil)
}

for (i in 1:1338) {
    if (data4$Graves[i]=="1"){
      age = profil_age(data4$age[i])
      sexe = profil_sexe(data4$sex[i])
      bmi = profil_bmi(data4$bmi[i])
      children = profil_children(data4$children[i])
      smoker = profil_smoker(data4$smoker[i])
      region = profil_region(data4$region[i])
      profil_final = profil(age,sexe,children,smoker,region,bmi)
    estimate = estimation(fit6$coefficients, profil_final, lien2)
    m=m+1
    if (estimate>=0.85){
      l=l+1
    }
    }
    
}

l
m

#Calcul du nombre de cas qu'on aurait declare non grave (car proba < 0.85) parmis les cas non graves de notre base de donnees
l=0
m=0

profil <- function(age,sexe,children,smoker,region,bmi){
  profil = c(age,sexe,children,smoker,region,bmi)
  return(profil)
}

for (i in 1:1338) {
  if (data4$Graves[i]=="0"){
    age = profil_age(data4$age[i])
    sexe = profil_sexe(data4$sex[i])
    bmi = profil_bmi(data4$bmi[i])
    children = profil_children(data4$children[i])
    smoker = profil_smoker(data4$smoker[i])
    region = profil_region(data4$region[i])
    profil_final = profil(age,sexe,children,smoker,region,bmi)
    estimate = estimation(fit6$coefficients, profil_final, lien2)
    m=m+1
    if (estimate<=0.85){
      l=l+1
    }
  }
  
}

l
m

#Calcul du nombre de cas graves ou non qu'on a bien trouve.

l=0
compteur=0

for (i in 1:1338) {
    age = profil_age(data4$age[i])
    sexe = profil_sexe(data4$sex[i])
    bmi = profil_bmi(data4$bmi[i])
    children = profil_children(data4$children[i])
    smoker = profil_smoker(data4$smoker[i])
    region = profil_region(data4$region[i])
    profil_final = profil(age,sexe,children,smoker,region,bmi)
    estimate = estimation(fit6$coefficients, profil_final, lien2)
    if (estimate>=0.85){
      l=1
    }
    else{l=0}
    if (l==data4$Graves[i]){
      compteur=compteur+1
    }
}


compteur

#GLM Binomial pour estimer la probabilite d'avoir un sinistre grave qu'avec les classes significatives
fit7 <- glm(formula = data4$Graves~data4$smoker+data4$bmi, data = data4, family = binomial())
summary(fit7)
anova(fit7, test="Chisq")
Anova(fit7,test.statistic="LR",type=3)

#Calcul du nombre de cas qu'on aurait declare grave (car proba > 0.85) parmis les cas graves de notre base de donnees
l=0
m=0

profil <- function(smoker,bmi){
  profil = c(1,smoker,bmi)
  return(profil)
}

for (i in 1:1338) {
  if (data4$Graves[i]=="1"){
    bmi = profil_bmi(data4$bmi[i])
    smoker = profil_smoker(data4$smoker[i])
    profil_final = profil(smoker,bmi)
    estimate = estimation(fit7$coefficients, profil_final, lien2)
    m=m+1
    if (estimate>=0.85){
      l=l+1
    }
  }
  
}

l
m

#Calcul du nombre de cas qu'on aurait declare non grave (car proba < 0.85) parmis les cas non graves de notre base de donnees
l=0
m=0

profil <- function(smoker,bmi){
  profil = c(1,smoker,bmi)
  return(profil)
}

for (i in 1:1338) {
  if (data4$Graves[i]=="0"){
    bmi = profil_bmi(data4$bmi[i])
    smoker = profil_smoker(data4$smoker[i])
    profil_final = profil(smoker,bmi)
    estimate = estimation(fit7$coefficients, profil_final, lien2)
    m=m+1
    if (estimate<=0.85){
      l=l+1
    }
  }
  
}

l
m

#Calcul du nombre de cas graves ou non qu'on a bien trouve.

l=0
compteur=0

for (i in 1:1338) {
  bmi = profil_bmi(data4$bmi[i])
  smoker = profil_smoker(data4$smoker[i])
  profil_final = profil(smoker,bmi)
  estimate = estimation(fit7$coefficients, profil_final, lien2)
  if (estimate>=0.85){
    l=1
  }
  else{l=0}
  if (l==data4$Graves[i]){
    compteur=compteur+1
  }
}


compteur

profil <- function(age,sexe,children,smoker,region,bmi){
  profil = c(age,sexe,children,smoker,region,bmi)
  return(profil)
}

#Exemple d'une estimation d'un profil
profilageX=profil_age("23-27")
profilsexX=profil_sexe("female")
profilfumX=profil_smoker("yes")
profilbmiX=profil_bmi("32.41-34.70")
profilchildX=profil_children("zero")
profilregX=profil_region("northwest")
profilX=profil(profilageX,profilsexX,profilchildX,profilfumX,profilregX,profilbmiX)
X = estimation(fit6$coefficients,profilX,lien2)
X

