---
output:
  pdf_document: default
  html_document: default
---
## Report by : Mehdi Jebs, Fedi Kouki, Yosri Sakly
## Problem Statement 
The data gives the details of third party motor insurance claims in Sweden for the year 1977. In Sweden, all motor insurance companies apply identical risk arguments to classify customers, and thus their portfolios and their claims statistics can be combined. The data were compiled by a Swedish Committee on the Analysis of Risk Premium in Motor Insurance. The Committee was asked to look into the problem of analysing the real influence on the claims of the risk arguments and to compare this structure with the actual tariff.

```{r include = FALSE}
knitr::opts_chunk$set(echo=FALSE)
```


```{r echo=FALSE}
library(lme4)
Swedish = read.csv("SwedishMotorInsurance.csv",header=TRUE)

dim(Swedish)   #rows and columns
#2182 x 7 is the dimension of the dataset.

str(Swedish)   #internal data structure
head(Swedish)  #print the first 5 observations
tail(Swedish)  #print the last 5 obs
```
As we could see from the dataset above, there are 7 variables :

#### Kilometre : Kilometre travelled per year 
 
 1: <1000
 
 2: 1000-15000
 
 3: 15000-20000
 
 4: 20000-25000
 
 5: >25000

#### Zone   
 1: Stockholm, Göteborg, and Malmö with surroundings
 
 2: Other large cities with surroundings
 
 3: Smaller cities with surroundings in southern Sweden
 
 4: Rural areas in southern Sweden
 
 5: Smaller cities with surroundings in northern Sweden
 
 6: Rural areas in northern Sweden 7: Gotland

#### Bonus : No claims bonus; equal to the number of years, plus one, since the last claim

#### Make : 1-8 represents eight different common car models. All other models are combined in class 9.

#### Insured : Number of insured in policy-years

#### Claims : Number of claims

#### Payment : Total value of payments in Skr (Swedish Krona)

 We are interested to know each field of the data collected through descriptive analysis to gain basic insights into the data set and to prepare for further analysis.

```{r echo=FALSE} 
summary(Swedish)
```

The result gives us the median, mean, quartile, max and min values. We can infer that insured minimum claims and payment is 0 from which we can deduce that there are cars which are insured but there has been no insurance claimed
```{r}
Swedish$Zone <- as.factor(Swedish$Zone) 
```
The total value of payment by an insurance company is an important factor to be monitored. So, the committee has decided to find whether this payment is related to number of claims and the number of insured policy years. They also want to visualize the results for better understanding.

we perform corelation to find out the relation between Claims and Payment
```{r}
b=cor(Swedish$Claims,Swedish$Payment)
#b*100 percentage of corelation between claims and payments
#Hence we can see that the corelation between claims and payment are 0.9954 ie, 99% correlated.
plot(Swedish$Claims,Swedish$Payment)
c=cor(Swedish$Claims,Swedish$Insured)
#c*100 Claims and insured have corelation of 91% .
d=cor(Swedish$Insured,Swedish$Payment)
#d*100 Payment and Insured has Corelation of 93% 
plot(Swedish$Claims,Swedish$Insured)
plot(Swedish$Payment,Swedish$Insured)
```


Hence the relation between Claims and Payments are correlated and as the value of claim increases the payment value will also increase. 


# Analysis
We want to figure out the reasons for insurance payment increase and decrease. 
```{r}
reg=lm(Swedish$Payment~Swedish$Claims+Swedish$Insured+Swedish$Make+Swedish$Bonus+Swedish$Zone+Swedish$Kilometres)
summary(reg)
plot(reg)
```


Independent variable: insured, claims, make, bonus, zone, and kilometres. Dependent variable: payment. From this we can see the regression lines (as seen on the left side) and this would help us predict the payment values. The high p value of make and bonus show they do not make much influence on the payment. 


 We want to understand what affects their claim rates so as to decide the right premiums for a certain set of situations. Hence, we need to find whether the insured amount, zone, kilometer, bonus, or make affects the claim rates and to what extent.
```{r}
regcl = lm(Swedish$Claims~Swedish$Zone+Swedish$Bonus+Swedish$Kilometres+Swedish$Make)
summary(regcl)
plot(regcl)
```


Dependent variable: claims, Independent variable: kilometres, zone, bonus, make, and insured. In the last case we see all the P values are significantly high hence we can assume that kilometer, zone , insured, bonus , make are making impacts on claims. 


# Plot Claim Frequencies

```{r}
par(mfrow=c(2, 2)) 
boxplot(Claims/Insured ~ Kilometres, data=Swedish, xlab="Distance Driven",
        ylab="Average Claim Number", ylim = c(0, 0.6))
boxplot(Claims/Insured ~ Zone, data=Swedish, xlab="Geographic Zone",
        ylab="Average Claim Number", ylim = c(0, 0.6))
boxplot(Claims/Insured ~ Bonus, data=Swedish, xlab="Accident Free Years",
        ylab="Average Claim Number", ylim = c(0, 0.6))
boxplot(Claims/Insured ~ Make, data=Swedish, xlab="Auto Make",
        ylab="Average Claim Number", ylim = c(0, 0.6))
```

# Plot Payment

```{r}
par(mfrow=c(2, 2)) 
boxplot(Payment ~ Kilometres, data=Swedish, xlab="Distance Driven",
        ylab="Payment")
boxplot(Payment ~ Zone, data=Swedish, xlab="Geographic Zone",
        ylab="Payment")
boxplot(Payment ~ Bonus, data=Swedish, xlab="Accident Free Years",
        ylab="Payment")
boxplot(Payment ~ Make, data=Swedish, xlab="Auto Make",
        ylab="Payment")
```

# GLM Poisson on claim frequency

```{r}
model.frequency =glm(Claims ~ factor(Kilometres)+Zone+
                       factor(Bonus)+factor(Make), offset=log(Insured),poisson(link=log), data=Swedish)
summary(model.frequency)
```


The information on deviance is also provided. We can use the residual deviance to perform a goodness-of-fit test for the overall model. The residual deviance is the difference between the deviance of the current model and the maximum deviance of the ideal model where the predicted values are identical to the observed. Therefore, if the residual difference is small enough, the goodness of fit test will not be significant, indicating that the model fits the data. We conclude that the model fits reasonably well because the goodness-of-fit chi-squared test is not statistically significant. If the test had been statistically significant, it would indicate that the data do not fit the model well. In that situation, we may try to determine if there are omitted predictor variables, if our linearity assumption holds and/or if there is an issue of over-dispersion.
with(model.frequency, cbind(res.deviance = deviance, df = df.residual, p = pchisq(deviance, df.residual, lower.tail = FALSE)))
 We can also test the overall effect of zone by comparing the deviance of the full model with the deviance of the model excluding zone. The chi-square test indicates that zone, taken together, is a statistically significant predictor of claims.
 update model.frequency model dropping zone

```{r}
model.f2 <- update(model.frequency, . ~ . - Zone)
```

test model differences with chi square test 

```{r}
anova(model.f2, model.frequency, test = "Chisq")
```

# GLM Gamma on payment

```{r}
model.payment =glm(Payment~ factor(Kilometres)+Zone+factor(Bonus)+factor(Make), data=Swedish[Swedish$Payment > 0, ],family = Gamma("log"), weights = Claims)
summary(model.payment)
```

As before, we use the residual deviance to perform a goodness-of-fit test for the overall model. 
with(model.payment, cbind(res.deviance = deviance, df = df.residual, p = pchisq(deviance, df.residual, lower.tail = FALSE)))
 We can also test the overall effect of zone by comparing the deviance of the full model with the deviance of the model excluding zone. The chi-square test indicates that zone, taken together, is a statistically significant predictor of payment.
 update model.payment model dropping zone

```{r}
model.p2 <- update(model.payment, . ~ . - Zone)
```

 test model differences with chi square test 

```{r}
anova(model.p2, model.payment, test = "Chisq")
```
