library("insuranceData")
library("glmnet")
library('leaps')
library('car')

data("dataOhlsson", package = "insuranceData")

#analyses des variables
hist(dataOhlsson$agarald) 

#on enleve les données où age c'est plus petite que 16
dataOhlsson <- dataOhlsson[dataOhlsson$agarald >= 16 ,]
n <- length(dataOhlsson$agarald)

summary(dataOhlsson$agarald)
age_names <- c("age 16-20","age 21-24","age 25-29","age 30-34",
               "age 35-39", "age 40-49","age 50-59","age 60+")
cuts <- c(15, 20, 24, 29, 34, 39, 49, 59, 99)
dataOhlsson$age_prop_factor <- cut(dataOhlsson$agarald, breaks = cuts, labels = age_names, right = FALSE)
summary(dataOhlsson$age_prop_factor)
for(i in seq(1,length(cuts)-1)){
  print(age_names[i])
  print(sum(dataOhlsson$agarald >= cuts[i] & dataOhlsson$agarald < cuts[i+1]))
}

#dataOhlsson$kon # variable binaire pour le sexe: K kvinna=femme e M man=homme
#sex est dejà un facteur
# we are going to rename the variable
colnames(dataOhlsson)
colnames(dataOhlsson)[2] <- "sexe"

zone_names <- c("zone_1","zone_2","zone_3", "zone_4","zone_5","zone_6", "zone_7")
cuts <- seq(1,8)
dataOhlsson$zone_factor <- cut(dataOhlsson$zon, breaks = cuts, labels = zone_names, right = FALSE)
summary(dataOhlsson$zone_factor)
for(i in seq(1,7)){
  print(zone_names[i])
  print(sum(dataOhlsson$zon == i))
}

hist(dataOhlsson$mcklass)
mcklass_names <- c("mcklass_1","mcklass_2","mcklass_3", "mcklass_4","mcklass_5","mcklass_6", "mcklass_7")
dataOhlsson$mcklass_factor <- cut(dataOhlsson$mcklass, breaks = cuts, labels = mcklass_names,
                                  right = FALSE)
summary(dataOhlsson$mcklass_factor)
for(i in seq(1,7)){
  print(mcklass_names[i])
  print(sum(dataOhlsson$mcklass == i))
}

hist(dataOhlsson$fordald)
age_veih_names <- c("age_veih 0-5","age_veih 6-10","age_veih 11-15","age_veih 16-20","age_veih 21+")
cuts <- c(0, 5, 10, 15, 20, 100)
dataOhlsson$age_veih_factor <- cut(dataOhlsson$fordald, breaks = cuts, labels = age_veih_names,
                                   right = FALSE)
summary(dataOhlsson$age_veih_factor)
for(i in seq(1,length(cuts)-1)){
  print(age_veih_names[i])
  print(sum(dataOhlsson$fordald >= cuts[i] & dataOhlsson$fordald < cuts[i+1]))
}

hist(dataOhlsson$bonuskl) # bonus class
bonus_names <- c("bonus_1","bonus_2","bonus_3", "bonus_4","bonus_5","bonus_6", "bonus_7")
cuts <- seq(1,8)
dataOhlsson$bonus_factor <- cut(dataOhlsson$bonuskl, breaks = cuts, labels = bonus_names, right = FALSE)
summary(dataOhlsson$bonus_factor)
for(i in seq(1,length(cuts)-1)){
  print(bonus_names[i])
  print(sum(dataOhlsson$bonuskl >= cuts[i] & dataOhlsson$bonuskl < cuts[i+1]))
}

hist(dataOhlsson$duration)
duration_names <- c("duration 0-0.5","duration 0.5-1","duration 1-2","duration 2-3",
                    "duration 3-5","duration 5+")
cuts <- c(0, 0.5, 1, 2,3, 5,40)
dataOhlsson$duration_factor <- cut(dataOhlsson$duration, breaks = cuts, labels = duration_names,
                                   right = FALSE)
summary(dataOhlsson$duration_factor)
for(i in seq(1,length(cuts)-1)){
  print(duration_names[i])
  print(sum(dataOhlsson$duration >= cuts[i] & dataOhlsson$duration < cuts[i+1]))
}

#variable explicative
hist(dataOhlsson$antskad[dataOhlsson$antskad >0])
hist(dataOhlsson$skadkost[dataOhlsson$antskad >0])

# on va a creer une variable binaire 0-1

dataOhlsson$claims <- dataOhlsson$antskad
dataOhlsson$claims[ dataOhlsson$antskad > 0 ] <- 1
dataOhlsson$claims <- as.factor(dataOhlsson$claims)
summary(dataOhlsson$antskad)

# on selectionne les endroits où claims=1  
data_2 <- dataOhlsson[dataOhlsson$skadkost > 0 , ]
head(data_2)

names(dataOhlsson)
######################

var_used <- c("age_prop_factor", "sexe", "zone_factor", "mcklass_factor","age_veih_factor","bonus_factor",
              "duration_factor")
names(dataOhlsson)

model_logistic = glm(
  formula = claims ~ age_prop_factor + sexe + zone_factor + mcklass_factor + age_veih_factor +
                bonus_factor + duration_factor,
  data = dataOhlsson,
  family = binomial
)

summary.glm(model_logistic)
names(data_2)

model_gamma = glm(
  formula = skadkost ~ age_prop_factor + sexe + zone_factor + mcklass_factor + age_veih_factor +
    bonus_factor + duration_factor,
  data = data_2,
  family = Gamma
)

model_gauss = lm(
  formula = skadkost ~ age_prop_factor + sexe + zone_factor + mcklass_factor + age_veih_factor +
    bonus_factor + duration_factor,
  data = data_2
)

summary.glm(model_gamma)
#anova(model_gamma)
Anova(model_gamma, test.statistic = "LR")


#############################
# model selection for logistic regression
#############################

model_logistic = glm( formula = claims ~ age_prop_factor + sexe + zone_factor +
                        mcklass_factor + age_veih_factor + bonus_factor + duration_factor,
                      data = dataOhlsson,
                      family = binomial
)

summary.glm(model_logistic)
#anova(model_logistic, test = 'Chisq')
Anova(model_logistic, test.statistic = "LR")

# La fonction anova appliquée à une sortie de la fonction glm réalise une "analye de la déviance".
# Le principe est le même que l'analyse de la variance en remplaçant les tests de modèles emboîtés
# par des tests de rapport de vraisemblance : comparaison de la déviance à une loi du  χ2  avec le
# bon nombre de degrés de liberté.

model_logistic_with_interaction = glm( formula = claims ~ age_prop_factor + sexe + zone_factor +
                        mcklass_factor * age_veih_factor + bonus_factor * duration_factor,
                      data = dataOhlsson,
                      family = binomial
)

#anova(model_logistic_with_interaction, test = 'Chisq')
Anova(model_logistic_with_interaction, test.statistic = "LR")

model_logistic_select <- regsubsets(claims ~ age_prop_factor + sexe + zone_factor +
                      mcklass_factor + age_veih_factor + bonus_factor + duration_factor,
                    nbest = 1,       # 1 best model for each number of predictors
                    data = dataOhlsson,
                    nvmax = NULL    # NULL for no limit on number of variables
                    )

plot(model_logistic_select, scale = "Cp")
plot(model_logistic_select, scale = "bic")
# we can see that for each criterion we don't really have a variable that is completey excluded
# this is why we have decided to keep the model with the whole variables


###############
# we must see if all the classes are well represented

summary(data_2$age_prop_factor)
unique(data_2$age_prop_factor)

summary(data_2$sexe)
unique(data_2$sexe)

summary(data_2$zone_factor)
unique(data_2$zone_factor)

summary(data_2$mcklass_factor)
unique(data_2$mcklass_factor)

summary(data_2$age_veih_factor)
unique(data_2$age_veih_factor)

summary(data_2$bonus_factor)
unique(data_2$bonus_factor)

summary(data_2$duration_factor)
unique(data_2$duration_factor)

###############
# gamma - gauss

model_gamma = glm(
  formula = skadkost ~ age_prop_factor + sexe + zone_factor + mcklass_factor + age_veih_factor +
    bonus_factor + duration_factor,
  data = data_2,
  family = Gamma(link = log)
)

#anova(model_gamma, test = 'Chisq')
Anova(model_gamma, test.statistic = "LR")


model_gamma_with_interactions = glm(
  formula = skadkost ~ age_prop_factor + sexe + zone_factor + mcklass_factor * age_veih_factor +
    bonus_factor * duration_factor,
  data = data_2,
  family = Gamma(link = log)
)

#anova(model_gamma_with_interactions, test = 'Chisq')
Anova(model_gamma_with_interactions, test.statistic = "LR")

model_AIC <- step(model_gamma, scale = 0, #scope
                  direction = c("both", "backward", "forward"),
                  trace = 1, keep = NULL, steps = 1000, k = 2)

model_gamma_select <- regsubsets(skadkost ~ age_prop_factor + sexe + zone_factor +
                                   mcklass_factor + age_veih_factor + bonus_factor + duration_factor,
                                 nbest = 1,       # 1 best model for each number of predictors
                                 data = data_2,
                                 nvmax = 11,  # NULL for no limit on number of variables
                                 method = 'exhaustive'
)
# we can't use R2 and R2adj for other models than linear models
plot(model_gamma_select, scale = "Cp")
plot(model_gamma_select, scale = "bic")


model_gauss_reducted = glm(
  formula = skadkost ~ age_prop_factor + sexe + mcklass_factor + age_veih_factor +
    duration_factor,
  data = data_2,
  family = 'Gamma'
)

#anova(model_gauss_reducted, test = 'Chisq')
Anova(model_gauss_reducted, test.statistic = "LR")

set.seed(7)
allrows <- 1:nrow(data_2)
trainrows <- sample(allrows, replace = F, size = 0.6*length(allrows))
testrows <- allrows[-trainrows]
train <- data_2[trainrows,]
test <- data_2[testrows,]

model_gamma_train_all = glm(
  formula = skadkost ~ age_prop_factor + sexe + zone_factor + mcklass_factor + age_veih_factor +
    bonus_factor + duration_factor,
  data = train,
  family = Gamma(link = log)
)

new <- predict( object = model_gamma_train_all,
         newdata = test
         )
mse <- c()
mse[1] <- mean((new - test$skadkost)^2)


model_gamma_train_cp = glm(
  formula = skadkost ~ age_prop_factor + sexe + mcklass_factor + age_veih_factor +
    duration_factor,
  data = train,
  family = Gamma(link = log)
)

new <- predict( object = model_gamma_train_cp,
                newdata = test
)
mse[2] <- mean((new - test$skadkost)^2)

model_gamma_train_bic = glm(
  formula = skadkost ~ age_veih_factor,
  data = train,
  family = Gamma(link = log)
)

new <- predict( object = model_gamma_train_bic,
                newdata = test
)
mse[3] <- mean((new - test$skadkost)^2)

model_gamma_train_bic_1 = glm(
  formula = skadkost ~ age_veih_factor + duration_factor,
  data = train,
  family = Gamma(link = log)
)

new <- predict( object = model_gamma_train_bic_1,
                newdata = test
)
mse[4] <- mean((new - test$skadkost)^2)

model_gamma_train_aic = glm(
  formula = skadkost ~ age_prop_factor + age_veih_factor,
  data = train,
  family = Gamma(link = log)
)
new <- predict( object = model_gamma_train_aic,
                newdata = test
)
mse[5] <- mean((new - test$skadkost)^2)

model_gamma_train_aic_2 = glm(
  formula = skadkost ~ age_prop_factor + zone_factor+ age_veih_factor,
  data = train,
  family = Gamma(link = log)
)
new <- predict( object = model_gamma_train_aic_2,
                newdata = test
)
mse[6] <- mean((new - test$skadkost)^2)

plot(mse)

mse
