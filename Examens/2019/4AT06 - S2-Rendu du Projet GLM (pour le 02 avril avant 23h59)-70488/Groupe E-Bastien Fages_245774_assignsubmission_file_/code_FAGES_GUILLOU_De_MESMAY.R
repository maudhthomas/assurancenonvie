library(ModelMetrics)
library(ResourceSelection)
library(MASS)
library(ggplot2)
library(ROCR)
library(caTools)


#Chargement des donnees
load("C:/Users/Bastien/Documents/ISUP/Assurance non-vie/ausprivauto0405.rda")
data<-ausprivauto0405
print(head(data))


### 9 variables : ###
#Exposure, The number of policy years.
#VehValue, The vehicle value in thousand of AUD.
#VehAge, The vehicle age group.
#VehBody,The vehicle body group.
#Gender,The gender of the policyholder.
#DrivAge,The age of the policyholder.
#ClaimOcc,Indicates occurence of a claim.
#ClaimNb,The number of claims.
#ClaimAmount,The sum of claim payments.

#Regroupement des variables quantitatives en classes
ClaimOcc_classe<-cut(data$ClaimOcc,2)
ClaimNb_classe<-cut(data$ClaimNb,4)
VehValue_classe<-cut(data$VehValue,c(-0.0001,2.500,5.000,10.000,35.000))


#Cr�ation de notre fichier de donn�es, on a donc 5 variables explicatives
#Nous n'utilisons pas la variable Exposure
donnees<-data[,c(3,4,5,6,7,9)]
donnees["VehValue"]<-VehValue_classe






#S�paration des donn�es en 2/3 et 1/3. Nous utilisons pour cela une m�thode al�atoire qui s�pare notre 
#jeu de donn�es. NOus prenons les 2/3 de nos donn�es pour entrainer nos mod�le et 1/3 pour 
sample = sample.int(n = nrow(donnees), size = .66*nrow(donnees), replace = F)
data_train = donnees[sample,]
data_test = donnees[-sample,]

### I.SURVENANCE DES SINISTRES ###
### 1) Cas negative binomial ###

#Les lignes suivantes permettent d'observer la survenance d'un sinistre en fonction des variables explicatives
par(mfrow=c(2,3))
ggplot(data=data_train,aes(x=data_train$VehValue,y=data_train$ClaimOcc))+geom_jitter()
ggplot(data=data_train,aes(x=data_train$VehAge,y=data_train$ClaimOcc))+geom_jitter()
ggplot(data=data_train,aes(x=data_train$VehBody,y=data_train$ClaimOcc))+geom_jitter()
ggplot(data=data_train,aes(x=data_train$Gender,y=data_train$ClaimOcc))+geom_jitter()
ggplot(data=data_train,aes(x=data_train$DrivAge,y=data_train$ClaimOcc))+geom_jitter()


#Nous utilisons ici un mod�le de type negative binomial
#Nous utilisons dans un premier temps toutes les variables que nous avons � notre dispostion
glmnb=glm.nb(ClaimOcc~VehValue+VehAge+VehBody+Gender+DrivAge,data=data_train)

#Nous �tudions les r�sultats de notre glm
summary(glmnb)
anova(glmnb, test = "Chisq")

#Nous regardons les r�sidus 
res <- residuals(glmnb,type="deviance")

#Nous affichons le carr� des r�sidus 
par(mfrow=c(1,1))
plot(res^2,ylab="Residuals Deviance")
abline(h=1, col='red')

length(which(res^2>1))


#Nous faisons ici des pr�dictions gr�ce au glm que nous venons d'entrainer
pred <- predict(glmnb, data_test, type="response")
head(pred)


#Ces lignes nous servent pour fixer le seuil que nous allons utiliser dans la pr�diction
pred_trainnb <- predict(glmnb, data_train, type="response")
nrow(data_train[data_train$ClaimOcc>0,])
length(pred_trainnb[pred_trainnb > 0.0874])


#Nous �tudions maintenant nos pr�dictions nottament grace � la confusion Matrix
nrow(data_test[data_test$ClaimOcc>0,])
length(pred[pred > 0.0874])
M=confusionMatrix(ifelse(pred > 0.0874 ,TRUE,FALSE), data_test$ClaimOcc>0)
M

#Nous regardons � pr�sent le taux de faux-positifs dans nos pr�dictions
ROCRpred <- prediction(pred, data_test$ClaimOcc>0)
ROCRperf <- performance(ROCRpred, 'tpr','fpr')

plot(0:1,0:1,xlab="False Positive Rate", ylab="True Positive Rate",cex=.5)
plot(ROCRperf, colorize = TRUE)
survenance=ifelse(data_test$ClaimOcc>0, 1,0)
colAUC(survenance, pred, plotROC = F)

### 2) Cas quasi-poisson ###

#Nous utilisons ici un mod�le de type quasi-poisson 
#Nous utilisons dans un premier temps toutes les variables que nous avons � notre dispostion
glmqp=glm(formula = ClaimOcc ~ VehAge + VehBody + Gender + DrivAge + VehValue, family = quasipoisson(), data=data_train)

glmqp2=glm(formula = ClaimOcc ~  VehAge + VehBody + DrivAge, family = quasipoisson(), data=data_train)

#Nous �tudions les r�sultats de notre glm
summary(glmqp)
summary(glmqp2)
anova(glmqp, test = "Chisq")


#Nous regardons les r�sidus 
res <- residuals(glmqp,type="deviance")

#Nous affichons le carr� des r�sidus 
par(mfrow=c(1,1))
plot(res^2,ylab="Residuals Deviance")
abline(h=1, col='red')

length(which(res^2>1))


pred_train <- predict(glmqp, data_train, type="response")
nrow(data_train[data_train$ClaimOcc>0,])
length(pred_train[pred_train > 0.0874])


pred_train2 <- predict(glmqp2, data_train, type="response")
nrow(data_train[data_train$ClaimOcc>0,])
length(pred_train2[pred_train2 > 0.0858])


#Nous faisons ici des pr�dictions gr�ce au glm que nous venons d'entrainer
pred_qp <- predict(glmqp, data_test, type="response")
head(pred_qp)

pred_qp2 <- predict(glmqp2, data_test, type="response")
head(pred_qp2)

#Nous �tudions maintenant nos pr�dictions nottament grace � la confusion Matrix
nrow(data_test[data_test$ClaimOcc>0,])
length(pred_qp[pred_qp > 0.0874])
M=confusionMatrix(ifelse(pred_qp > 0.0874 ,TRUE,FALSE), data_test$ClaimOcc>0)
M

length(pred_qp[pred_qp2 > 0.0858])
M2=confusionMatrix(ifelse(pred_qp2 > 0.0858 ,TRUE,FALSE), data_test$ClaimOcc>0)
M2
#Nous regardons � pr�sent le taux de faux-positifs dans nos pr�dictions
ROCRpred <- prediction(pred_qp, data_test$ClaimOcc>0)
ROCRperf <- performance(ROCRpred, 'tpr','fpr')

plot(0:1,0:1,xlab="False Positive Rate", ylab="True Positive Rate",cex=0.082)
plot(ROCRperf, colorize = TRUE)
survenance=ifelse(data_test$ClaimOcc>0, 1,0)
colAUC(survenance, pred_qp, plotROC = F)

### MONTANT DES SINISTRES ###

##Utilisation da la Gamma
#Ne marche que pour les montants > 0, pond�ration par la fr�quence(=nb de sinistres normalis�s)

data_train_gamma<-subset(data_train,data_train$ClaimAmount>0)
data_test_gamma<-subset(data_train,data_test$ClaimAmount>0)

##Modele initiale avec toutes les variables
fit <- glm(formula = ClaimAmount~VehAge+VehBody+Gender+DrivAge+VehValue, data = data_train_gamma, family = Gamma(link=log))
summary(fit) #trop de p_value > 0.05, on ne rejette pas les hypotheses de nullit�s des coeff

qchisq(0.95,2957) #quantile de 3084,62, on a une d�viance trop �lev�e (sup au quantile)

## Modele pas optimal -> processus de selection de variables
fit_aic_backward <- stepAIC(fit,direction="backward",trace=FALSE) #suppression de 1 variable : VehBody
fit_aic_backward <- glm(formula = ClaimAmount~VehAge+Gender+DrivAge+VehValue, data = data_train_gamma, family = Gamma(link=log))
summary(fit_aic_backward)
c(AIC(fit),AIC(fit_aic_backward)) 
# la suppression de la variable entraine une augmentation de l'AIC ainsi que de la d�viance. incoh�rence ?

#test annova de maximum de vraisemblance 
anova(fit,fit_aic_backward,test="LRT") #On garde le modele avec toutes les variables ?

##Suppresion des individus atypiques et ab�rants et des points leviers
plot(fit_aic_backward) # 3 individus atypiques, que l'on retire

#recherche de points leviers
plot(influence(fit_aic_backward)$hat,type="h",ylab="hii")
n=length(data_train_gamma$ClaimAmount)
p=n-fit_aic_backward$df.resid
abline(h=3*p/n, col='red')   #trop de points leviers


indiv_a_sup <-c (1102,1875,2596)  #on retire les individus atypiques (~~)
data_train_gamma <- data_train_gamma[-indiv_a_sup,]


fit_aic_backward <- glm(formula = ClaimAmount~VehAge+Gender+DrivAge+VehValue, data = data_train_gamma, family = Gamma(link=log))
plot(fit_aic_backward)
summary(fit_aic_backward) #trop de p_value > 0.05, on ne rejette pas les hypotheses de nullit�s des coeff
#Changement : AIC passe 51048 de � 50869
#             D�viance passe de 4660.9 � 4524.27
#Baisse de l'AIC ainsi que de la d�viance, m�me si cette derniere reste �lev�e

1-pchisq(deviance(fit_aic_backward),fit_aic_backward$df.resid)

#Conclusion finale apr�s suppression d'individus et de variables: nous avons supprim� 0 variables et 3 individus, 
#le meilleur mod�le est donc le mod�le fit obtenu suite � ces suppressions, 
#l'AIC et la d�viance de ce mod�le restent sont tr�s �l�v�s ce qui laisse supposer une mauvaise mod�lisation.

##Pr�diction sur le jeu de donn�es test
prediction <- predict(fit_aic_backward, newdata=data_test_gamma, type="response")
dif<-abs(prediction-data_test_gamma$ClaimAmount)

res<-cbind(prediction,data_test_gamma$ClaimAmount, dif)

summary(dif)

summary(data_test_gamma$ClaimAmount)
summary(prediction)

stand_error<-sqrt(sum(dif^2)/length(dif))
stand_error

####Pond�ration par la fr�quence
freq <- nrow(data_test_gamma) / nrow(data_test)
freq
summary(prediction)
prediction <- prediction * freq
summary(prediction)

#dif<-abs(prediction-data_test_gamma$ClaimAmount*freq)
dif<-prediction-data_test_gamma$ClaimAmount*freq
summary(dif)
summary(data_test_gamma$ClaimAmount*freq)
dif2<-(prediction-data_test_gamma$ClaimAmount*freq)
summary(dif2)