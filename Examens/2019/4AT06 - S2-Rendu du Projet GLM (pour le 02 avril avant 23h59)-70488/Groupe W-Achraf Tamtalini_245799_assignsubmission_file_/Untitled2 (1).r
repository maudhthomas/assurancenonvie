
library(readr)
library(dplyr)

MyData<-read.delim("C:/Users/Admin/Downloads/motorins.txt")
print(head(MyData))
summary(MyData)
MyData<-na.omit(MyData)

levels(MyData$Kilometres); levels(MyData$Zone); levels(MyData$Make)

fit <- glm(formula = Claims~Kilometres+Zone+Bonus+Make+Insured, data = MyData, family = poisson())
fit

summary(fit)

anova(fit, test="Chisq")


MyData <- filter(MyData, Payment > 0, )
summary(MyData)

fit2<-glm(formula=Payment~Kilometres+Zone+Make+Bonus, data=MyData, family=Gamma())

summary(fit2)

"Nous supprimons la variable zone qui n'est pas significatif dans le modèle précédent.
Les coefficient obtenus sont cette fois-ci tous significatifs. En revanche, l'AIC et la déviance n'ont pas été améliorés."
fit3<-glm(formula=Payment~Kilometres+Make+Bonus, data=MyData, family=Gamma())
summary(fit3)

anova(fit3, test="Chisq")


