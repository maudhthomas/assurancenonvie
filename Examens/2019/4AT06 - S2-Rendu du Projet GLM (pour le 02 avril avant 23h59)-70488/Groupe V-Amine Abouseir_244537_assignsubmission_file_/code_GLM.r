setwd("C:/Users/Amine Abouseir/Desktop/GLM")
rm(list=objects())
library(readr)

motorin <- read.csv("MotorClaimData.csv",sep=";")

motorin$AvPayment <- motorin$Payment/motorin$NClaims
motorin$AvPayment[motorin$NClaims==0] <- NA
motorin$Payment <- NULL

print(head(motorin))

levels(motorin$Kilometres) ; levels(motorin$Zone) ; levels(motorin$Model)

fit <- glm(formula = NClaims~Kilometres+Zone+Bonus+Model+Insured, data = motorin, family = poisson())
fit

summary(fit)

anova(fit, test = "Chisq")

fit <- glm(formula = AvPayment~Kilometres+Zone+Bonus+Model+Insured, data = motorin, family = Gamma())
fit

summary(fit)

anova(fit, test="Chisq")