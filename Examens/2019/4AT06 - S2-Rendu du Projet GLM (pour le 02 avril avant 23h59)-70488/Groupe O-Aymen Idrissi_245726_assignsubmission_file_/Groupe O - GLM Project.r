data_with_classes <- read.delim("~/Data_Groupe_O_with_classes.csv", sep=",")

data_with_classes$age_class= factor(data_with_classes$age_class)
data_with_classes$bmi_class= factor(data_with_classes$bmi_class)
data_with_classes$steps_class= factor(data_with_classes$steps_class)
data_with_classes$smoker= factor(data_with_classes$smoker)
data_with_classes$region= factor(data_with_classes$region)
data_with_classes$sex= factor(data_with_classes$sex)
data_with_classes$children= factor(data_with_classes$children)

levels(factor(data_with_classes$age_class));
levels(factor(data_with_classes$bmi_class));
levels(factor(data_with_classes$steps_class));
levels(factor(data_with_classes$smoker));
levels(factor(data_with_classes$region));
levels(factor(data_with_classes$sex));
levels(factor(data_with_classes$children))


fit <- glm(formula = insuranceclaim ~age_class+bmi_class+steps_class+smoker+region+sex+children, data = data_with_classes, family = poisson())
fit
summary(fit)
plot(fit)

fit <- glm(formula = charges ~age_class+bmi_class+steps_class+smoker+region+sex+children, data = data_with_classes, family = Gamma())
fit
summary(fit)
plot(fit)



