---
title: "projet_anv"
author: "Issam"
date: "2 avril 2019"
output: html_document
---
# Projet Assurance Non-Vie
```{r message=TRUE, warning=TRUE, include=FALSE, paged.print=TRUE}
rm(list=ls())
library(CASdatasets)
library(MASS)
data(norauto)

#Simplification base de donnée
table<-as.data.frame(norauto)
table$Male<-replace(table$Male,which(table$Male==0),c("F"))
table$Male<-replace(table$Male,which(table$Male==1),c("H"))
table$Young<-replace(table$Young,which(table$Young==0),c("O"))
table$Young<-replace(table$Young,which(table$Young==1),c("Y"))
data<-table
data$Expo<-as.numeric(data$Expo)
```
## 1) Presentation du jeu de donnee
```{r}
head(data)
```
La base de donnée contient 4 variables explicatives qualitatives et 1 quantitative. Les deux variables a modeliser sont les variables "NbClaim" et "ClaimAmount"
## 1) Modelisation de la survenance
Afin de pouvoir tester les prédictions que nous allons réaliser, on créer une base travail et une base test.
```{r}
set.seed(101)
v_bern<-rbinom(n=length(data$Male),size=1,prob=0.9)
base_travail<-subset(data,v_bern>0)
base_test<-subset(data,v_bern<1)
row.names(base_travail)<-c(1:length(base_travail$Male))
row.names(base_test)<-c(1:length(base_test$Male))
```
### a) GLM Poisson et Binomiale Negative
On applique 2 glm sur notre jeu de donnée
```{r}
glm1_Nb<-glm(formula = NbClaim ~ DistLimit + GeoRegion + Male + Young + offset(log(Expo)), family=poisson, data=base_travail)
glm2_Nb<-glm.nb(formula = NbClaim ~ DistLimit + GeoRegion + Male + Young, data=base_travail, maxit = 10000)
summary(glm1_Nb)
summary(glm2_Nb)
```
On voit que les deux modeles sont bien ajuste
```{r}
step(glm1_Nb,direction="backward",trace=1)
glm1_Nb$deviance-qchisq(0.95,length(base_travail$Male)-5)<0 #Le modèle est bien ajusté
glm2_Nb$deviance-qchisq(0.95,length(base_travail$Male)-5)<0 #Le modèle est bien ajusté
```
On choisit le GLM Poisson
```{r}
glm1_Nb$aic-glm2_Nb$aic<0 #Le modele 1 est meilleur que le modele 2
```
Le test de l'anova nous dit de garder toutes les variables.
```{r}
anova(glm1_Nb,test="Chisq")
```
On peut voir qu'il n'y a que tres peu d'individus qui contribuent au mauvais ajustement du modele ce qui nous arrange.
```{r}
res <- residuals(glm1_Nb,type="deviance") #résidus de déviance
plot(res^2,ylab="Residuals Deviance")
abline(h=1, col='red')
length(which(res^2<1))
```
### b) Prediction
On definit des seuils pour notre prevision de sinistres en se basant sur la base_travail
```{r}
fit <- fitted(glm1_Nb)
fit<-replace(fit,which(fit>0.31),c(2))
fit<-replace(fit,which(0.1101<fit & fit<0.31),c(1))
fit<-replace(fit,which(fit<0.1101),c(0))
length(which(fit==1))
length(which(base_travail$NbClaim==1))
a<-0.31
b<-0.1105
```
On test la coherence des seuils etablis
```{r}
pred <- predict(glm1_Nb, base_test, type="response")
pred<-replace(pred,which(pred>a),c(2))
pred<-replace(pred,which(b<pred & pred<a),c(1))
pred<-replace(pred,which(pred<b),c(0))
length(which(pred==1))
length(which(base_test$NbClaim==1))
```
## 2) Modelisation des Montants
On s'interesse d'abord à l'allure de la distribution des montants
```{r}
data<-subset(data,data$NbClaim>0)
row.names(data)<-c(1:length(data$NbClaim))
hist(data$ClaimAmount, breaks=seq(from=0,to=700000,by=1000),xlim=range(0,150000))# a vue d'oeil testons une gamma

```
On normalise le montant des sinistres pour avoir un montant unitaire et on convertit la variable "Expo" en variables qualitative.
```{r}
data$ClaimAmount<-data$ClaimAmount/data$NbClaim
q<-quantile(data$Expo,probs=seq(0,1,1/3))
data$Expo<-cut(data$Expo,c(q[1],q[2],q[3],q[4]),labels=c("recent","intermediaire","ancien"))
```

```{r}
glm1_Montant<-glm(formula = ClaimAmount ~ DistLimit + GeoRegion + Male + Young+ Expo, family=Gamma(), data=data)
sum(fitted(glm1_Montant))
res <- residuals(glm1_Montant,type="deviance") #résidus de déviance
plot(res^2,ylab="Residuals Deviance")
abline(h=1, col='red')
length(which(res^2<1))
```

```{r}
sum(pred*predict(glm1_Montant,base_test,type="response"))/sum(base_test$ClaimAmount) #methode individuelle
sum(pred)*mean(predict(glm1_Montant,base_test,type="response"))/sum(base_test$ClaimAmount) #methode collective

classe_Male<-c("H","F")
classe_Young<-c("O","Y")
classe_DistLimit<-c("8000 km","12000 km","16000 km","20000 km","25000-30000 km","no limit")
classe_GeoRegion<-c("High+","High-","Medium+","Medium-","Low+","Low-")
classe_DistLimit[2]<-as.vector(data$DistLimit[1])
classe_DistLimit[6]<-as.vector(data$DistLimit[5])
classe_DistLimit[4]<-as.vector(data$DistLimit[6])
classe_DistLimit[5]<-as.vector(data$DistLimit[7])
classe_DistLimit[3]<-as.vector(data$DistLimit[45])
classe_DistLimit[1]<-as.vector(data$DistLimit[150])
s<-c(0)
for(i in 1:2){
  for(j in 1:2){
    for(k in 1:6){
      for(l in 1:6){
        nb<-pred[which(base_test$Male==classe_Male[i] & base_test$Young==classe_Young[j] & base_test$DistLimit==classe_DistLimit[k] & base_test$GeoRegion==classe_GeoRegion[l])]
        mt<-predict(glm1_Montant,base_test,type="response")[which(base_test$Male==classe_Male[i] & base_test$Young==classe_Young[j] & base_test$DistLimit==classe_DistLimit[k] & base_test$GeoRegion==classe_GeoRegion[l])]
        s<-c(s,sum(nb)*mean(mt))
      }
    }
  }
}
s<-replace(s,which(s=="NaN"),c(0))
sum(s)/sum(base_test$ClaimAmount)
```