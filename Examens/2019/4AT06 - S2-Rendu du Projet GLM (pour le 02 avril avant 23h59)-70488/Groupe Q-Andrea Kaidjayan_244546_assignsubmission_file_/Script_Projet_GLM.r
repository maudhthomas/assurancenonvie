###############################################################################################################
#                                    ASSURANCE NON-VIE : PROJET GLM
#                    réalisé par : Joel Hounkanli, Raphael Tumminello et Andréa Kaidjayan
#                                           Avril 2019
###############################################################################################################


###############################################################################################################
#                         PARTIE 1 : Traitement du jeu de donnnées
###############################################################################################################

# Importation
data_auto<-read.csv2("C:/Users/Joel Hounkanli/Documents/Projet Glm/baseFREQ.csv")

set.seed(101)

# Traitement de la base (valeurs manquantes, créations de classes pour les variables quantitatives)
data_auto<-data_auto[data_auto$age-data_auto$duree_permis>=18,]

data_auto$sin[data_auto$nbsin == 0]<-FALSE
data_auto$sin[!(data_auto$nbsin == 0)]<-TRUE

data_auto$age_rec <- cut(data_auto$age, include.lowest=FALSE,  right=TRUE,
                         breaks=c(18, 36, 44, 54, 87))

data_auto$age_vehicule<-as.integer(2013-data_auto$annee_vehicule)

data_auto$age_vehicule_rec <- cut(data_auto$age_vehicule, include.lowest=FALSE,  right=TRUE,
                                  breaks=c(10, 15, 18, 22, 68))

data_auto$exposition<-as.numeric(as.character(data_auto$exposition))

data_auto$duree_permis_rec <- cut(data_auto$duree_permis, include.lowest=FALSE,  right=TRUE,
                                          breaks=c(0, 5, 10, Inf))

data_auto<-na.omit(data_auto)

# Création de deux sous-bases (test et apprentissage)
sample <- sample.int(n = nrow(data_auto), size = floor(.70*nrow(data_auto)), replace = F)

data_auto_l<-data_auto[sample,]
data_auto_t<-data_auto[-sample,]


###############################################################################################################
#                         PARTIE 2 : Modèles linéaires généralisés
###############################################################################################################

#############################################################
# 1ère Étude : Modélisation de la survenance, GLM Poisson
#############################################################


# GLM Poisson avec prise en compte de toutes les variables qualitatives

fit_pois <- glm(formula = nbsin~age_rec+type_territoire+presence_alarme+age_vehicule_rec+utilisation
           +duree_permis_rec+sexe+langue+freq_paiement+type_prof+offset(log(exposition)),
           data = data_auto_l, family = poisson(link = "log"))
summary(fit_pois)

# Sélection du meilleur modèle pour le critère AIC avec une méthode backward

fit_pois2 = step(fit_pois, direction = "backward")
summary(fit_pois2)
res<- c(24932,24962,24948,24973,24942,24985,24912,24927,24923,24923,24927,24932)
length(res)
etape <- c(1:12)

#Visualisation des AIC

plot(etape,res,ylab = "AIC pour chacun des modeles testes", xlab = "Numero des etapes")
abline (h = 24922 , col = "red")
c(aic1=AIC(fit_pois),aic2=AIC(fit_pois2))


# Analyse du modèle fit_pois2 (tests, déviance)

stat <- 2*(logLik(fit_pois)-logLik(fit_pois2))
1-pchisq(stat,df=length(fit_pois$coef)-length(fit_pois2$coef))

# Anova pour retrouver les résultats précédents

anova(fit_pois,fit_pois2,test="LRT")

# Analyse des residus

res <- residuals(fit_pois2,type="deviance") 
plot(res^2,ylab="Residuals Deviance",ylim=c(0,5))
abline(h=2, col='red')
length(which(res^2>2))

# Pourcentage d'individus contribuant au mauvais ajustement du modèle

100*length(which(res^2>2))/length(res)

# Prediction sur chaque sous-base
pred_t = predict(fit_pois2,data_auto_t,type="response")
head(pred_t)
summary(pred_t)
summary(data_auto_t$nbsin)
c(moy_prediction = mean(pred_t),moy_realite = mean(data_auto_t$nbsin) )


pred_l<-predict(fit_pois2,data_auto_l,type="response")
summary(pred_l)
length(which(pred_l<0.28))
length(which(data_auto_l$nbsin==0))
length(which(data_auto_l$nbsin>=1))
length(which(data_auto_l$nbsin==0))/length(data_auto_l$nbsin==0)


seuil<-0.28
pred_t[pred_t<seuil]<-FALSE
pred_t[pred_t>=seuil]<-TRUE

100*(sum(pred_t == data_auto_t$sin)/nrow(data_auto_t))


#####################################################################################
# 2ème Étude : Modélisation du montant des sinistres en cas de survenance : Glm Gamma 
#####################################################################################


#Estimation cout d'un sinistre en cas de survenance

#conversion du type de la variable

data_auto_l$cout<-as.numeric(as.character(data_auto_l$cout))
data_auto_t$cout<-as.numeric(as.character(data_auto_t$cout))


# Selection des couts non nuls

data_auto_l_gam<-data_auto_l[data_auto_l$nbsin!=0,]
data_auto_t_gam<-data_auto_t[data_auto_t$nbsin!=0,]

# Traitement des valeurs manquantes

data_auto_l_gam <- na.omit(data_auto_l)
data_auto_t_gam <- na.omit(data_auto_t)

# Création du coût moyen pour chaque assuré

data_auto_l_gam$cout_moy<-data_auto_l_gam$cout/(data_auto_l_gam$nbsin)
data_auto_t_gam$cout_moy<-data_auto_t_gam$cout/(data_auto_t_gam$nbsin)

summary(data_auto_t_gam$cout_moy)


# GLM Gamma 
fit_gam <-glm(formula = cout_moy~age_rec+type_territoire+age_vehicule_rec+presence_alarme+utilisation
                           +duree_permis_rec+sexe+langue+freq_paiement+type_prof,
                           data = data_auto_l_gam, family = Gamma(link = "log"))
summary(fit_gam)


#On va donc ameliorer le modele, en selectionnant pour commencer des variables.
#step(fit_gam,direction = "backward")

#Cette fonction nous conseille de supprimer la variable langue ainsi que le sexe. Effectuons une
#nouvelle glm en supprimant ces des variables

fit_gam2 <- glm(formula = cout_moy~age_rec+duree_permis_rec+age_vehicule_rec+presence_alarme+utilisation
                +type_territoire +freq_paiement+type_prof, data = data_auto_l_gam,
               family = Gamma(link = "log"))



AIC(fit_gam2)
summary(fit_gam2)

c(AIC(fit_gam),AIC(fit_gam2))

# Vérification des modèles

stat2 <- 2*(logLik(fit_gam)-logLik(fit_gam2))
1-pchisq(stat2,df=length(fit_gam$coef)-length(fit_gam2$coef))

anova(fit_gam,fit_gam2,test="LRT")

# Analyse des residus

resg <- residuals(fit_gam2,type="deviance") 
plot(resg^2,ylab="Residuals Deviance",ylim = c(0,20))
abline(h=2, col='red')
length(which(resg^2>2))
#on calcule le pourcentage d'individus qui contribuent au mauvais ajustement du modele:
100*length(which(resg^2>2))/length(res)

# Test et prédiction sur notre modèle avec la base test
pred = predict(fit_gam2,data_auto_t_gam,type="response")
summary(data_auto_t_gam$cout_moy)
summary(pred)
c(m_prediction = mean(pred),m_realite = mean(data_auto_t_gam$cout_moy))

100*sum(data_auto_t_gam$cout_moy)/sum(pred)


# Analyse des erreurs
erreur<-abs(pred-data_auto_t_gam$cout_moy)
summary(erreur)


data_auto_t$cout_moy<-data_auto_t$cout/data_auto_t$nbsin
data_auto_t$cout_moy[is.na(data_auto_t$cout_moy)]<-0
data_auto_t$cout[is.na(data_auto_t$cout)]<-0


###############################################################################################################
#                                         PARTIE 3 : Tarification
###############################################################################################################

# Tarification avec nos 2 GLM avec la formule prime = freq*coutmoy

freq<- predict(fit_pois2,data_auto_t,type="response")
cout_m<-predict(fit_gam2,data_auto_t,type="response")
length(freq)
length(cout_m)
cout_m[is.na(cout_m)]
freq[is.na(freq)]
length(data_auto_t$cout_moy)

#Calcul du Sinistres/Primes (S/P).
sum(data_auto_t$cout)/sum(freq*cout_m)


