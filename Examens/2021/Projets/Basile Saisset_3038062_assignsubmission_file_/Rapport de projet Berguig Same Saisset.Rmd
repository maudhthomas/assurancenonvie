---
title: "Rapport de Projet en Assurance non-vie"
author: "Mathieu Berguig, Omar Same et Basile Saïsset"
date: "30/03/2021"
output: 
  pdf_document: 
    number_sections: yes
    toc: yes
    toc_depth: 3
    fig_width: 6.5
    fig_height: 3
documentclass: article
header-includes:
- \usepackage[francais]{babel}
- \usepackage[margin = 2cm, top = 2cm, bottom=2cm,a4paper]{geometry}
- \usepackage{hyperref}
---
\thispagestyle{empty} 
\newpage
\setcounter{page}{1}

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, fig.align = 'center', warning = FALSE)
```

```{r Chargement des bibliothèques, include = FALSE}
library(dplyr)
library(MASS)
```

```{r Chargement du jeu de données}
rm(list=ls())
database=read.table("dbFull.csv",header=TRUE,sep=",",row.names = 1)
database <- filter(database,database$claim_amount < 5000)
```


\section{Présentation de nos données}

Nous disposons d'un jeu de données avec 100.000 observations représentant le portefeuille d'une compagnie d'assurance. Pour chaque assuré, nous avons ses caractéristiques propres (âge, sexe, âge du permis, bonus-malus), des caractéristiques liées à sa voiture (âge, vitesse max, valeur, ...), des informations sur le contrat souscrit (couverture choisie (mini, intermédiaire, tout risque), fréquence de paiement, etc.) et enfin, la sinistralité sur l'année passée (nombre de sinistres, montant du ou des sinistres).\newline
Variables du jeu de données (nous ne définissons que les variables qui seront utilisées):

  * pol_coverage : la couverture choisie (Mini, Median1, Median2, Maxi)
  * pol_duration : la durée de vie du contrat en année(s)
  * pol_pay_freq : La fréquence des primes
  * pol_usage : L'utilisation référencée du véhicule dans le contrat d'assurance (Retired, WorkPrivate, Professional, AllTrips)
  * drv_age1 : L'âge du conducteur principal du véhicule
  * vh_age : L'âge du véhicule
  * vh_cyl : La cylindrée du véhicule
  * vh_din : La puissance moteur du véhicule (chevaux)
  * vh_fuel : Le "carburant" utilisé (Gasoline, Diesel, Hybride)
  * vh_value : La valeur du véhicule
  * claim_nb : Le nombre de sinistre(s) lors de l'année écoulée
  * claim_amount : Le montant du ou des sinistre(s)


\subsection*{Problématique}

Lorsqu’un client souhaite souscrire à un nouveau contrat d’assurance, l’assureur doit déterminer la prime que l’assuré devra payer en fonction de certaines caractéristiques. Cette prime reflète le coût d’un assuré au cours d’une certaine période, ici 1 an ; elle doit être à la fois suffisamment élevée pour que la
compagnie d’assurance puisse prendre en charge un éventuel sinistre et à la fois suffisamment basse pour que l’assuré accepte de signer le contrat et
qu’il ne parte pas vers une autre compagnie d’assurance.
Dans le cadre de ce projet, nous allons réaliser une tarification a priori dont le but est de trouver un modèle statistique permettant d’estimer cette prime
Nous allons donc réaliser 2 GLM sur 2 variables réponses différentes : le nombre de sinitres et le montant des sinistres.

\subsection{Sinistralité}

```{r Nb de sinistres}
claimnb=database$claim_nb
eff.claimnb=table(claimnb)
#eff.claimnb
prop.claimnb=prop.table(table(claimnb))
#prop.claimnb
barplot(eff.claimnb, col=2:6, main = "Sinistralité")
```

Grâce à nos résultats, on observe que 89% des assurés de la compagnie d'assurance n'ont pas eu de sinistres contre 11% des assurés qui en ont. Ce qui semble être très bon pour la compagnie d'assurance qui aura tendance à verser moins de prestations.
On peut voir aussi qu'environ 10% des assurés ont eu un sinistre, environ 1% ont eu plus d'un sinistre. On peut visualiser cette distribution grâce à notre barplot.

\subsection{Âge des conducteurs}

```{r âge}
age=database$drv_age1
prop.age=prop.table(age)
barplot(table(age),col=2, main = "Répartition de l'âge des conducteurs" )
#hist(prop.age , col=3)
summary(age)
```

L'âge maximal des conducteurs principaux est de 103 ans et l'âge minimal est de 19 ans. La moyenne d'âge des conducteurs est de 54,68 ans.
On a donc une très grande amplitude d'âge des assurés dans notre portefeuille.

\subsection{Couverture}
```{r coverage}
coverage=database$pol_coverage
eff.coverage=table(coverage)
prop.table(eff.coverage)
```
La compagnie d'assurance a quatre formules de couverture à proposer.Nous pouvons voir qu'environ 64,5% des assurés souscrivent à la formule "Maxi ou multirisque", environ 26,9% choisissent plutôt la couverture "Median(1 ou 2)" et une minorité d'environ 8,6% choisissent la formule minimale.

\subsection{Âge du véhicule}

```{r vh_age}
reg=lm(database$claim_nb~database$vh_age)
#plot(database$claim_nb~database$vh_age)
#abline(reg, col="red")
reg
```


Une régression entre l'âge et le nombre de sinistres montre qu'il y a une relation linéaire négative entre l'âge du véhicule et le nombre de sinistres. En effet il semblerait que plus le véhicule est âgé, moins il y a de sinistres. 

\subsection{Classification}
Pour réaliser nos GLM de façon plus efficace, nous devons classifier nos variables quantitatives en classes équilibrées en utilisant la fonction quantile ou le summary quand nous voulons 4 classes. 

Liste des variables à classifier : 

  * pol_duration
  * drv_age1
  * vh_age
  * vh_cyl
  * vh_din
  * vh_speed
  * vh_value.
  
```{r Classification}

database$pol_duration = cut(database$pol_duration, breaks = c(-Inf,4,9,16,+Inf),
                            labels = c("0:4","5;9","10;16","16;41"))

database$drv_age1 = cut(database$drv_age1, breaks = c(-Inf,45,60,Inf),
                        labels = c("18:45","46:60","61:103"))

database$vh_age = cut(database$vh_age, breaks = c(-Inf,5,10,Inf),
                      labels = c("1:5","6:10","11:70"))

database$vh_cyl = cut(database$vh_cyl, breaks = c(-Inf,1000,1500,2000,Inf),
                      labels = c("0:1000","1001:1500","1501:2000","2001:7000"))  

database$vh_din = cut(database$vh_din, breaks = c(-Inf,68,87,109,Inf),
                      labels = c("13:68","69:87","88:109","110:555"))
  
database$vh_speed = cut(database$vh_speed, breaks = c(-Inf,170,Inf),
                        labels = c("25:170","171:300"))

database$vh_value = cut(database$vh_value, breaks = c(-Inf,11950,16230,22106,Inf),
                        labels = c("0:11950","11951:16230","16231:22106","22107:155498"))

```

\section{Modèle \& Prédiction}

\subsection{Création des sous-jeux de données }
```{r Création des sous-jeux de données}
#On crée un sous échantillon de notre jeu avec les claims positifs
d <- filter(database,database$claim_amount > 0)

index = 1:length(database[,1])
ind_database_pred = sample(index,length(database[,1])/2)
ind_database_pred = sort(ind_database_pred)

ind_database_test = setdiff(index, ind_database_pred)

database_pred = database[ind_database_pred,]
database_test = database[ind_database_test,]

d_pred <- filter(database_pred,database_pred$claim_amount > 0)
```

On sous-échantillonne la base de données initiale en 2 bases de tailles égales, après avoir éliminé les valeurs extrêmes (au dessus de 5000). L'une de ces bases servira à modéliser l'influence des variables sur nos variables réponses, et l'autre servira à comparer à notre prédiction.
Il faut aussi créer un jeu de données avec uniquement les sinistres positifs, pour réaliser notre GLM $\Gamma$ sur le montant des sinistres.\newline
\newline
\indent Nous allons d'abord créer des modèles avec toutes nos variables puis utiliser le critère AIC pour retirer des variables.

\subsection{GLM pour le nombre de sinistres}

Le modèle linéaire généralisé Poisson permet de modéliser le nombre de sinistres. Ainsi, nous allons dans un premier temps faire un GLM en utilisant l'ensemble des variables susceptibles d'expliquer notre variable d'intérêt "claim_nb". Par curiosité, nous utiliserons deux GLM pour cette régression afin d'évaluer le plus juste : un GLM Poisson comparé à un GLM Binomiale négative. 

```{r glm_nb poiss, include= FALSE}
glm_nb = glm(claim_nb ~ pol_coverage + pol_duration + pol_pay_freq
             + pol_usage + drv_age1 + vh_age + vh_cyl + vh_din + vh_fuel
             + vh_speed + vh_type + vh_value,
             data = database_pred, family = poisson())

stepAIC(glm_nb)

nb = glm(formula = claim_nb ~ pol_coverage + pol_duration + pol_pay_freq + 
           pol_usage + drv_age1 + vh_age + vh_din + vh_fuel + vh_value, 
         family = poisson(), data = database_pred)


```

```{r affichage nb}
summary(nb)
```


On obtient, ci-dessus un modèle réduit selon le critère de l'AIC. On peut comparer ce modèle de famille Poisson avec le modèle de famille binomiale négative.

```{r comparaison Poiss bin nég}
nb2 = glm.nb(formula = claim_nb ~ pol_coverage + pol_duration 
                   + pol_pay_freq + pol_usage + drv_age1 + vh_age 
                   + vh_din + vh_fuel + vh_value, data = database_pred)

AIC(nb,nb2)
```

Ici, nb est le modèle de Poisson et nb2 est le modèle binomiale négative. On remarque que, le modèle qui minimise le plus l'AIC est le modèle binomiale négative, il semble donc être le plus adapté pour faire notre prédiction. 

```{r affichage nb2}
summary(nb2)
```
De plus, on peut observer sur les sorties R que les déviances confirment notre choix de modèle, 23382 (Binomiale Négative) étant inférieur à 25950 (Poisson).\newline 
On observe que ce modèle est meilleur que le modèle qui ne contient que la constante. Aussi, nous notons, sur la sortie que les premières modalités de chaque variable sont fixées à 0 par contrainte d'identifiabilité. Nous pouvons, à partir des coefficients, estimer le nombre de sinistres d'un assuré selon ses modalités grâce à la fonction de lien canonique $log(\frac{x}{r+x})$. La fonction réciproque de cette fonction est $-\frac{r e^x}{e^x-1}$. On prend le r que l'on trouve dans la sortie ci-dessus, $r = 2.0466$ .
Prenons l'exemple d'un assuré qui aurait les modalités suivantes :


  * Couverture : Median2
  * Durée de contrat : Entre 5 et 9 ans 
  * Paiement : annuel
  * Usage : WorkPrivate
  * Âge : entre 61 et 103 ans 
  * Âge du véhicule : entre 11 et 70 ans 
  * Puissance moteur : entre 88 et 109 
  * Carburant : Essence
  * Valeur du véhicule : entre 16231 et 22106


```{r calcul nb, include= FALSE}
x = -1.37 - 0.18 - 0.01 - 0.06 - 0.64 + 0.06 - 0.44 + 0.19 - 0.16 + 0.13
r = 2.0466
-(r*exp(x))/(exp(x)-1)
```
Un assuré aura donc en moyenne $0.19$ de sinistre par an avec ses modalités.


\subsection{GLM pour le Montant}


De même que précédemment, on va dans cette partie essayer de modéliser le montant du ou des sinistres grâce à un GLM $\Gamma$.

```{r glm_amount, include= FALSE}
glm_amount = glm(claim_amount ~ pol_coverage + pol_duration
                 + pol_pay_freq + pol_usage + drv_age1 + vh_age + vh_cyl 
                 + vh_din + vh_fuel + vh_speed + vh_type + vh_value,
                 data = d_pred, family = Gamma(link = "log"))

stepAIC(glm_amount)
```

On obtient le modèle réduit ci-dessous par la méthode du stepAIC, pour ce GLM $\Gamma$.

```{r amount réduit}
amount = glm(claim_amount ~ pol_coverage + drv_age1 + vh_age + vh_cyl 
             + vh_fuel, data = d_pred, family = Gamma(link = "log"))

summary(amount)
```

On observe, encore, que ce modèle est meilleur que le modèle qui ne contient que la constante. On va, cette fois ci estimer le montant de sinistre pour un assuré selon ses modalités. La fonction de lien utilisée ici, est la fonction log. La fonction réciproque ici utilisée sera donc la fonction exponentielle. \newline
Prenons l'exemple d'un assuré qui aurait les modalités suivantes :
  * Couverture : Median2
  * Âge : entre 61 et 103 ans 
  * Âge du véhicule : entre 11 et 70 ans 
  * Cylindrée moteur : entre 1501 et 2000 
  * Carburant : Essence
  
```{r calcul montant, include= FALSE}
exp(6.84 - 0.29 + 0.11 - 0.18 + 0.04 + 0.09)
```
Un assuré avec ses modalités aura donc en moyenne un(des) sinistre.s pour un montant de 742.48€  

\subsection{Validation des modèles}

Nous allons maintenant procéder à la validation du modèle afin de vérifier sa conformité avec la réalité. Comme vu dans le cours et contrairement au modèle linéaire nous allons nous passer de l'analyse des résidus car ils ne fournissent que peu d'information et n'ont aucune raison d'avoir la même variance. Nous allons plutôt nous intéresser à la déviance pour faire cette validation. \newline
Le théorème de Wilks nous dit que si les hypothèses du modèle GLM sont vérifiées, alors la déviance tend vers une $\chi^2_{n-p}$.

```{r Wilks nb, include= FALSE}
pchisq(nb2$deviance,nb2$df.residual,lower.tail = FALSE)
```
Le résultat de la commande nous donne que notre déviance est inférieure
à une $\chi^2_{n-p}$ avec une probabilité de "1".
Notre déviance converge donc bien vers une $\chi^2_{n-p}$. D'après le théorème de Wilks, notre modèle est donc adapté pour la prédiction.

```{r Wilks amount, include= FALSE}
#Quantile d'une chi2
qchisq(0.95,amount$df.residual)
#Déviance du modèle
amount$deviance

```

On observe ici, d'abord le quantile d'une $\chi^2_{5527}$ au niveau de confiance $95\%$, et sur la 2ème ligne la déviance de notre modèle. 
La déviance étant inférieure, on peut dire qu'elle converge bien vers une $\chi^2_{n-p}$. Et donc, d'après le théorème de Wilks, notre modèle est donc adapté pour la prédiction.

\subsection{Prédiction}

```{r pred}
prediction_nb = predict.glm(nb2, newdata = database_test, type = "response")

prediction_amount = predict.glm(amount, newdata = database_test, 
                                type = "response")
```

On va utiliser la fonction predict.glm pour créer nos vecteurs de prédiction pour les deux variables que nous souhaitons prédire.

```{r graph nb}
hist(prediction_nb, main= "Nombre 'moyen' de sinistre par assuré au cours de l'année", col = c(0,8,0,8,0,8,0,8))
```

Le nombre moyen de sinistre se situe entre 0 et 0.5, cela est dû au fait que le nombre de sinistres était faible dans notre jeu de données (seulement $11\%$ de conducteurs sinistrés). 

```{r sum nb, include= FALSE}
sum(prediction_nb)
sum(database_test$claim_nb)
```

On observe ci-dessus que le nombre de sinistres d'après la prédiction (6130.542) est proche du nombre réel de sinistres dans la deuxième moitié du jeu de données (5986). Notre modèle est cohérent et correspond au devoir de prudence auquel les compagnies d'assurance doivent se tenir.

```{r graph amount}
hist(prediction_amount, main= "Montant 'moyen' de sinistre par assuré au cours de l'année", col = c(0,8,0,8,0,8,0,8))
```

Le montant moyen du ou des sinistres d'un assuré est compris entre 350 et 2420, la moyenne étant de 912. 

```{r sum amount, include= FALSE}
sum(prediction_amount)
sum(database_test$claim_amount)
```

Ici, on observe un bien trop grand écart entre notre prédiction et notre jeu de données. En effet, notre modèle prédit un montant de sinistres à rembourser de 45 513 814, contre seulement 5 236 395 dans les vraies données. L'écart étant de l'ordre de 40M, cela remet largement en cause la qualité de la prédiction du montant des sinistres.


\section*{Conclusion}

\indent En conclusion, la statistique descriptive nous a permis de déterminer le profil de risque du portefeuille. En effet, on observe une très faible sinistralité (12%).\newline
Nous avons ensuite reussi à modéliser le nombre et le montant des sinistres grâce à des modèles GLM. Le modèle de tarification a priori, qu'est le GLM, est un modèle facile d'utilisation même si nous avons dû oter les valeurs extrêmes de notre jeu de données, en l'occurence celles supérieures à 5000. Le critère AIC nous a permis de sélectionner le modèle le plus adéquat pour l'estimation de la variable "claim_nb". Cela ayant été confirmé par l'écart de déviance que nous pouvons observer dans les sorties du logiciel. L'ajustement était de meilleure qualité pour le nombre de réclamations que pour les montants de sinistres. On a pu remarquer cela lorsque nous avons réalisé des prédictions 50/50 sur notre jeu de données. On  arrive à un résultat cohérent et prudent pour la prédiction du nombre de sinistres, contrairement au résultat obtenu lors de la prédiction du montant des sinistres qui ne reflètent pas forcément la réalité. 




