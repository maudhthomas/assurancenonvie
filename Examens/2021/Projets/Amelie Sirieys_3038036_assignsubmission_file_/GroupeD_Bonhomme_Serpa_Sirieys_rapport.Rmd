---
lang : fr #langue française
geometry: margin=2cm
title: "Projet - Econométrie de l'assurance non-vie"
author: "Groupe D - Alexandra Bonhomme, Deadona Serpa et Amélie Sirieys - Avril 2021"
output: pdf_document
---


```{r setup, include=FALSE}
# paramètres, packages, librairie
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, error = FALSE)

# paramètres, packages, librairie
#install.packages("yarrr")
#install.packages("GGally")
#install.packages("stargazer")
#install.packages("xtable")
#install.packages("lmtest")
#install.packages("dplyr")
#install.packages("kableExtra")
#install.packages("png")
#install.packages("grid")
#install.packages("rgrs", repos="http://R-Forge.R-project.org")


library(car)
library(yarrr)
library(ggplot2)
library(GGally)
library(stargazer)
library(knitr)
library(xtable)
library(lmtest)
library(kableExtra)
library(dplyr)
library(png)
library(grid)
library(rgrs)
```



```{r}
# website : https://raw.githubusercontent.com/stedy/Machine-Learning-with-R-datasets/master/insurance.csv

# télécharger les données
# packages base et utils utilisés, généralement déjà implémentés
url = "https://raw.githubusercontent.com/stedy/Machine-Learning-with-R-datasets/master/insurance.csv"
temp = tempfile(fileext = ".csv")
download.file(url, destfile=temp, mode='wb')
data_init = read.csv(temp)
data_init = as.data.frame(data_init)
# base quantitative uniquement
data_quantitative = data_init[, c("age", "bmi", "charges")]
```

\setlength{\parindent}{0.5cm}
\tableofcontents
\setcounter{page}{0}

\newpage
\setcounter{page}{1}


 

# 1. Introduction

## 1.1 Introduction de nos données

Notre base de données est extraite du site Kaggle et est disponible également sur GitHub. Elle permet la prédiction des dépenses de santé annuelles pour des individus en fonction de leur âge, leur sexe, leur indice de masse corporelle, leur nombre d’enfants, leur statut de fumeur ou non et leur localisation géographique. Elle comporte ainsi 1338 observations (individus) et 7 variables. Dans cette étude nous avons travaillé avec les noms des variables en anglais, nous précisons ainsi : 

\begin{itemize}
\item charges : dépenses de santé
\item age : âge
\item sex : sexe
\item bmi : IMC, indice de masse corporelle
\item children : nombre d’enfants
\item smoker : fumeur, statut de fumeur
\item region : région, localisation géographique 
\end{itemize}




## 1.2 Problématique

Notre base de données nous permet la prédiction des dépenses de santé annuelles pour un individu selon ses caractéristiques. Ainsi, notre projet consiste à réaliser un modèle de tarification a priori de la prime pure annuelle d’une assurance santé individuelle. En effet, grâce à ces données empiriques des dépenses de santé individuelles, une compagnie d’assurance peut estimer le coût en santé d’un assuré selon ses caractéristiques, afin d’en déduire sa prime pure. Ce type de modélisation est réalisé à l’aide des modèles linéaires généralisés qui permettent une forme plus générale que celle imposée par la réalisation d’un modèle linéaire. 

Dans un premier temps, nous allons réaliser une analyse synthétique de nos variables, dont certaines seront transformées afin de modéliser les dépenses de santé individuelles à l’aide de variables qualitatives uniquement. Dans un second temps, nous réaliserons une première modélisation des dépenses de santé individuelles annuelles à l’aide d’une loi Gamma et de la fonction de lien canonique. Puis, nous modéliserons les dépenses de santé individuelles annuelles également avec la loi Gamma mais avec la fonction de lien Identité. Après avoir sélectionné le modèle le plus adéquat parmi les 2 précédemment cités, nous réaliserons une première prédiction. Enfin, au vu des résultats obtenus avec cette prédiction, nous préciserons le modèle en ajoutant une variable d'interaction et nous conclurons cette étude. 

## 1.3 Notations

Afin de faciliter la lecture de ce rapport, nous résumons dans cette sous-partie les notations utilisées. 
\begin{itemize}
\item $i$ : représentation de l’individu
\item $n$ : nombre total d’individus
\item $p$ : nombre total de variables caractéristiques
\item $Y$ = $(Y_1, …, Y_n)\prime$ : vecteur colonne des dépenses en santé
\item $X$ : matrice des variables caractéristiques de taille $n*p$ où les lignes nommées $X_i \prime$ ( = $(X_{i,1}, … , X_{i,p})$ ) correspondent aux individus
\item $\beta$ = $(\beta_1, …, \beta_p)\prime$ : vecteur colonne des paramètres du modèle
\end{itemize}


# 2. Analyse et modification de nos données


## 2.1 Analyse synthétique de nos données

Nous nous intéressons dans un premier temps à la distribution de nos données selon chacune des variables. Nous obtenons les tableaux suivants respectivement pour les variables quantitatives et les variables qualitatives.


```{r results = "asis"}
# description de chacune des variables quantitative
kable(summary(data_quantitative), caption = "Description des variables quantitatives") %>% kable_styling(latex_options = "hold_position")
```

```{r}
# description de chacune des variables qualitatives
children = as.data.frame(table(data_init$children))
names(children) = c("children", "Fréquence")
sex = as.data.frame(table(data_init$sex))
names(sex) = c("sex", "Fréquence")
smoker = as.data.frame(table(data_init$smoker))
names(smoker) = c("smoker", "Fréquence")
region = as.data.frame(table(data_init$region))
names(region) = c("region", "Fréquence")
prop.non.fumeur = round(table(data_init$smoker)[1]/length(data_init$smoker)*100,1)
prop.fumeur = round(table(data_init$smoker)[2]/length(data_init$smoker)*100,1)
```

```{r results="asis"}
kable(list(children, sex, smoker, region), align='c', caption = "Description des variables qualitatives") %>% kable_styling(latex_options = "hold_position")
```
\ 
A la lecture de ces tableaux, nous pouvons dire de notre population qu'elle se concentre entre 18 et 64 ans. De plus, 50% de nos individus ont un IMC supérieur à 30.40 ce qui signifie qu'ils sont en situation d'obésité (voir Figure en Annexe). Par ailleurs, nous avons approximativement une répartition égale de nos observations selon le sexe et selon la région puisque leurs modalités respectives ont des fréquences proches. En revanche, nous avons une grande population de non-fumeurs dans notre base de données puisqu'il s'agit de `r prop.non.fumeur`% de nos individus.

## 2.2 Transformation de certaines de nos variables

Nous souhaitons désormais concevoir des classes pour les variables quantitatives `bmi` et `age`, afin de considérer ces variables comme qualitatives pour la suite de notre étude. En effet, pour réaliser un modèle de tarification a priori, il est plus pertinent d’utiliser des variables qualitatives afin de pouvoir tarifer par groupe d’assurés. Ainsi, les 5 classes pour `bmi` ont été déterminées selon l’échelle conventionnelle (voir Figure dans l'Annexe) : sous poids, poids normal, surpoids, obésité, obésité extrême et obésité morbide. Quant à la variable `age`, nous avons choisi de les classifier en 5 catégories selon ses quartiles. Nous distinguons ainsi la population entre 18 et 26 ans, entre 27 et 38 ans, entre 39 et 49 ans, et celle âgée de 50 ou plus.

```{r}
# on transforme les variables quantitatives en variables qualitatives
data = data_init
data = renomme.variable(data, "age", "age_class")
data = renomme.variable(data, "bmi", "bmi_class")

vect_quantile_age = quantile(data$age_class,seq(0,1,0.25))


data$bmi_class = cut(data$bmi_class, breaks=c(-Inf, 18.5, 24.9, 29.9, 34.9,39.9, +Inf), labels=c("underweight", "normal", "overweigth", "obese", "extremely obese", "morbid obesity"))
data$age_class = cut(data$age_class, breaks=c(-Inf, 26, 38, 49, +Inf), labels=c("[18-27[", "[27-39[", "[39-50[", "[50+"))

```

```{r}
# description de chacune des variables qualitatives
age = as.data.frame(table(data$age_class))
names(age) = c("age", "Fréquence")
bmi = as.data.frame(table(data$bmi_class))
names(bmi) = c("bmi", "Fréquence")
```

```{r results="asis"}
kable(list(age, bmi), align='c', caption = "Description des nouvelles variables qualitatives") %>% kable_styling(latex_options = "hold_position")
```
\ 
Le tableau ci-dessus permet de rappeler la distribution de chacune de ces variables transformées selon leurs modalités respectives. Nous observons ainsi, comme lors de notre analyse de ces variables sous leur forme qualitative, que notre population est majoritairement en situation de surpoids.

# 3. GLM  

## 3.1 GLM : loi Gamma et fonction de lien canonique

### 3.1.1 Modélisation

Notre premier modèle linéaire généralisé va chercher à prédire les dépenses en santé via les variables caractéristiques renseignées. Nous allons pour cela utiliser la loi Gamma car la variable à prédire des dépenses est continue et positive et qu’elle figure comme la loi fréquemment utilisée pour modéliser les montants de sinistres. Nous avons choisi d’utiliser la fonction de lien inverse qui est la fonction de lien canonique associée. Nous avons ainsi : 

\begin{itemize}
\item $Y_i|X_i = x_i$ qui suit une loi Gamma
\item $1/(\mu(X_i)) = \frac{1}{E[Y_i|X_i]} = X_i \prime\beta$
\end{itemize}

Nous tenons à préciser ici, et pour la suite du projet, que nos données sont déjà normalisées puisque $Y_i$ correspond au montant de sinistres par personne. 

Dans un premier temps, nous réalisons grâce à la fonction `glm` de `R` le modèle avec l’ensemble des variables renseignées soit : 
\begin{itemize}
\item $Y_i$ : dépenses en santé pour $i$
\item $X_i$ = $(age\_class_i, bmi\_class_i, region_i, smoker_i, sex_i, children_i)$
\end{itemize}

```{r}
model_gamma_inverse = glm(data = data, formula = charges ~ age_class + bmi_class + region + smoker + sex + as.factor(children), family = Gamma(link = "inverse"))
summary(model_gamma_inverse)
```
\ 
Notre modèle se constitue de variables qualitatives et se définit ainsi par un individu de référence. Nous pouvons déduire de la sortie `R` ci-dessus que l’individu de référence est une femme non-fumeuse qui appartient à la classe d’âge 18-26 ans, est en situation de maigreur, de la région du nord-est, et n’a pas d’enfant. Cet individu référent par défaut sera celui sélectionné systématiquement dans la suite de notre étude.

Nous pouvons voir à la suite de ce `glm` que l’âge semble avoir un impact sur les dépenses totales. En effet, toutes les classes d’âge ont une p-value < 0.05, traduisant donc qu’elles sont toutes significatives par rapport à la classe référente des 18-26 ans. 

Par ailleurs, nous pouvons observer qu’un poids très élevé semble avoir une influence significative dans les charges, par rapport à un poids maigre, puisque les différents types d'obésité ont des p-value plus faibles que 0.05. D’autre part, être en situation de poids normal ou surpoids ne semble pas avoir un réel impact sur les dépenses par rapport à la maigreur d’après la sortie `R`.

Quant aux modalités de `children`, seul le statut de 4 enfants a un impact notoire sur les charges par rapport à la classe référente.

De plus, le fait d’être fumeur semble avoir un impact fort sur les dépenses en santé par rapport aux dépenses des non-fumeurs.

Finalement, les régions et le sexe ne semblent pas avoir de modalités qui ont un impact significatif. 

Cependant, nous rappelons que la lecture de cette sortie ne nous permet pas de sélectionner des variables et d'optimiser notre modèle. En effet, les tests de Student effectués ici permettent de comprendre pour chaque variable catégorique s’il existe une différence significative entre la modalité de référence de la variable sélectionnée et celle donnée dans le `summary`, et non la significativité de la variable dans son ensemble. Nous souhaitons entamer une procédure de sélection de variables.


### 3.1.2 Sélection de variables

Pour sélectionner nos variables afin d’optimiser notre modèle, nous allons ainsi utiliser la méthode d’analyse de la déviance et celle de l’analyse de l’AIC dans la suite de cette étude.

**Par analyse de la déviance**

```{r}
A = anova(model_gamma_inverse, test = "Chisq")
```

```{r results = "asis"}
# description de chacune des variables quantitative
kable(A, caption = "Table de la déviance") %>% kable_styling(latex_options = "hold_position")
```
\

Premièrement, à l’aide de la fonction `anova`, nous étudions la table de la déviance, qui permet de considérer à la première ligne un modèle comprenant uniquement la constante, puis à chaque ligne d’ajouter une nouvelle variable dans le modèle. Nous constatons, sur la table ci-dessus, que le modèle comprenant la constante et les variables `age`, `bmi`, `region` et `smoker` semblent significativement expliquer la variable réponse `charges`.

Néanmoins, le tableau ne permet pas de vérifier directement la significativité de chaque variable. En effet, selon l’ordre d’ajout des variables dans le modèle, certaines peuvent être considérées comme significatives en étant combinées à d’autres. De ce fait, nous vérifions la significativité individuelle de chaque variable à l’aide de la fonction `Anova` du package `car`.


```{r}
Abis = Anova(model_gamma_inverse, test.statistic = "LR", type = 3)
```

```{r results = "asis"}
# description de chacune des variables quantitative
kable(Abis , caption = "Etude de la significativité des variables") %>% kable_styling(latex_options = "hold_position")
```
\
Le test du rapport de vraisemblance `LR` permet ainsi de maintenir `age`, `bmi` et `smoker` dans le modèle optimisé. Les variables `region`, `children` et `sex` s'avèrent non significatives individuellement, avec leur p-value > 0.05. 


**Par analyse de l'AIC**


Dans un second temps, nous pouvons confirmer cette sélection de variables grâce à l’analyse de l’AIC, en vérifiant que le modèle incluant la constante, `age`, `bmi` et `smoker` présente bien l’AIC le plus faible. Pour ce faire, la fonction `step` direction `backward` permet de partir d’un modèle comprenant toutes les variables et d’enlever à chaque étape une variable afin d’obtenir le modèle minimisant l’AIC à la fin de la sortie. Nous obtenons les résultats suivants. 

```{r}
step(model_gamma_inverse, direction = "backward")
```


Nous constatons que les résultats concordent avec l’analyse de la déviance. Cela permet également d’affirmer que le modèle minimisant l’AIC est celui comprenant la constante, `age`, `bmi` et `smoker`. 

### 3.1.3 Modèle optimisé

Nous construisons alors le modèle optimisé selon notre étape des sélections de variables. Le modèle est ainsi défini : 
\begin{itemize}
\item $Y_i|X_i = x_i$ qui suit une loi Gamma
\item $1/(\mu(X_i)) = \frac{1}{E[Y_i|X_i]} = X_i \prime\beta$
\end{itemize}
Avec : 
\begin{itemize}
\item $Y_i$ : dépenses en santé pour $i$
\item $X_i$ = $(age\_class_i, bmi\_class_i, smoker_i)$
\end{itemize}


```{r}
model_gamma_inverse_opti = glm(data = data, formula = charges ~ age_class + bmi_class + smoker, family = Gamma(link = "inverse"))
summary(model_gamma_inverse_opti)
```

Ce modèle présente bien un AIC inférieur à celui calculé avec l’ensemble des variables explicatives. Toutefois, nous pensons que celui-ci n’est pas optimal puisqu'il ne contient seulement 3 des 6 variables explicatives. Il nous manque donc des informations qui pourraient impacter les dépenses en santé d'un individu. Par conséquent, nous allons construire un nouveau modèle.

## 3.2 GLM : loi Gamma et fonction de lien Identité


### 3.2.1 Modélisation

Dans cette partie, nous cherchons toujours à prédire les dépenses de santé en utilisant la loi Gamma, avec désormais la fonction de lien identité. Nous avons alors : 
\begin{itemize}
\item $Y_i|X_i = x_i$ qui suit une loi Gamma
\item $\mu(X_i) = E[Y_i|X_i] = X_i^{\prime}\beta$
\end{itemize}
Avec, dans un premier temps, l’ensemble des variables explicatives soit : 
\begin{itemize}
\item $Y_i$ : dépenses en santé pour $i$
\item $X_i$ = $(age\_class_i, bmi\_class_i, region_i, smoker_i, sex_i, children_i)$
\end{itemize}


```{r}
model_gamma_identite = glm(data = data, formula = charges ~ age_class + bmi_class + region + smoker + sex + as.factor(children), family = Gamma(link = "identity"))
summary(model_gamma_identite)
```

Nous pouvons voir à la suite du `summary` qu’approximativement toutes les modalités semblent significatives. En effet, les classes d’âge, le statut de fumeur, le sexe ainsi que le nombre d’enfants sont des facteurs qui semblent influents par rapport à la modalité de référence au seuil 0.05.

Quant au bmi, nous observons que presque toutes les classes sauf celle de l’obésité morbide, ont un impact significatif sur les dépenses en santé en comparaison avec celle de la maigreur.

Enfin, le nombre d'enfants semble significatif et toutes les classes d’enfants sont acceptées comme facteurs influents au seuil 0.05 par rapport à la classe référente sans enfant.

Comme indiqué précédemment, cette rapide analyse du `summary` n’est pas une méthode efficiente pour sélectionner les variables dans notre modèle. Par conséquent, nous effectuons désormais l’étape de sélection des variables à l’aide l’analyse de la déviance et de l’AIC. 

### 3.2.2 Sélection de variables

**Par analyse de la déviance**



Nous vérifions la significativité individuelle de chaque variable à l’aide de la fonction `Anova` du package `car`. 

```{r}
Atier = Anova(model_gamma_identite, test.statistic = "LR", type = 3)
```

```{r results = "asis"}
# description de chacune des variables quantitative
kable(Atier , caption = "Etude de la significativité des variables") %>% kable_styling(latex_options = "hold_position")
```
\

Le test de rapport de vraisemblance `LR` nous permet de déduire qu' hormis `bmi`, toutes les variables semblent significatives au seuil 0.05. Nous pouvons donc construire un modèle dans lequel uniquement `bmi` ne serait pas conservé.


**Par analyse de l'AIC**


Nous étudions maintenant l’AIC afin de confirmer ou infirmer nos hypothèses de sélection de variables. 

```{r}
step(model_gamma_identite, direction = "backward")
```

La sortie de la fonction `step` nous indique que le modèle optimisé avec l’AIC le plus faible est celui comprenant toutes les variables sauf `bmi`, comme nous l’obtenons dans l’analyse de la déviance. Nous ne conservons donc pas `bmi` dans le modèle optimisé. 

### 3.2.3 Modèle optimisé

Nous construisons alors le modèle optimisé suivant : 
\begin{itemize}
\item $Y_i|X_i = x_i$ qui suit une loi Gamma
\item $\mu(X_i) = E[Y_i|X_i] = X_i^{\prime}\beta$
\end{itemize}
Avec : 
\begin{itemize}
\item $Y_i$ : dépenses en santé pour $i$
\item $X_i$ = $(age\_class_i, region_i, smoker_i, sex_i, children_i)$
\end{itemize}


```{r}
model_gamma_identite_opti = glm(data = data, formula = charges ~ age_class + region + smoker + sex + as.factor(children), family = Gamma(link = "identity"))
summary(model_gamma_identite_opti)
```

Ce modèle permet ainsi d’obtenir un AIC de valeur minimum avec quasiment l’ensemble des variables caractéristiques d’un individu dont nous disposons. 

## 3.3 Choix du modèle final

Finalement, nous choisissons le modèle optimisé Gamma avec la fonction de lien identité comme modèle final. En effet, celui-ci présente un AIC de `26271` face à  `26867` pour le modèle optimisé Gamma de fonction de lien canonique. De plus, ce modèle contenant le plus de variables et utilisant donc plus de données, il permettra une meilleure prédiction par la suite. Notre modèle est ainsi décrit : 
\begin{itemize}
\item $Y_i|X_i = x_i$ qui suit une loi Gamma
\item $\mu(X_i) = E[Y_i|X_i] = X_i^{\prime}\beta$
\end{itemize}
Avec : 
\begin{itemize}
\item $Y_i$ : dépenses en santé pour $i$
\item $X_i$ = $(age\_class_i, region_i, smoker_i, sex_i, children_i)$
\end{itemize}


## 3.4 Prédiction à l'aide du modèle final choisi 

Pour vérifier la qualité de prédiction du modèle finalement choisi, nous effectuons une prédiction sur nos données de la table `test` grâce à notre modèle entraîné sur la table `train`, respectivement 20% et 80% de notre base de données tirés aléatoirement. Nous obtenons le graphique ci-dessous, où la droite est celle de la fonction $x=y$. 

```{r}
set.seed(356)
# nous tirons aléatoirement notre échantillon d'entraînement de 80%
obs = length(data$sex)
liste.alea.80 = sample(1:obs, 0.8*obs, replace = FALSE)
data_train = data[liste.alea.80, ]
# les 20% restants sont l'échantillon de test
data_test = data[-liste.alea.80, ]
# on entraîne notre modèle
model_final_sur_train = glm(data = data_train, formula = charges ~ age_class + region + smoker + sex + as.factor(children), family = Gamma(link = "identity"))
# avec le model entraîné on prédit sur la base de test
pred = predict(model_final_sur_train, newdata = data_test)

```

```{r, message=FALSE}
residus_empiriques = c(1:length(pred))
points.originaux = c(1:length(pred))
smoker_status = c(1:length(pred))
bmi_class = c(1:length(pred))
 
for (i in 1:length(pred)){
  index = row.names(as.data.frame(pred[i]))
  residus_empiriques[i] = pred[index] - data[index, c("charges")]
  points.originaux[i] = data[index, c("charges")]
  smoker_status[i] = data[index, c("smoker")]
  bmi_class[i] = data[index, c("bmi_class")]
}

data_prediction = as.data.frame(cbind(pred, points.originaux, smoker_status))
data_prediction$pred = as.numeric(data_prediction$pred)
data_prediction$points.originaux = as.numeric(data_prediction$points.originaux)

pred.graph = ggplot(data = data_prediction, aes(x=pred, y = points.originaux),cex.lab = 0.8, cex.axis = 0.6) + geom_point(aes(colour=factor(smoker_status), shape=factor(bmi_class)))+ geom_abline(slope=1, intercept = 0) + xlab("charges prédites") + ylab("charges") + theme_bw() + ggtitle("Graphique des prédictions")
```

```{r, fig.cap= "Graphique des prédictions", message=FALSE}
pred.graph
```
\

Deux choses se détachent particulièrement sur ce graphique :
\begin{itemize}
\item Premièrement, les données relatives aux fumeurs se situent à gauche et celles relatives aux non-fumeurs à droite de ce graphique. De ce fait, le modèle a tendance à considérer les fumeurs comme ayant significativement plus de dépenses de santé que les non-fumeurs : le statut fumeur est donc très influent dans notre modèle.
\item Deuxièmement, nous pouvons remarquer que les valeurs relatives aux fumeurs semblent moins bien expliquées par le modèle que celles relatives aux non-fumeurs. En effet, les points correspondants aux fumeurs en situation d’obésité, d'extrême obésité et d’obésité morbide ont en réalité des dépenses sous-évaluées par le modèle. A contrario, les fumeurs maigres, normaux et en surpoids ont des dépenses sur-évaluées par le modèle. Cette baisse de qualité de prédiction pourrait s’expliquer par le fait que l’association obésité-tabagisme a indéniablement des conséquences néfastes sur la santé, ajoutant ainsi une interaction entre le statut de fumeur et l’indice de masse corporelle. 
\end{itemize}


# 4. Amélioration du modèle : ajout d'une variable d'interaction

Afin de corriger la mauvaise prédiction du modèle par rapport aux fumeurs, nous décidons d’ajouter une variable d’interaction. Nous réintégrons donc la variable `bmi` dans notre modèle et ajoutons le terme d'interaction qui semble exister entre l’indice de masse corporelle et le statut de fumeur ou non d’après la prédiction précédente. 


## 4.1 Etude de l'interaction entre l'indice de masse corporelle et le statut de fumeur ou non de l'individu

Au vu des résultats de la prédiction, nous pensons qu’il existe une interaction entre le statut de fumeur et l’indice de masse corporelle. Le graphique ci-dessous représente la distribution des dépenses en santé selon les variables bmi et smoker. 

```{r fig.cap= "Boîte à moustache des dépenses en santé selon le statut de fumeur et l'I.M.C"}
# intéraction entre fumeur et bmi
boxplot(charges ~ bmi_class * smoker, data = data)
title("Boîte à moustache des dépenses en santé \n selon le statut de fumeur et l'I.M.C")
```
\

Nous pouvons observer que le fait qu’un individu soit fumeur augmente nettement ses dépenses en santé et également qu’un indice de masse corporelle élevé les accroît d’autant plus. En effet, sur la partie droite du graphique, les distributions des dépenses des individus en situation de surpoids et fumeurs sont significativement supérieures à celles des individus en situation de surpoids mais non-fumeurs. Par conséquent, nous décidons d’intégrer la variable d’interaction entre l’indice de masse corporelle et le statut de fumeur dans notre modèle. 


## 4.2 GLM : loi Gamma avec fonction de lien Identité et variable d'interaction

### 4.2.1 Modélisation

Nous reprenons les caractéristiques du modèle précédent utilisé dans la prédiction, c’est-à-dire le modèle linéaire généralisé avec la loi Gamma et la fonction de lien Identité. En ajoutant le terme d’interaction dans le modèle, nous décidons d’effectuer à nouveau une sélection de variables. Pour cela, nous modélisons dans un premier temps le modèle avec l’ensemble des variables explicatives et le terme d’interaction. Nous avons ainsi : 
\begin{itemize}
\item $Y_i|X_i = x_i$ qui suit une loi Gamma
\item $1/(\mu(X_i)) = E[Y_i|X_i] = X_i \prime\beta$
\end{itemize}
Avec :
\begin{itemize}
\item $Y_i$ : dépenses en santé pour $i$
\item $X_i$ = $(age\_class_i, bmi\_class_i, region_i, smoker_i, sex_i, children_i, bmi\_class_i*smoker_i)$
\end{itemize}


```{r}
# model avec intéraction
model_gamma_identite_interaction = glm(data = data, formula = charges ~ age_class + bmi_class + region + smoker + sex + as.factor(children) + smoker*bmi_class, family = Gamma(link = "identity"))
summary(model_gamma_identite_interaction)
```

Dans la sortie `R` ci-dessus, nous observons qu’une majorité des modalités semblent significatives par rapport aux modalités de référence. Toutefois, comme nous l’avons expliqué au début de notre étude, cette lecture de résumé ne suffit pas à établir un modèle optimal. Par conséquent, nous réalisons une étape de sélection des variables grâce à l’analyse de la déviance et l’analyse de l’AIC pour étudier la significativité globale de chacune d’entre elles.


### 4.2.2 Sélection des variables

**Par analyse de la déviance**

Nous vérifions la significativité de chaque variable à l’aide de la fonction `Anova` du package `car`.


```{r}
# analyse déviance
Aquart = Anova(model_gamma_identite_interaction, test.statistic = "LR", type = 3 )
```

```{r results = "asis"}
# description de chacune des variables quantitative
kable(Aquart , caption = "Etude de la significativité des variables") %>% kable_styling(latex_options = "hold_position")
```
\
Le test de rapport de vraisemblance `LR` nous permet de déduire qu' à part la variable `bmi`, tous les facteurs semblent avoir un impact significatif au seuil de 0.05 sur les dépenses de santé. Nous pouvons donc nous demander s'il serait pertinent d’enlever `bmi` de notre modélisation avec cette analyse.


**Par analyse de l'AIC**

Nous étudions maintenant la significativité des variables grâce au calcul de l’AIC afin de confirmer ou infirmer nos hypothèses de sélection de variables. 


```{r}
# analyse AIC
step(model_gamma_identite_interaction, direction = "backward")
```

La sortie de la fonction `step` nous indique que le modèle optimisé avec l’AIC le plus faible est celui comprenant toutes les variables. Nous décidons donc de toutes les conserver, ce qui valide également notre hypothèse d’interaction entre `bmi` et `smoker`. 

### 4.2.3 Modèle optimisé

Le modèle final choisi est alors défini comme suit : 
\begin{itemize}
\item $Y_i|X_i = x_i$ qui suit une loi Gamma
\item $1/(\mu(X_i)) = E[Y_i|X_i] = X_i \prime\beta$
\end{itemize}
Avec :
\begin{itemize}
\item $Y_i$ : dépenses en santé pour $i$
\item $X_i$ = $(age\_class_i, bmi\_class_i, region_i, smoker_i, sex_i, children_i, bmi\_class_i*smoker_i)$
\end{itemize}


## 4.3 Prédiction

Pour vérifier la qualité de prédiction du modèle, nous effectuons une prédiction sur nos données de la table `test` grâce à notre modèle entraîné sur la table `train`, respectivement 20% et 80% de notre base de données tirés aléatoirement. Enfin, afin de pouvoir juger facilement de la qualité de notre prédiction, nous réalisons le graphique suivant. 


```{r}
set.seed(356)
# nous tirons aléatoirement notre échantillon d'entraînement de 80%
obs = length(data$sex)
liste.alea.80 = sample(1:obs, 0.8*obs, replace = FALSE)
data_train = data[liste.alea.80, ]
# les 20% restants sont l'échantillon de test
data_test = data[-liste.alea.80, ]
# on entraîne notre modèle
model_final_sur_train = glm(data = data_train, formula = charges ~ age_class + bmi_class + region + smoker + sex + as.factor(children) + smoker*bmi_class, family = Gamma(link = "identity"))
# avec le model entraîné on prédit sur la base de test
pred = predict(model_final_sur_train, newdata = data_test)

```


```{r}
residus_empiriques = c(1:length(pred))
points.originaux = c(1:length(pred))
smoker_status = c(1:length(pred))
bmi_class = c(1:length(pred))
 
for (i in 1:length(pred)){
  index = row.names(as.data.frame(pred[i]))
  residus_empiriques[i] = pred[index] - data[index, c("charges")]
  points.originaux[i] = data[index, c("charges")]
  smoker_status[i] = data[index, c("smoker")]
  bmi_class[i] = data[index, c("bmi_class")]
}

data_prediction = as.data.frame(cbind(pred, points.originaux, smoker_status))
data_prediction$pred = as.numeric(data_prediction$pred)
data_prediction$points.originaux = as.numeric(data_prediction$points.originaux)

pred.graph = ggplot(data = data_prediction, aes(x=pred, y = points.originaux),cex.lab = 0.8, cex.axis = 0.6) + geom_point(aes(colour=factor(smoker_status), shape=factor(bmi_class)))+ geom_abline(slope=1, intercept = 0) + xlab("charges prédites") + ylab("charges") + theme_bw() + ggtitle("Graphique des prédictions")

```


```{r fig.cap= "Graphique des prédictions"}
pred.graph
```
\

Nous observons immédiatement que notre prédiction est plus juste que celle réalisée précédemment. En effet, la plupart des points se trouvent sur la droite $x=y$, ce qui implique que nos prédictions correspondent approximativement aux valeurs empiriques. Par conséquent, pour établir la prime pure d’assurance santé individuelle, il est judicieux de considérer ce dernier modèle. 



# 5. Conclusion

Notre étude aboutit finalement à l’établissement d’un GLM Gamma de fonction de lien Identité, qui tient compte de l’âge, du sexe, de l’IMC, du nombre d’enfants, du statut de fumeur, de la localisation géographique et de la variable d'interaction entre l’IMC et le statut de fumeur. Ce modèle satisfaisant nous permet ainsi d’estimer la prime pure annuelle des individus selon leurs caractéristiques pour un contrat de santé individuel.

 



\newpage

# Annexe  

```{r fig.cap="Signification des indices de masses corporelles"}
image = "https://medias.santepratique.fr/wp-content/uploads/Infographie-IMC-1-960x960.jpg"
temp.image = tempfile(fileext = ".jpg")
download.file(image, destfile=temp.image, mode='wb')
image_imc = readJPEG(temp.image, native = FALSE)
grid.raster(image_imc)
```


