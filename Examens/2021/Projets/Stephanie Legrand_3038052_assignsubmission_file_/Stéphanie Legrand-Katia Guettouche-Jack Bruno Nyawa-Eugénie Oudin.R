rm(list = ls())
library(ggplot2)
library(car)

#Importation des donn�es
data_csv = read.csv("HouseToRent.csv", sep = ",")

#Visualisation des donn�es
View(data_csv)
head(data_csv)


#Formatage des donn�es
dt <- data.frame(area = data_csv[,1],rooms = data_csv[,2],bathroom = data_csv[,3],
                 parking = data_csv[,4],animal = data_csv[,5],furniture = data_csv[,6],
                 hoa = data_csv[,7],rent_amount = data_csv[,8],property_tax = data_csv[,9],
                 fire_insurance = data_csv[,10]) 
#On ne retient pas la variable total car elle r�sume les autres variables

###########Pr�sentation des donn�es########################
summary(data_csv)


#Construction de l'histogramme avec la densit� et la moyenne pour la variable fire insurance
fire <- data.frame(fire_insurance = data_csv[,10])
h <-  ggplot(fire, aes(x=fire_insurance)) + 
  geom_histogram(aes(y=..density..), colour="black", fill="white")+
  geom_density(alpha=.2, fill="#FF6666") + ggtitle("Histogramme de la variable 'fire insurance'")+
  theme(plot.title = element_text(hjust = 0.5))
h + geom_vline(aes(xintercept=mean(fire_insurance)),
               color="blue", linetype="dashed", size=1) + labs(title="Histogramme de la variable 'fire insurance'",x="fire insurance ($)", y = "Count")


summary(dt)

# D'apr�s les valeurs visualis�es dans le summary, nous avons d�cid� de couper nos vecteurs apr�s le 
# 3�me quartile car apr�s les valeurs sont extr�mes (tr�s �loign�es de leur moyenne)


#Coupage
dt2<-subset(dt,dt$property_tax<=quantile(dt$property_tax,0.75) & dt$rent_amount<= quantile(dt$rent_amount,0.75) 
            & dt$hoa<=quantile(dt$hoa,0.75) & dt$area<=quantile(dt$area,0.75) & dt$fire_insurance<=quantile(dt$fire_insurance,0.75))

View(dt2)
summary(dt2) # nous observons que les donn�es sont mieux regroup�es autour de leur moyenne

#Construction de l'histogramme avec la densit� et la moyenne pour la nouvelle variable fire insurance
fire <- data.frame(fire_insurance = dt2$fire_insurance)
h <-  ggplot(fire, aes(x=dt2$fire_insurance)) + 
  geom_histogram(aes(y=..density..), colour="black", fill="white")+
  geom_density(alpha=.2, fill="#FF6666") + ggtitle("Histogramme de la nouvelle variable 'fire insurance'")+
  theme(plot.title = element_text(hjust = 0.5))
h + geom_vline(aes(xintercept=mean(dt2$fire_insurance)),
               color="blue", linetype="dashed", size=1) + labs(title="Histogramme de la nouvelle variable 'fire insurance'",x="fire insurance ($)", y = "Count")

################Formatage des donn�es en classes#################
dt2$rooms <- cut(x = dt2$rooms,breaks = seq(from=0,to = 8,by= 2)) 
# nous avons choisi de regrouper les valeurs 2 par 2 pour �viter d'avoir un trop grand nombre de classes
dt2$bathroom <-cut(x = dt2$bathroom,breaks = seq(from=0,to = 6,by= 2))
dt2$parking <-cut(x = dt2$parking,breaks = seq(from=0,to = 6,by= 2))
dt2$animal<- as.factor(dt2$animal)
dt2$furniture <- as.factor(dt2$furniture)

dt2$area <- cut(x = dt2$area,breaks = seq(from=0,to = max(dt2$area)+25,by= 25))
dt2$hoa <- cut(x = dt2$hoa,breaks = seq(from=0,to = max(dt2$hoa)+200,by= 200))
dt2$rent_amount <- cut(x = dt2$rent_amount,breaks = seq(from=0,to = max(dt2$rent_amount)+500,by= 500))
dt2$property_tax <- cut(x = dt2$property_tax,breaks = seq(from=0,to = max(dt2$property_tax)+50,by= 50))


#######################GLM################################

#GLM Gamma avec fonction de lien logarithme
fit <- glm(formula = fire_insurance ~ area + rooms + bathroom + parking + animal + furniture + hoa +
             rent_amount + property_tax, data = dt2, family = Gamma(link = "log"))

summary(fit)
anova(fit, test = "Chisq")
Anova(fit, test.statistic = "LR", type = 3)

#GLM Gamma avec fonction de lien logarithme avec les variables significatives
fit2 <- glm(formula = fire_insurance ~ area + furniture + hoa + rent_amount, data = dt2, family = Gamma(link = "log"))

summary(fit2)
anova(fit2, test = "Chisq")
Anova(fit2,test.statistic = "LR", type = 3)


#GLM gaussien avec fonction de lien identit�
fit3 <- glm(formula = fire_insurance ~ area + rooms + bathroom + parking + animal + furniture + hoa +
              rent_amount + property_tax, data = dt2, family = gaussian(link = "identity"))

summary(fit3)
anova(fit3, test = "Chisq")
Anova(fit3,test.statistic = "LR", type = 3)

#GLM gaussien avec fonction de lien identit� avec les variables significatives
fit4 <- glm(formula = fire_insurance ~ area + furniture + hoa + rent_amount, data = dt2, family = gaussian(link = "identity"))

summary(fit4)
anova(fit4, test = "Chisq")
Anova(fit4,test.statistic = "LR", type = 3)
