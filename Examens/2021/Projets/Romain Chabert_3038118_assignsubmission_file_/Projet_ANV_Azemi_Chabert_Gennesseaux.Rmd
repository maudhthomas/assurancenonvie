---
title: "Projet - Assurance non-vie"
author: "Rilind Azemi, Romain Chabert, Blaise Gennesseaux"
output:
  pdf_document:
    toc: yes
    toc_depth: 3
  html_document: default
header-includes: \usepackage{bbm}
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

#install.packages("gridExtra")
#install.packages("cowplot")
#install.packages("GGally")
#install.packages("vctrs")
#install.packages("car")
#install.packages("leaps")
#install.packages("randomForest")
#install.packages("MASS")

library(gridExtra)
library(cowplot)
library(readr)
library(ggplot2)
library(GGally)
library(car)
library(leaps)
library(randomForest)
library(MASS)

data = read.csv(file = "BDD_Azemi_Chabert_Gennesseaux.csv", header = TRUE, sep = ",")
```


\newpage

# I. Introduction

## A. Présentation du jeu de données

Les données utilisées dans le cadre de ce projet proviennent du secteur de l'assurance automobile. L'objectif sera de déterminer le prix d'une voiture en fonction de ses caractéristiques.  
  
Les différentes variables que contient le jeu de données sont :

**symboling** : Échelle de risque de la voiture (allant de -3 *peu risqué* à 3 *très risqué*)  
**normalized.losses** : Perte de valeur normalisée due à l'usage  
**wheel.base** : Empattement du véhicule (en centimètres)  
**length** : Longueur du véhicule (en centimètres)  
**width** : Largeur du véhicule (en centimètres)  
**height** : Hauteur du véhicule (en centimètres)  
**curb.weight** : Poids à vide du véhicule (en kilogrammes)  
**engine.size** : Cylindrée du véhicule (en centimètres cubes)  
**bore** : Diamètre des cylindres (en centimètres)  
**stroke** : Distance maximale parcourue par un piston dans le bloc-cylindres (en centimètres)  
**compression.ratio** : Rapport des volumes du cylindre lorsque le piston est au point mort bas et lorsque le piston est au point mort haut  
**horsepower** : Puissance du moteur (en chevaux)  
**peak.rpm** : Maximum du nombre de tour du moteur par minute  
**city.mpg** : Distance que peut effectuer la voiture par gallon en ville  
**highway.mpg** : Distance que peut effectuer la voiture par gallon en autoroute  
**class** : Prix de la voiture  


## B. Statistiques descriptives sur les données


Il convient après cette brève présentation du jeu de données de présenter leurs caractéristiques essentielles (une fois encore brièvement car là n'est pas l'enjeu de l'étude).  

Il est notamment possible de s'apensentir d'abord sur la variable *class*, c'est à dire le prix de la voiture.

```{r, echo=FALSE, fig.height=2.2, fig.width=8, fig.align = 'center'}
A = ggplot(data,aes(y=class)) +
  geom_boxplot(color="darkblue", fill="lightblue", alpha=0.4) +
  scale_x_discrete() +
  labs(title = "Répartition du coût des voitures") +
  coord_flip()+
  theme(axis.title.x=element_blank(),
        axis.title.y=element_blank())

B = ggplot(data,aes(class)) +
    geom_density(color="darkblue", fill="lightblue", alpha=0.4) +
  labs(title = "Densité empirique du coût des voitures") +
  xlim(0,40000) +
  theme(axis.title.x=element_blank(),
        axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())

plot_grid(A,B)
```


Les prix semblent relativement homogènes, bien que légèrement déséquilibrés au niveau des valeurs supérieures, ce que est confirmé par la densité des données.

De manière plus générale il est possible de tracer les densités des différentes variables afin d'avoir une vision plus précise de leur aspect empirique.

```{r, echo=FALSE, fig.align = 'center'}
A1 = ggplot(data, aes(symboling))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A2 = ggplot(data, aes(normalized.losses))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A3 = ggplot(data, aes(wheel.base))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A4 = ggplot(data, aes(length))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A5 = ggplot(data, aes(width))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A6 = ggplot(data, aes(height))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A7 = ggplot(data, aes(curb.weight))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A8 = ggplot(data, aes(engine.size))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A9 = ggplot(data, aes(bore))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A10 = ggplot(data, aes(stroke))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A11 = ggplot(data, aes(compression.ratio))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A12 = ggplot(data, aes(horsepower))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A13 = ggplot(data, aes(peak.rpm))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A14 = ggplot(data, aes(city.mpg))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())
A15 = ggplot(data, aes(highway.mpg))+
  geom_density(color="darkred", fill="tomato", alpha=0.4)+
  theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank())

grid.arrange(A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,A15, nrow = 3)
```

Cependant, les variables présentées ci-dessus sont continues. Afin de pouvoir poursuivre l'étude, celles-ci ont donc été regroupées en classes. Dans l'objectif de disposer de classes relativement équilibrées, celles-ci ont donc été construites avec pour bornes les quartiles respectifs de chaque variable. Ainsi, chaque variable est scindée en 4 classes : *min - premier quartile* / *premier quartile - médiane* / *médiane - troisième quartile* / *troisième quartile - max*.

Ce sont ces classes qui seront donc utilisées pour la suite de la présente étude.

```{r, echo=FALSE}

symboling_class = cut(data$symboling, breaks=c(-3,-1,0,1,4))

normalized.losses_class = cut(data$normalized.losses, breaks=c(64,94,113,148,256))

wheel.base_class = cut(data$wheel.base, breaks=c(86,94.50,96.90,100.80,115.60))

length_class = cut(data$length, breaks=c(141,165.7,172.4,177.8,203))

width_class = cut(data$width, breaks=c(60,64.00,65.40,66.50,72))

height_class = cut(data$height, breaks=c(49,52.25,54.10,55.50,59.80))

curb.weight_class = cut(data$curb.weight, breaks=c(1487,2066,2340,2810,4066))

engine.size_class = cut(data$engine.size, breaks=c(60,100,110,135,260))

bore_class = cut(data$bore, breaks=c(2,3.05,3.27,3.56,3.94))

stroke_class = cut(data$stroke, breaks=c(2,3.105,3.270,3.410,4.170))

compression.ratio_class = cut(data$compression.ratio, breaks=c(6,8.70,9,9.40,23))

horsepower_class = cut(data$horsepower, breaks=c(47,69,88,114,200))

peak.rpm_class = cut(data$peak.rpm, breaks=c(4000,4799,5199,5499,7000))

city.mpg_class = cut(data$city.mpg, breaks=c(14,23,26,30,49))

highway.mpg_class = cut(data$highway.mpg, breaks=c(17,28,32,37,54))

data_class = data.frame(prix = data$class,
                       symboling_class,
                       normalized.losses_class,
                       wheel.base_class,
                       length_class,
                       width_class,
                       height_class,
                       curb.weight_class,
                       engine.size_class,
                       bore_class,
                       stroke_class,
                       compression.ratio_class,
                       horsepower_class,
                       peak.rpm_class,
                       city.mpg_class,
                       highway.mpg_class)

```


# II. Problématique

L'objectif de cette étude est la prédiction du prix d'une voiture en utilisant les modèles linéaires généralisés.


# III. Modèles linéaires généralisés

Un modèle est un modèle linéaire généralisé s’il vérifie les hypothèses suivantes :

- $Y | X = x ~\mathbb{P}_{\theta, \phi}$  
- $g(\mu(X)) = g(E[Y | X]) = X\beta$ pour une certaine fonction g bijective, appelée *fonction de lien*.  

Pour la suite, nous utiliserions l'abréviation "GLM" (Generalized Linear Model) pour nous référer à un modèle linéaire généralisé.

Afin de prédire le prix de la voiture, nous allons fait le choix d'un GLM Gamma puis d'un GLM Gaussienne car la variable réponse est continue positive.

## A. GLM Gamma

En notant $Y_{i}$ le prix de la voiture, nous avons : 

- $Y_{i}|X_{i}$ suit une loi gamma  
- $\frac{1}{\mathbb{E}[Y_{i}|X_{i}]} = X_{i}^{'}\beta$  

- la matrice X est de dimension $159\times61$  
- le vecteur $\beta$ est de dimension 17 : $\beta = (\mu,\alpha_{1},\alpha_{2}, \alpha_{3}, \alpha_{4}, \gamma_{1}, \gamma_{2}, \gamma_{3}, \gamma_{4}, \eta_{1}, \eta_{2}, \eta_{3}, \eta_{4}, \lambda_{1}, \lambda_{2}, \lambda_{3}, \lambda_{4},...)$  

-- $\alpha_{j}$ j = 1,..., 4 l'effet de la classe de symboling_class  
-- $\gamma_{j}$ j = 1,..., 4 l'effet de la classe de normalized.losses_class    
-- $\eta_{j}$ j = 1,..., 4 l'effet de la classe de wheel.base_class  
-- $\lambda_{j}$ j = 1,..., 4 l'effet de la classe de stroke_class  
-- ...  

Les conditions d'identifiabilité du modèle sont :  

$\alpha_{1}=0 \ \ \gamma_{1}=0 \ \ \eta_{1}=0 \ \ \lambda_{1}=0 \ \ ...\ et\ ainsi\ de\ suite$  

Le détail de cette régression indique que :

```{r, echo=FALSE, comment=NA}
fit1 <- glm(formula = prix ~ .,
            data = data_class,
            family = Gamma())

summary(fit1)
```

Il est possible de constater que les résidus de la déviance sont tous très inférieur à 1 en valeur absolue, donc le modèle semble bien ajusté. De plus la déviance du modèle est nettement inférieure à celle du modèle comprenant juste la constante.

### Sélection de modèle

Regardons à présent la significativité de chaque variable du modèle à l'aide de la fonction Anova du package "car" afin de sélectionner les variables à garder.

```{r, echo=FALSE, comment=NA}
Anova(fit1, test.statistic = "LR", type = 3)
```

Nous nous intéressons ici à la p-valeur du test du ratio de vraisemblance, en prenant comme seuil 0.05.  

Dans notre cas, plusieurs variables ont une p-valeur supérieure à 0.05, donc on retire la variable avec la plus grande p-valeur supérieure à 0.05 puis on réitère le procédé jusqu'à ce qu'il n'y en ai plus.

```{r, echo=FALSE, comment=NA}
fit1 <- glm(formula = class ~symboling_class +
              length_class + 
              width_class + 
              height_class +
              bore_class +
              horsepower_class +
              peak.rpm_class +
              city.mpg_class +
              highway.mpg_class,
            data = data,
            family = Gamma())

Anova(fit1, test.statistic = "LR", type = 3)
```

Ici toutes les variables ont une p-valeur inférieure à 0.05 (seuil que l'on se donne) donc elles sont toutes significatives.

Nous obtenons ainsi le modèle suivant :

```{r, echo=FALSE, comment=NA}
summary(fit1)
```

Les résidus de la déviance sont toujours tous très inférieurs à 1 en valeur absolue donc nous pouvons conclure que le modèle semble bien ajusté.

De plus avec un test "anova" qui permet de comparer les modèles emboîtés nous pouvons voir que la déviance diminue avec l'ajout des variables au fur et à mesure.

```{r, echo=FALSE, comment=NA}
anova(fit1, test = "Chisq")
```


### Prédiction

Nous allons tester notre modèle linéaire en prenant pour base d'entraînement les 80% premières observations et comme base de test les 20% dernières observations.

Nous faisons la prédiction et nous allons la comparer avec la prédiction des *random forest* qui est une méthode de prédiction assez pertinente.

```{r, echo=FALSE, warning=FALSE,fig.height=3}
#Prédiction

data_final = data_class[,c("symboling_class","length_class","width_class","height_class","bore_class","horsepower_class","peak.rpm_class","city.mpg_class","highway.mpg_class", "prix")]

datatrain = data_final[1:128,]
datatest = data_final[129:159,]

MODEL_PRED =glm(formula = prix ~ ., data = datatrain, family = Gamma())

PRED = predict.glm(MODEL_PRED, newdata = datatest, type="response")
```


```{r, echo=FALSE, warning=FALSE,fig.height=3}
#Comparaison avec les random forest

rf.reg <- randomForest(prix ~ . , data = datatrain)
rf.pred = predict(rf.reg, newdata =  datatest, type="response")

dfpred = data.frame(pred = rep(datatest[,"prix"],2), allpred= c(PRED,rf.pred), lab= c(rep('pred', length(PRED)), rep('rf', length(rf.pred))))

ggplot(dfpred)+ aes(x=dfpred$pred, y= dfpred$allpred, color= dfpred$lab)+
  geom_point()+geom_abline(slope=1, intercept=0)+ xlab('')+ylab('')+
  labs(colour = "Algorithm") 
```

Il convient de noter que ce graphique illustre, par comparaison avec les *random forest*, une prédiction des prix plutôt performante. On note notamment que, malgré une certaine dispersion, les prédictions sont bien centrées autour de la moyenne. Cette dispersion est probablement à mettre en lien avec le faible nombre d'observations à disposition (159).


## B. GLM Gaussien

En notant $Y_{i}$ le prix de la voiture, nous avons :  

- $Y_{i}|X_{i}$ suit une loi gaussienne  
- $\mathbb{E}[Y_{i}|X_{i}] = X_{i}^{'}\beta$    

- la matrice X est de dimension $159\times61$  
- le vecteur $\beta$ est de dimension 17 : $\beta = (\mu,\alpha_{1},\alpha_{2}, \alpha_{3}, \alpha_{4}, \gamma_{1}, \gamma_{2}, \gamma_{3}, \gamma_{4}, \eta_{1}, \eta_{2}, \eta_{3}, \eta_{4}, \lambda_{1}, \lambda_{2}, \lambda_{3}, \lambda_{4},...)$  

-- $\alpha_{j}$ j = 1,..., 4 l'effet de la classe de symboling_class  
-- $\gamma_{j}$ j = 1,..., 4 l'effet de la classe de normalized.losses_class  
-- $\eta_{j}$ j = 1,..., 4 l'effet de la classe de wheel.base_class  
-- $\lambda_{j}$ j = 1,..., 4 l'effet de la classe de stroke_class  
-- ...  

Les conditions d'identifiabilité du modèle sont :  

$\alpha_{1}=0 \ \ \gamma_{1}=0 \ \ \eta_{1}=0 \ \ \lambda_{1}=0 \ \ ...\ et\ ainsi\ de\ suite$  

Le détail de cette régression indique que :

```{r, echo=FALSE, comment=NA}
fit2 <- glm(formula = prix ~ ., data = data_class, family = gaussian())

summary(fit2)
```

Il convient d'observer que les résidus de la déviance sont tous très grand par rapport à 1 en valeur absolue donc le modèle semble mal ajusté. Par ailleurs, la déviance du modèle est également très grande, ce qui conforte cette hypothèse.

### Sélection de modèle

Regardons à présent la significativité de chaque variable du modèle à l'aide de la fonction Anova du package "car".

```{r, echo=FALSE, comment=NA}
Anova(fit2, test.statistic = "LR", type = 3)
```

Nous passons ensuite à la sélection de variables en utilisant le même procédé que lors du premier GLM.

```{r, echo=FALSE, comment=NA}
fit2 <- glm(formula = prix ~ symboling_class +
              length_class +
              width_class+
              height_class+
              horsepower_class+
              peak.rpm_class+
              highway.mpg_class, data = data_class, family = gaussian())

Anova(fit2, test.statistic = "LR", type = 3)
```

Ici toutes les variables ont une p-valeur inférieur à 0.05 (seuil que l'on se donne) donc elles sont toutes significatives.

Le modèle suivant est alors obtenu :

```{r, echo=FALSE, comment=NA}
summary(fit2)
```


De plus avec un test "anova" qui permet de comparer les modèles emboîtés nous pouvons voir que la déviance baisse avec l'ajout des variables au fur et à mesure.

```{r, echo=FALSE, comment=NA}
anova(fit2, test = "Chisq")
```

### Prédiction

Nous allons tester notre modèle linéaire en prenant pour base d'entraînement les 80% premières observations et comme base de test les 20% dernières observations.

Nous faisons la prédiction et nous allons la comparer avec la prédiction des *random forest*.

```{r, echo=FALSE, warning=FALSE,fig.height=3}
#Prédiction

data_final = data_class[,c("symboling_class","length_class","width_class","height_class","horsepower_class","peak.rpm_class","highway.mpg_class", "prix")]

datatrain = data_final[1:128,]
datatest = data_final[129:159,]

MODEL_PRED =glm(formula = prix ~ ., data = datatrain, family = gaussian())

PRED = predict.glm(MODEL_PRED, newdata = datatest, type="response")
```


```{r, echo=FALSE, warning=FALSE,fig.height=3}
#Comparaison avec les random forest

rf.reg <- randomForest(prix ~. , data = datatrain)
rf.pred = predict(rf.reg, newdata =  datatest, type="response")

dfpred = data.frame(pred = rep(datatest[,"prix"],2), allpred= c(PRED,rf.pred), lab= c(rep('pred', length(PRED)), rep('rf', length(rf.pred))))

ggplot(dfpred)+ aes(x=dfpred$pred, y= dfpred$allpred, color= dfpred$lab)+
  geom_point()+geom_abline(slope=1, intercept=0)+ xlab('')+ylab('')+
  labs(colour = "Algorithm") 
```

Ainsi, le graphique montre que par comparaison avec les *random forest* la prédiction semble relativement satisfaisante, notamment compte tenu du faible nombre d'observations. Cependant, il semblerait que les valeurs prédites soient légèrement surévaluées par rapport à la moyenne.

# IV. Conclusion

Au vu des résultats précédents, il semble raisonnable de privilégier le modèle gamma car les valeurs des résidus de la déviance et de la déviance permettent indiquent que celui-ci est bien ajusté, contrairement aux résultats obtenus pour le modèle gaussien.  

Par ailleurs, les prédictions réalisés avec les deux modèles illustrent un biais haussier pour le modèle gaussien, là où les valeurs obtenues grâce au modèle gamma sont bien centrées autour de la moyenne *(en dépit d'une dispersion légèrement plus élevée)*.

Il convient donc de privilégier le GLM gamma pour la prédiction des prix des voitures.

