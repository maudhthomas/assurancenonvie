\documentclass[11pt]{article}

\RequirePackage{natbib}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{fullpage}

\RequirePackage{amsthm}
\RequirePackage{enumerate}
\RequirePackage{color}
\RequirePackage{placeins}
\RequirePackage{pdfsync}
\RequirePackage{mathbbol}
\RequirePackage{amsmath}
\RequirePackage{amsthm}
\RequirePackage[colorlinks]{hyperref}
\RequirePackage{xcolor}
\RequirePackage{array}
\RequirePackage{booktabs}
\RequirePackage{graphicx}
%%\usepackage[font=sl,position=top]{caption}
\definecolor{halfgray}{gray}{0.55} % chapter numbers will be semi transparent .5 .55 .6 .0
\definecolor{webgreen}{rgb}{0,.5,0}
\definecolor{webbrown}{rgb}{.6,0,0}
\definecolor{RoyalBlue}{rgb}{0,0.08,0.45}
\RequirePackage{hypernat}
\RequirePackage{multirow}
\usepackage{tikz}

 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\def\QEDclosed{\mbox{\rule[0pt]{1.3ex}{1.3ex}}} % for a filled box
\def\QEDopen{{\setlength{\fboxsep}{0pt}\setlength{\fboxrule}{0.2pt}\fbox{\rule[0pt]{0pt}{1.3ex}\rule[0pt]{1.3ex}{0pt}}}}
\def\QED{\QEDclosed} % default to closed


%%\renewcommand{\theequation}{\thesection.\arabic{equation}}
\numberwithin{equation}{section}
\newtheorem{thm}[equation]{Théorème}
\newtheorem{cor}[equation]{Corollaire}
\newtheorem{lem}[equation]{Lemme}
\newtheorem{prp}[equation]{Proposition}
\newtheorem{propt}[equation]{Propriétés}
\newtheorem{clm}[equation]{Claim}
\newtheorem{cond}[equation]{Condition}  
\newtheorem{dfn}[equation]{Définition}
\theoremstyle{remark}
\newtheorem{crt}[equation]{Criterion}

\newtheorem{hyp}[equation]{Hypothèses}
\newtheorem{rem}[equation]{Remarque}
\newtheorem{exe}[equation]{Exemple}
\newtheorem{exo}[equation]{Exercice}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.


\newcommand{\mathd}{\mathrm{d}}
\newcommand{\mathe}{\mathrm{e}}
\newcommand{\tmmathbf}[1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\tmop}[1]{\ensuremath{\operatorname{#1}}}
\newcommand{\EXP}{\ensuremath{\mathbb{E}}}
\newcommand{\IND}{\mathbb{I}}
\newcommand{\KT}{\textsc{kt}}
\newcommand{\Np}{\ensuremath{\mathbb{N}_+}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\probaB}{\ensuremath{\mathbb{P}}}
\newcommand{\equivaut}{\ensuremath{\Leftrightarrow}}
\newcommand{\M}{\ensuremath{\mathfrak M}}
\newcommand{\longrightarrowlim}{\ensuremath{\mathop{\longrightarrow}\limits}}
\newcommand{\var}{\operatorname{Var}}
\newcommand{\esp}{\mathbb{E}}
\newcommand{\pt}{\enspace .}
\newcommand{\ent}{\operatorname{Ent}}
\newcommand{\dis}{\stackrel{d}{=}}
\newcommand{\proba}{\mathbb{P}}
\newcommand{\cov}{\operatorname{Cov}}
\newcommand{\arsinh}{\operatorname{arsinh}}

\title{Econométrie de l'assurance de non-vie}
\author{Maud Thomas}
\date{}
\begin{document}
\input{header}

\begin{center}
    \large \bf \scshape
   Chapitre 1 - Introduction
\end{center}
%\section{Introduction}

\section{Bases de l'assurance}

Une police d'assurance non-vie est un accord entre une compagnie d'assurance et un assuré selon lequel l'assureur s'engage à l'indemniser pour certaines pertes imprévisibles sur une période contre une cotisation. 

Considérons un sinistre dont le coût est très élevé pour un assuré seul. En général, la probabilité d'occurrence de ce sinistre est très faible, mais s'il se produit il a des conséquences dramatiques pour l'assuré. Par exemple, un incendie de maison ou un grave accident de voiture. L'assureur accepte de lui donner de l'argent si le sinistre se produit pour l'aider.  En retour, l'assuré paye une certaine prime d'assurance dont le montant est beaucoup plus petit que le préjudice causé par le sinistre. 

La clé de ce système est la \emph{mutualisation}. Un coût impossible à payer pour un assuré seul peut être assumé par une collectivité telle qu'une compagnie d'assurance.


Soit $X_i$ le coût d'un tel sinistre pour l'assuré $i$ (avec $X_i = 0$ si le sinistre ne s'est produit pas). Si la compagnie d'assurance a $n$ assurés, et si on suppose que les $X_i, i=1,\ldots,n$ sont i.i.d., par la loi des grands nombres 
\[
\frac{1}{n} \sum_{i=1}^n X_i \underset{n \to \infty}{\longrightarrow} \esp \left[X_1 \right] \text{ p.s.}
\]
Ainsi, si $n$ est suffisamment grand, le coût pour l'assureur est approximativement $n \esp[X_1]$, donc si chaque assuré paye individuellement une prime $\pi := \esp X_1$, appelée \emph{prime pure}, la compagnie d'assurance devrait pouvoir payer en cas de sinistre.  

\flushleft{\textbf{Limites évidentes}}

\begin{itemize}
   \item N'inclut pas les frais administratifs: gestion des sinistres
   \item Variance ? (Théorème central limite)
   \item Extrêmes ? (Théorie des valeurs extrêmes)
   \item Erreurs de modélisation 
   \item Assurés non i.i.d.  
   \item Primes impayées
\end{itemize}

En général, les assurés payent donc une prime légèrement supérieure à $\pi$. 

La difficulté en assurance est l'inversion de la chaîne de production. Dans un commerce traditionnel, le produit destiné à la vente est fabriqué avant d’être vendu. Le vendeur connaît donc son coût de production, tous les frais engendrés et donc la marge unitaire qu'il réalisera lors de la vente. En assurance, ce cycle est inversé, l'assuré paye une prime et ne reçoit la prestation qu'a posteriori. La prestation, constituée principalement de paiements pour des sinistres et de frais, n'est pas connue lors du paiement de la prime, elle ne peut être évaluée qu'approximativement par des méthodes statistiques. Cette inversion du cycle rend impossible la détermination exacte de la richesse d'une société d'assurance à un instant donné puisqu'elle ne connaît pas exactement ses engagements.

% En assurance le cycle de production est inversé. Dans un commerce traditionnel, le produit destiné à la vente est fabriqué a priori. Le vendeur connaît ainsi son coût de production, tous les frais et donc la marge unitaire qu'il réalisera lors de la vente. En assurance, ce cycle est inversé, l'assuré paye une prime et ne reçoit la prestation qu'a posteriori. La prestation, constituée principalement de sinistres et de frais, n'est pas connue lors du paiement de la prime, elle ne peut être évaluée qu'approximativement par des méthodes statistiques. Cette inversion du cycle rend impossible la détermination exacte de la richesse d'une société d'assurance à un instant donné puisqu'elle ne connaît pas exactement ses engagements.  

\section{Première limite : hétérogénéité }

Les assurés d'une compagnie d'assurance n'ont pas tous le même profil de risque, c'est-à-dire la probabilité d'avoir un sinistre. 

\begin{exe}
Un automobiliste âgé, ayant une forte consommation d'alcool, avec une vue basse, qui a eu 17 accidents ces trois dernières années a plus de risques qu'un jeune automobiliste qui ne boit que du jus d'orange et qui connaît le code de la route par c\oe{}ur. 
\end{exe}


La compagnie d'assurance a donc intérêt à utiliser le plus d'informations possible pour identifier les assurés \og{} à risque faible \fg{} des assurés \og{} à risque élevé \fg{}. L'idée est donc de se débarrasser des assurés à risque élevé (ou de les faire payer suffisamment) et de garder ceux à risque faible. 

Que se passe-t-il si on ne différencie pas les assurés à risque faible de ceux à risque élevé ? 
\begin{itemize}
   \item Les assurés à risque faible payent plus que ce qu'ils devraient et les assurés à risque élevé moins ;
   \item Les assurés à risque faible iraient donc dans une autre compagnie d'assurance et les assurés à risque éleve resteraient ;
   \item Même si la prime était suffisante pour couvrir tous les assurés du portefeuille, la compagnie d'assurance aura un moins bon résultat que prédit. En effet, les estimations et les prédictions auront été faites sur toute la population alors que seulement une partie risque de se désassurer (les risques faibles) et il ne restera que les risques élevés. 
\end{itemize}

\section{Deuxième limite : provisionnement }

Déterminer quelle devrait être la valeur de la prime ne suffit pas. En effet, dans le portefeuille d’une compagnie d’assurance les primes payées, à une data donnée, les frais administratifs et les sinistres déjà payés sont relativement bien connus. En revanche, les paiements pour des sinistres qui se seront déclarés après cette date sont mal connus. Cette partie inconnue représente les engagements de l'assureur envers l’assuré. Il faut donc mettre de l’argent de côté pour pouvoir honorer cet engagement, on parle de provisionnement, en anglais, de \emph{claim reserving}. Il faut donc évaluer le plus précisément possible les provisions afin de pouvoir 
\begin{itemize}
   \item finir de payer les sinistres en cours ;
   \item payer les prochains sinistres ;
   \item payer les sinistres déjà survenus mais non déclarés (IBNR = incurred but not reported).
\end{itemize}

 Augmenter la prime pour compenser les mauvais résultats n'est pas toujours une option, puisque les provisions reflètent les engagements passés de l'assureur. 

\section{Buts de ce cours}

\begin{enumerate}
   \item \textbf {Tarification} Comment utiliser des méthodes statistiques pour avoir une meilleure idée de la prime que l'assuré doit payer. 
   \begin{enumerate}
      \item[i)] Tarification a priori : comment utiliser les caractéristiques d'un assuré pour déterminer sa prime (Chapitre 2.) ;
      \item[ii)] Tarification a posteriori : comment tirer profiter de ce que l'on a appris sur l'assuré au cours des dernières années (le nombre et le montant des différents sinistres) ?\\
      \quad $\hookrightarrow$ Théorie de la crédibilité (Chapitre 3.), Systèmes Bonus-Malus (Chapitre 4.) ;
   \end{enumerate}
   \item \textbf {Provisionnement} (Chapitre 5.)
   \begin{enumerate}
      \item[i)] Méthodes déterministes vs méthodes stochastiques ;
      \item[ii)] Gestion du risque.
   \end{enumerate} 
\end{enumerate}

\end{document}