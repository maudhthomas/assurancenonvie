\section{Gestion du risque}

Dans les chapitres précédents, nous avons vu qu’il n’était pas suffisant de déterminer la prime d’un assuré pour éviter la ruine d’une compagnie d’assurance et qu’il était également important de faire des provisions pour être en mesure de faire face aux paiements en cas de sinistre.
Ce dernier chapitre traite de la gestion du risque. Le but est d’estimer le montant des fonds propres de la compagnie d’assurance afin qu’elle puisse assumer ses engagements envers ses assurés. Après avoir défini formellement ce qu’est la gestion du risque nous ferons une brève introduction à la théorie des valeurs extrêmes. 

 

Le but principal de la gestion du risque est de déterminer le capital qui devrait être rassemblé pour éviter la ruine à l'entreprise. Le problème n'est pas seulement de déterminer la valeur moyenne des coûts des sinistres mais de garder aussi une marge raisonnable pour prévenir la ruine. Solvabilité II est une réforme réglementaire européenne du monde de l'assurance. Son objectif est de mieux adapter les fonds propres exigés des compagnies d'assurance et de réassurance aux risques que celles-ci encourent dans leur activité. La date d'effet de cette directive est le 1er janvier 2016.
Les assureurs et réassureurs seront contraints de mesurer leurs risques et de s'assurer qu'ils ont suffisamment de fonds propres pour les couvrir.
Le niveau de probabilité de 99,5\% a été celui retenu pour assurer que l'assureur (ou le réassureur) puisse assumer ses engagements envers ses assurés avec une certaine probabilité.  

% La règlementation impose la valeur de la probabilité associée au quantile : 99.5\% pour SII (Solvabilité II).
\begin{dfn}[Value-at-Risk]
Soit $R$ la somme totale des provisions, soit $R = \sum_i R_i$. Alors la Value-at-Risk est définie comme 
\[
VaR_\alpha = \inf \left\{x \colon \mathbb{P}\left(R \leq x \right) =1-\alpha \right\}
\]
\end{dfn} 
La Value-at-Risk d'ordre 99.5\% correspond à $\alpha = 0.005$.

Pour calculer la VaR, il existe au moins trois méthodes 
\begin{itemize}
   \item en proposant une loi pour $R$
   \item avec la théorie des valeurs extrêmes
   \item avec du bootstrap non paramétrique (non traité ici)
\end{itemize}

\subsection{Loi pour $R$}

L'idée est de calculer la $VaR$ comme le quantile associé à une loi de $R$. Pour cela,  
\begin{itemize}
   \item Soit on suppose que $R$ et $\widehat{R}$ sont gaussiens et on estime leur variance. 
   \item Soit on utilise une approche GLM et on calcule les quantiles grâce au choix d'une famille exponentielle.
\end{itemize}

Dans le cas où il est difficile d'obtenir une formule explicite de la VaR, on peut utiliser des méthodes de simulations de type Monte Carlo.

Le problème de cette méthode est qu’elle ne permet pas de prendre en compte les événements extrêmes alors que la VaR correspond en fait à un quantile extrême. Nous allons donc voir comment la théorie des valeurs extrêmes permet de répondre à cette question.

\subsection{Introduction à la théorie des valeurs extrêmes}

\subsubsection{Introduction}
Dans la nuit du 31 janvier au 1\ier{} février 1953, une tempête submergea plusieurs centaines de kilomètres de digues le long de la côte néerlandaise. L'inondation tua 1836 personnes et détruisit 50 000 habitations. A la suite de cette catastrophe, il fut décidé de construire une digue dont la hauteur devait assurer qu'il n'y ait pas plus d'une inondation tous les 10 000 ans. Les données disponibles ne couvrant  qu'une période de 100 ans, comment déterminer une hauteur de digue qui ne soit dépassée qu'une fois tous les 10 000 ans ?

Pour le statisticien, déterminer la hauteur de la digue, c'est estimer un quantile. Prendre comme référence la plus haute vague reviendrait à considérer que le pire s'est déjà produit. 

La statistique classique a pour but d'utiliser les données disponibles afin d'en retirer le maximum d'informations et de modéliser au mieux la loi du hasard régissant le phénomène. Les événements extrêmes, c'est-à-dire un événement qui prend de très petites ou de très grandes valeurs, se prêtent difficiellement à l'application de la statistique classique  puisque les données sont peu nombreuses voire inexistantes. L'information la plus précise est celle contenue dans les valeurs les plus extrêmes observées. La théorie des valeurs extrêmes fournit le cadre mathématique probabiliste rigoureux pour répondre à cette problématique.

La théorie des valeurs extrêmes a pour but d'étudier et de caractériser le comportement des valeurs extrêmes d'un échantillon de variables aléatoires.
Intuitivement, les valeurs extrêmes peuvent être vues comme les plus grandes observations d'un échantillon ou comme les observations dépassant un certain seuil. On peut donc s'attendre à ce que leur comportement soit lié à l'épaisseur de la queue de la loi de l'échantillon. 

On souhaite estimer des quantités dont la probabilité d'observation est très faible. Ces quantités sont en fait des quantiles : on parle de quantile extrêmes lorsque l'ordre du quantile converge vers 0 quand la taille de l'échantillon tend vers l'infini. Ces quantiles extrêmes se trouvent dans la queue de distribution de la loi de l'échantillon.

\begin{dfn}[Quantile]
Le quantile d'ordre $1-\alpha$ de la fonction de répartition $F$ est défini par
\[
q(\alpha) = \inf\left\{y \colon 1-F(y) \leq \alpha \right\}
\]
avec $\alpha \in [0, 1]$.
\end{dfn}
\begin{dfn}[Quantile extrême]
Le quantile extrême d'ordre $1-\alpha_n$ de la fonction de répartition $F$ est défini par
\[
q(\alpha_n) = \inf\left\{y \colon 1-F(y) \leq \alpha_n \right\}
\]
avec $\alpha_n \to 0$ quand $n \to \infty$.
\end{dfn}
Autrement dit, un quantile sera dit extrême si l'on remplace son ordre $\alpha$ par une suite $\alpha_n$ qui tend vers 0 avec $n$. Le fait que $\alpha_n$ tende vers 0 indique que l'information la plus importante pour estimer des quantiles extrêmes est contenue dans la queue de distribution.


\begin{dfn}[Période de retour, Niveau de retour]
On appelle période de retour la fonction $T$ donnée par 
\[
T(y) =\frac{1}{1-F(y)}
\]


On définit le niveau de retour q comme la fonction inverse de la période de retour
\[
q \left(\frac{1}{T} \right) = (1-F)^{\leftarrow} \left(\frac{1}{T}\right)
\]
où $F^{\leftarrow}$ est l'inverse généralisé de $F$. 

\end{dfn}
La période de retour représente le nombre d'observations tel que, en moyenne, il y ait une observation égale ou supérieure à y. Le niveau de retour représente la valeur qui sera dépassé pour une certaine période de retour (de probabilité : $1/T$). Un niveau de retour à un an revient à calculer le quantile d'ordre 1/365.25, soit 0.2737\%. 

\emph{Un évément extrême est un événement dont le temps de retour est grand}. 

Quelle est la probabilité d'observer un événement extrême ayant une valeur plus grand que le maximum de l'échantillon ? Autrement dit, quelle est la probabilité que le quantile extrême soit plus grande que le maximum observé ? 

On considère $n$ variables aléatoires i.i.d. $X_1,\ldots, X_n$ de fonction de répartition $F$. On considère les statistiques d'ordre associées $X_{1,n} \geq \ldots \geq X_{n,n}$ (Attention, je considère le réarrangement décroissant des $X_i$ de telle sorte que $X_{1,n}$ représente le maximum et $X_{n,n}$ le minimum). Remarquons que les statistiques d'ordre ne sont pas i.i.d. par définition.

Soit $\alpha_n \to 0$, alors
\begin{eqnarray*}
\mathbb{P} \left\{X_{1,n} \leq q(\alpha_n) \right\} &=& \mathbb{P} \left\{ \forall i  , \, X_{i} \leq q(\alpha_n) \right\}\\
&=& \prod_{i=1}^n \mathbb{P} \left\{X_i \leq q(\alpha_n) \right\}\\
&=&F^n(q(\alpha_n)) \\
&=&(1-\alpha_n)^n\\
&=& \exp(n \log(1-\alpha_n))\\
&=& \exp(-n \alpha_n(1+o(1)))
\end{eqnarray*}
La probabilité que le quantile extrême soit plus grand que le maximum dépend donc du comportement asymptotique de $n\alpha_n$. Ainsi, lorsque l'on souhaite estimer des quantiles extrêmes, on doit faire la distinction entre deux cas. 

\textbf{Premier cas} : $n\alpha_n \to \infty$, alors $\mathbb{P} \left\{X_{1,n} \leq q(\alpha_n) \right\} \to 0$

Dans ce cas, le quantile à estimer se trouve avec grande probabilité dans l'échantillon. Son estimation requiert donc une interpolation à l'intérieur de l'échantillon : un estimateur naturel est $X_{\lfloor n\alpha_n \rfloor,n}$. Cet estimateur est asymptotiquement gaussien. Remarquons que la condition $n \alpha_n \to \infty$ correspond à un temps de retour $T_n = 1/\alpha_n$ petit devant $n$. 

\textbf{Second cas} : $n\alpha_n \to 0$, alors $\mathbb{P} \left\{X_{1,n} \leq q(\alpha_n) \right\} \to 1$

On cherche alors à estimer un quantile qui se trouve en dehors de l'échantillon avec grande probabilité. Dans ce cas, l'estimateur de $q(\alpha_n)$ ne peut être obtenu en inversant simplement la fonction de répartition empirique. On est dans le cas où $\alpha_n$ converge rapidement vers 0 autrement dit cela revient à supposer que le quantile $q(\alpha_n)$ tend suffisamment vite vers $\infty$ quand $n \to \infty$. Remarquons que la condition $n \alpha_n \to 0$ correspond à un temps de retour $T_n = 1/\alpha_n$ grand devant $n$. Par conséquent, on ne peut pas estimer le quantile de manière empirique. Dans une telle situation, l’estimation du quantile extrême requiert une extrapolation au-delà de l’échantillon.

En théorie des valeurs extrêmes, il existe deux points de vue différents. La première approche étudie les limites possibles pour le maximum d'un échantillon et la deuxième les excès par rapport à un seuil $\tau$ préalablement choisi.

\subsubsection{Méthode des maxima par blocs}

On considère $n$ variables aléatoires i.i.d. $X_1,\ldots, X_n$ de fonction de répartition $F$. On considère les statistiques d'ordre associées $X_{1,n} \geq \ldots \geq X_{n,n}$ (Attention, je rappelle que je considère le réarrangement décroissant des $X_i$). Remarquons que les statistiques d'ordre ne sont pas i.i.d. par définition. Que pouvons-nous dire du comportement du maximum $X_{1,n}$ ? Ce comportement est caractérisé par $F$ :
\begin{eqnarray*}
\mathbb{P} \left\{X_{1,n} \leq x \right\} &=& \mathbb{P} \left\{ \forall i  , \, X_{i} \leq x \right\}\\
&=& \prod_{i=1}^n \mathbb{P} \left\{X_i \leq x \right\}\\
&=&F^n(x)
\end{eqnarray*}
Ainsi, 
\[
\lim_{n\to \infty} \mathbb{P} \left\{X_{1,n} \leq x \right\} = \lim_{n\to \infty} F^n(x) = 
\begin{cases}
1 & \text{ si $x \geq x_F$} \\
0 & \text{ si $x< x_F$} \\
\end{cases}
\]
où $x_F = \sup\left\{y \in \mathbb{R}, \, F(y)<1 \right\}$ est le point terminal de $F$. Ce résultat montre que la loi de $X_{1,n}$ est dégénérée, donc pour obtenir une loi non-dégénérée il faut normaliser $X_{1,n}$ avec des constantes bien choisies.

Le théorème fondamental de la théorie des valeurs extrêmes identifie la famille de lois limites possibles pour $X_{1,n}$

\begin{thm}\label{thm:max}
Soit $(X_n)_{n \geq 1}$ une suite de v.a. i.i.d. de fonction de répartition $F$. S'il existe deux suites normalisantes réelles $(a_n)_{n \geq 1} >0$ et $(b_n)_{n \geq 1} \in \mathbb{R}$ et une loi non dégénérée de fonction de répartition $G_\gamma$ telles que 
\[
\lim_{n \to \infty} \mathbb{P} \left\{\frac{X_{1,n} - b_n}{a_n} \leq x\right\} = \lim_{n \to \infty} F^n(a_n x +b_n) = G_{\gamma}(x)
\]
en tout point de continuité $x$ de $G_\gamma$, alors $G_\gamma$ est de la forme 
\[
G_\gamma (x) = \exp\left(-(1+\gamma x)_+^{-1/\gamma} \right)
\]
avec $\gamma \in \mathbb{R}$ et $z_+ = \max(0,z)$.
\end{thm}

\begin{rem}
\item Le cas $\gamma = 0$ peut être vu comme le cas limite lorsque $\gamma \to 0$. On retrouve alors la loi de Gumbel ayant pour fonction de répartition 
\[
G_0(x) = \exp\left(-\exp(x)\right)
\]
\item Quand les hypothèses du théorème sont vérifiées, on dit que $F$ appartient au domaine d'attraction de $G_\gamma$.
\end{rem}

Le comportement limite du maximum normalisé est ainsi décrit par la fonction de répartition $G_\gamma$ pour la plus grande partie des lois usuelles. $G_\gamma$ est appelée fonction de répartition de la loi de valeurs extrêmes généralisées. 

Ce théorème affirme que la famille des lois limites du maximum normalisé d'une suite de v.a. i.i.d. est paramétrée par un seul paramètre $\gamma$. Ce paramètre $\gamma$ est la clé dans l'étude des valeurs extrêmes.  On distingue trois domaines d'attraction selon le signe de $\gamma$.

\begin{itemize}
   \item Si $\gamma >0$, $F$ appartient au domaine de Fréchet. Il contient les lois dont la fonction de survie est à décroissance polynomiale, c'est-à-dire les lois à queues lourdes. Ex : Pareto, Student, Fréchet, Cauchy
   \item Si $\gamma = 0$, $F$ appartient au domaine de Gumbel. Il contient les lois dont la fonction de survie est à décroissance exponentielle, c'est-à-dire les lois à queue légère. Ex : normale, exponentielle, Gumbel, Gamma
   \item Si $\gamma <0$, $F$ appartient au domaine de Weibull. Toutes les lois de ce domaine ont un point terminal $x_F$ fini.  Ex : Uniforme, Beta
\end{itemize}

La méthode des maxima par blocs consiste à construire une suite de maxima i.i.d. afin de pouvoir ajuster une loi de valeurs extrêmes généralisée. Supposons que nous observons $X_1, X_2, \ldots$ i.i.d. que nous avons rangés en $m$ blocs de taille $n$. Une loi de valeurs extrêmes généralisée est alors ajustée. Les quantiles extrêmes correspondent alors aux quantiles de la loi ajustée.


% \emph{dessin}

% En pratique, 


\subsubsection{Méthode PoT}

Cette méthode coniste à ne garder que les observations ayant dépassé un certain seuil. 

On considère toujours une suite de v.a. i.i.d. $X_1,\ldots,X_n$ et on fixe un seuil $u < x_F$. Soit $X_{i_1},\ldots, X_{i_{N_u}}$ les $N_u$ variables qui ont dépassé le seuil $u$. On note alors $Z_j := X_{i_j}-u$ où $j=1,\ldots,N_u$ les excès au-delà du seuil $u$.

Soit $F_u$ la fonction de répartition de l'excès $Z$ au delà du seuil $u$. La loi des excès est celle de v.a. admettant pour fonction de répartition $F_u(x) = \mathbb{P} \left\{Z \leq x \mid Z > u \right\}$ représentant le probabilité que la v.a. $X$ ne dépasse pas le seuil $u$ de au moins une quantité $x$ sachant qu'elle dépasse $u$. 

Le théorème suivant identifie la famille des lois limites de la loi des excès. 

\begin{thm}
Soit $X_1,X_2,\ldots$ une suite de v.a. i.i.d. de fonction de répartition $F$. Si $F$appartient au domaine d'attraction d'une GEV, alors il existe des constantes $\alpha_u >0$ et $\beta_u$ telles que
\[
\lim_{x \to u_F} F_u(\alpha_u x+\beta_u) = H_{\sigma,\gamma}(x)
\]
en tout point de continuité $x$ de $H_{\sigma,\gamma}$ et $H_{\sigma,\gamma}$ est de la forme 
\[
H_{\sigma,\gamma}(x) = 1 - \left(1 + \gamma \frac{x}{\sigma}\right)_+^{-1/\gamma}, \ \, x \geq 0
\]
\end{thm}

$H_{\sigma,\gamma}$ est appelée loi de Pareto généralisée.

\begin{rem}
\begin{itemize}
   \item Le cas $\gamma =0$ peut être vu comme la limite lorsque $\gamma$ tend vers 0, on retrouve alors la loi exponentielle de paramètre $\sigma$.
   \item On retrouve les mêmes domaines que pour les GEV. 
\end{itemize}
\end{rem}

La méthode PoT consiste à fixer un seuil $u$ puis à ajuter une loi de Pareto généralisée sur les excès au dela de ce seuil $u$.

Une fois que l'on a appliqué l'une ou l'autre de ces deux méthodes on dispose d'une loi pour le maximum ou les excès on peut alors en déduire des quantiles extrêmes comme par exemple la VaR.

\subsection{Conclusion}

La tarification d'un contrat d'assurance non-vie est un équilibre entre la mutualisation et l'individualisation de la prime. Typiquement, il existe deux méthodes pour l'estimation de la prime : soit une loi a priori (en utilisant les caractéristiques de l'assuré sans aucune expérience sur son risque) soit une loi a posteriori (en utilisant l'information dont on dispose sur l'assuré pour comprendre son comportement). Le provisionnement revient à mesurer les engagements de l'assureur.

Dans ce cours, on a vu uniquement les modèles basiques utilisés en assurance. Ces modèles sont assez simples, la principale raison pour ne pas utiliser des modèles plus complexes est le manque de données ou de bases de données appropriées. De plus, ils sont faciles à comprendre et à utiliser du point de vue opérationnel.

\end{document}