\documentclass[leqno,10pt]{beamer}%
\usepackage{defs2}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{multicol}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{mathrsfs}
\usepackage{mathabx}
\usepackage {mathbbol}
%\usepackage{bbding}
\usepackage{manfnt}
%\usepackage{fourier}
\usepackage{apacite}
%\usepackage{biblatex}
%\usepackage{enumitem}
\usepackage{tikz}
\usepackage{mathbbol}
\usepackage{verbatim}
\usepackage{xcolor}


\newtheorem{thm}{Theorem}
\newtheorem{cor}[equation]{Corollary}
\newtheorem{lem}[equation]{Lemma}
\newtheorem{prp}[equation]{Proposition}
\newtheorem{clm}[equation]{Claim}
\theoremstyle{remark}
\newtheorem{crt}[equation]{Criterion}
\newtheorem{dfn}[equation]{Definition}
\newtheorem{rem}[equation]{Remark}
\newtheorem{exe}[equation]{Example}

\newcommand{\mathd}{\mathrm{d}}
\newcommand{\mathe}{\mathrm{e}}
\newcommand{\tmmathbf}[1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\tmop}[1]{\ensuremath{\operatorname{#1}}}
\newcommand{\EXP}{\ensuremath{\mathbb{E}}}
\newcommand{\IND}{\mathbb{I}}
\newcommand{\KT}{\textsc{kt}}
\newcommand{\Np}{\ensuremath{\mathbb{N}_+}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\probaB}{\ensuremath{\mathbb{P}}}
\newcommand{\equivaut}{\ensuremath{\Leftrightarrow}}
\newcommand{\M}{\ensuremath{\mathfrak M}}
\newcommand{\longrightarrowlim}{\ensuremath{\mathop{\longrightarrow}\limits}}
\newcommand{\var}{\operatorname{Var}}
\newcommand{\esp}{\mathbb{E}}
\newcommand{\pt}{\, .}
\newcommand{\ent}{\operatorname{Ent}}
\newcommand{\dis}{\stackrel{d}{=}}
\newcommand{\proba}{\mathbb{P}}

\newcommand*{\TakeFourierOrnament}[1]{{%
\fontencoding{U}\fontfamily{futs}\selectfont\char#1}}
\newcommand*{\danger}{\TakeFourierOrnament{66}}

%\newcommand{\blue}{\textcolor{blue}}

\usetheme{default}
\useoutertheme{infolines}
\setbeamercovered{transparent}
\setbeamerfont{block title}{size=\normalsize}
\setbeamertemplate{headline}{}
\setbeamertemplate{navigation symbols}{} 
\setbeamertemplate{footline}{
  % \hspace*{.5cm}\tiny{%\insertauthor
  %   \hspace*{50pt}
  %   \hfill\insertframenumber/\inserttotalframenumber
  %   \hspace*{.5cm}}\vspace*{.1cm}
  } 


\begin{document}
\title{\textbf{Econométrie de l'assurance de non-vie}}

\subtitle{{\large Chapitre 2 : Tarification a priori} \\ Familles exponentielles}

\author{Maud Thomas}
\institute{ISUP - Sorbonne Université}
\date{}
\maketitle


\begin{frame}{Famille exponentielle}
%\begin{block}{Famille exponentielle}
Un modèle statistique $\left(\Omega, \mathcal{F}, (\mathbb{P}_{\theta,\phi})_{\theta \in \Theta, \phi >0}\right)$ est appelé {\color{blue} famille exponentielle} si les probabilités $\mathbb{P}_{\theta,\phi}$ admettent une densité $f$ par rapport à une mesure dominante avec 
\begin{equation*}
f_{\theta, \phi}(y) = c_\phi(y) \exp\left(\frac{y\theta - a(\theta)}{\phi} \right)  \, . 
\end{equation*}
\begin{itemize}
\item $\theta$ = {\color{blue} paramètre canonique} 
 \item $\phi$ =  paramètre de dispersion, souvent considéré comme un paramètre de nuisance  ;
\item $a(\theta)$ est de classe $C^2$ et convexe ;
\item $c_\phi(y)$ ne dépend pas de $\theta$. 
\end{itemize}
%\end{block}
\only<2>{
\vspace{1em}
{\Large \danger} {\footnotesize Les lois discrètes peuvent appartenir à une famille exponentielle avec pour mesure dominante la mesure de comptage.} 
}
% \begin{itemize} 
% \item  \textbf{Propriété}: Si $Y$ est distribuée selon une loi appartenant à une famille exponentielle, alors  
% \begin{equation*}
% \esp\left[Y \right] = a'(\theta)\, , \ \var\left[ Y\right] = \phi a''(\theta)
% \end{equation*}
% \end{itemize}
\end{frame}

\begin{frame}{Espérance et variance}
\vspace{-4cm}
\begin{block} {Propriété}
Si $Y$ est distribuée selon une loi appartenant à une famille exponentielle, alors  
\begin{equation*}
\esp\left[Y \right] = a'(\theta)\, , \ \var\left[ Y\right] = \phi a''(\theta)
\end{equation*}
\end{block}
\end{frame}



\begin{frame}{Exemples}
\begin{itemize}
	\item  Lois appartenant à une famille exponentielle
	\begin{itemize}
		\item Loi gaussienne, loi inverse gaussienne
		\item Loi exponentielle, loi gamma
		\item Loi de Poisson
		\item Loi de Bernoulli, loi binomiale
		\item Loi binomaile négative
		\item ...
	\end{itemize}
	\only<2>{
	\item  Lois n'appartenant pas à une famille exponentielle
	\begin{itemize}
		\item Loi de Cauchy
		\item Loi de Pareto
		\item Loi log-normale
		\item ...
	\end{itemize}}
\end{itemize}
\end{frame}

\begin{frame}{Calcul pour la loi gaussienne}
On considère une loi normale de moyenne $m$ et de variance connue $\sigma^2$. Alors
\begin{eqnarray*}
f(y) &=& \frac{1}{\sqrt{2 \pi \sigma^2}}\exp \left(-\frac{(y-m)^2}{2\sigma^2}\right) \\
 &=& \frac{\mathe^{-y^2/(2\sigma^2)}}{\sqrt{2 \pi \sigma^2}}\exp \left( \frac{my -m^2/2}{\sigma^2}\right) \\
\end{eqnarray*}
Donc $\theta = m$, $\phi=\sigma^2$, $a(\theta) = \frac{\theta^2}{2}$ et 
\[
c_\phi(y) = \frac{\mathe^{-y^2/(2\sigma^2)}}{\sqrt{2 \pi \sigma^2}}
\]
On peut vérifier que 
\[
\esp[Y]=a'(\theta)=\theta = m \ \text{ et } \ \var[Y] = \phi a''(\theta) = \phi = \sigma^2
\]
\end{frame}


% \subsection{Examples}
% \begin{frame}{Exponential family}{Classical families}
% \begin{center}
% \begin{tabular}{|c|c|c|c|c|c|}
% \hline 
% Family & $\theta$ & $a(\theta)$ & $\phi$ & $\mathbb{E}$ & $\var$ \\
% \hline
% $\mathcal{P}(\lambda)$ & $\log \lambda$& $\mathe^\theta$ & 1 & $\lambda$ & $\lambda$\\
% $B(n,p)$, $n$ constant & $\log(p/(1-p))$ & $ n \log(1+\mathe^\theta)$ & 1 & $np$ & $np(1-p)$ \\
% $\mathcal{N}(m, \sigma^2)$, $\sigma^2$ constant & $m$ & $\theta^2/2$ & $\sigma^2$ & $m$ & $\sigma^2$ \\
% $\Gamma(k, \lambda)$, $k$ constant & $-1/\lambda$ & $-k\log(-\theta)$ & $1$ & $k\lambda$ & $k\lambda^2$ \\
% \hline
% \end{tabular}
% \end{center}
% \vspace{-0.5em}
% \begin{block}{Exercise}
% The negative binomial distribution with parameters $p$ and $r$ is a discrete distribution defined by 
% \begin{equation*}
% \mathbb{P} \left\{ Y=k \right\} = \frac{\Gamma(r+k)}{k! \Gamma(r)}p^k(1-p)^{r}
% \end{equation*}
% Show that it belongs to an exponential family
% \end{block}
% \only<2>{
% \begin{center}
% \begin{tabular}{|c|c|c|c|c|c|}
% \hline 
% Family & $\theta$ & $a(\theta)$ & $\phi$ & $\mathbb{E}$ & $\var$ \\
% \hline
% $NB(p,r)$, $r$ constant & $\log(p)$ & $-r\log(1+\mathe^{\theta})$ & $1$ & $rp/(1-p)$ & $rp/(1-p)^2$ \\
% \hline
% \end{tabular}
% \end{center}
% }
% \end{frame}


\end{document}

