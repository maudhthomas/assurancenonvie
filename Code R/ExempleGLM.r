UKAuto <- read.table("UKAuto.txt", header = T)
head(UKAuto)

levels(UKAuto$OwnerAge) ; levels(UKAuto$Model) ; levels(UKAuto$CarAge)
table(UKAuto[,c(2,3)])

fit <- glm(formula = NClaims ~ OwnerAge + Model + CarAge, data = UKAuto, family = poisson())
summary(fit)

anova(fit, test = "Chisq")

fit.sanscar <- glm(NClaims~Model+OwnerAge, data = UKAuto, family = poisson())
anova(fit.sanscar, fit, test = "Chisq")

library(car)
Anova(fit, test.statistic = "LR", type = 3)

fit2 <- glm(formula = AvCost~OwnerAge+Model+CarAge, data = UKAuto, family = Gamma())
summary(fit2)

anova(fit2, test = "Chisq")
